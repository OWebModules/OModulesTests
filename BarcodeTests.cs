﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OModulesTests.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests
{
    [TestClass]
    public class BarcodeTests
    {
        [TestMethod]
        public async Task QRCodeTests()
        {
            var expectations = new ExpecationsBarCodeModule();

            var controller = expectations.GetController();
            var request = expectations.GetQRCodeRequest();

            var createResult = await controller.GetQRCodeImage(request);

            if (createResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                Assert.Fail(createResult.ResultState.Additional1);
            }
                
        }
    }
}
