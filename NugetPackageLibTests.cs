﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace OModulesTests
{
    [TestClass]
    public class NugetPackageLibTests
    {
        [TestMethod]
        public async Task TestCreatePackage()
        {
            var expectations = new Configuration.ExpectationsNugetLib();
            var request = expectations.GetCreatePackageRequest();
            var controller = expectations.GetPackageCreator();

            var syncResult = await controller.CreatePackage(request);

            if (syncResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(syncResult.ResultState.Additional1);
            }

        }
    }
}
