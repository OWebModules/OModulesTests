﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests
{
    [TestClass]
    public class SQLImporterTests
    {
        [TestMethod]
        public async Task TestSLQImport()
        {
            var expectations = new Configuration.ExpectationsSQLImporter();

            var request = expectations.GetImportSQLRequest();

            var controller = expectations.GetController();

            var result = await controller.ImportSQL(request);

            if (result.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }
    }
}
