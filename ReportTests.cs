﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OModulesTests.Configuration;
using OModulesTests.Models;
using ReportModule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests
{
    [TestClass]
    public class ReportTests
    {
        [TestMethod]
        public async Task TestSyncReport()
        {
            var expectations = new ExpectationsReports();

            var oItemReport = expectations.GetReportOItemForSync;

            var controller = new ReportController(expectations.Globals, false);
            var getReportResult = await controller.GetReport(oItemReport);

            if (getReportResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail("Error while getting Report");
            }

            var syncReportResult = await controller.SyncReport(new ReportModule.Models.SyncReportRequest { ReportItem = getReportResult.Result.Report });

            if (syncReportResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail("Error while syncing Report");
            }

        }

        [TestMethod]
        public async Task TestCreatePDFReport()
        {
            var expectations = new ExpectationsReports();

            var request = expectations.GetCreatePDFReportRequest();

            var controller = new ReportController(expectations.Globals, false);
            var createPDFReportResult = await controller.CreatePDFReport(request);

            if (createPDFReportResult.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(createPDFReportResult.Additional1);
            }
        }

        [TestMethod]
        public async Task TestNameTransform()
        {
            var expectations = new ExpectationsReports();

            var controller = expectations.GetNameTransformProvider();
            var items = expectations.GetNameTransformObjects1();
            var idClass = expectations.IdClassNameTransformConfig();

            var transformResult = await controller.TransformNames(items, true, idClass);

            if (transformResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(transformResult.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task TestGetRowDiff()
        {
            var expectations = new ExpectationsReports();

            var controller = expectations.GetReportController();
            var reportOItem = expectations.ReportOItem;
            var report = await controller.GetReport(reportOItem);
            if (report.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(report.ResultState.Additional1);
            }

            var getRowDiffRequest = expectations.GetRowDiffRequest(report.Result.Report, report.Result.ReportFields);
            var messageOutput = new MessageOutputForTests();
            messageOutput.MessagesToCheck = expectations.TestGetRowDiff_MessageToCheck;
            messageOutput.AreMessagesOk = true;
            messageOutput.OutputMessageForTests += MessageOutput_OutputMessageForTests;
            getRowDiffRequest.MessageOutput = messageOutput;

            var rowDiff = await controller.GetRowDiff(getRowDiffRequest);

            if (rowDiff.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(rowDiff.ResultState.Additional1);
            }

            if (!messageOutput.AreMessagesOk)
            {
                Assert.Fail("The process was not executed like expected!");
            }
        }

        private void MessageOutput_OutputMessageForTests(string message, OntologyAppDBConnector.MessageOutputType messageType, DateTime timeStamp, MessageOutputForTests messageOutput)
        {
            if (!messageOutput.AreMessagesOk) return;
            var isOk = true;
            var messageToTest = messageOutput.MessagesToCheck[messageOutput.MessageCount];
            if (message != messageToTest) isOk = false;
            messageOutput.MessageCount++;
            messageOutput.AreMessagesOk = isOk;
        }

        [TestMethod]
        public async Task TestSyncColumns()
        {
            var expectations = new ExpectationsReports();

            var controller = expectations.GetReportController();
            
            var messageOutput = new MessageOutputForTests();
            messageOutput.MessagesToCheck = expectations.TestSyncColumns_MessageToCheck;
            messageOutput.AreMessagesOk = true;
            messageOutput.OutputMessageForTests += MessageOutput_OutputMessageForTests4; ;

            var reportOItem = expectations.GetReportForColumnsSync();
            var report = await controller.SyncColumns(reportOItem.GUID, messageOutput);
            if (report.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(report.Additional1);
            }
        }

        private void MessageOutput_OutputMessageForTests4(string message, OntologyAppDBConnector.MessageOutputType messageType, DateTime timeStamp, MessageOutputForTests messageOutput)
        {
            if (!messageOutput.AreMessagesOk) return;
            var isOk = true;
            var messageToTest = messageOutput.MessagesToCheck[messageOutput.MessageCount];
            if (message != messageToTest) isOk = false;
            messageOutput.MessageCount++;
            messageOutput.AreMessagesOk = isOk;
        }

        [TestMethod]
        public async Task TestGetFilterItems()
        {
            var expectations = new ExpectationsReports();

            var controller = expectations.GetReportController();
            var request = expectations.GetFilterItemsRequestByReport();
            
            var messageOutput = new MessageOutputForTests();
            messageOutput.AreMessagesOk = true;
            messageOutput.OutputMessageForTests += MessageOutput_OutputMessageForTests1;
            request.MessageOutput = messageOutput;

            var filterResult = await controller.GetFilterItems(request);

            if (filterResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(filterResult.ResultState.Additional1);
            }

            
        }

        private void MessageOutput_OutputMessageForTests1(string message, OntologyAppDBConnector.MessageOutputType messageType, DateTime timeStamp, MessageOutputForTests messageOutput)
        {
            
        }

        [TestMethod]
        public async Task TestReportFilter()
        {
            var expectations = new ExpectationsReports();

            var controller = expectations.GetReportController();
            var reportOItem = expectations.GetReportForFilter();
            var text = expectations.GetFilterTextToSave();

            var reportFields = await controller.GetReportFields(reportOItem);

            if (reportFields.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(reportFields.ResultState.Additional1);
            }
            var messageOutput = new MessageOutputForTests();
            messageOutput.AreMessagesOk = true;
            messageOutput.OutputMessageForTests += MessageOutput_OutputMessageForTests2; ;

            var filterResult = await controller.SavePattern(reportOItem, reportFields.Result, text, messageOutput);

            if (filterResult.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(filterResult.Additional1);
            }


        }

        private void MessageOutput_OutputMessageForTests2(string message, OntologyAppDBConnector.MessageOutputType messageType, DateTime timeStamp, MessageOutputForTests messageOutput)
        {
            
        }

        [TestMethod]
        public async Task TestMoveObjectsByReport()
        {
            var expectations = new ExpectationsReports();

            var controller = expectations.GetReportController();
            var request = expectations.GetMoveObjectsByReportRequest();
            var messageOutput = new MessageOutputForTests();
            messageOutput.MessagesToCheck = expectations.TestGetRowDiff_MessageToCheck;
            messageOutput.AreMessagesOk = true;
            messageOutput.OutputMessageForTests += MessageOutput_OutputMessageForTests3;
            var report = await controller.MoveObjectsByReport(request);
            if (report.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(report.ResultState.Additional1);
            }
        }

        private void MessageOutput_OutputMessageForTests3(string message, OntologyAppDBConnector.MessageOutputType messageType, DateTime timeStamp, MessageOutputForTests messageOutput)
        {
            
        }

        [TestMethod]
        public async Task TestGetObjectsFromReport()
        {
            var expectations = new ExpectationsReports();

            var controller = expectations.GetReportController();
            var request = expectations.GetObjectsFormReportRequest();
            var messageOutput = new MessageOutputForTests();
            messageOutput.AreMessagesOk = true;
            messageOutput.OutputMessageForTests += MessageOutput_OutputMessageForTests_TestGetObjectsFromReport;
            var objectsResult = await controller.GetObjectsFromReport(request);
            if (objectsResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(objectsResult.ResultState.Additional1);
            }

            if (objectsResult.Result.InvlidObjects.Any())
            {
                Assert.Fail($"{objectsResult.Result.InvlidObjects.Count} invlid objects!");
            }
        }

        private void MessageOutput_OutputMessageForTests_TestGetObjectsFromReport(string message, OntologyAppDBConnector.MessageOutputType messageType, DateTime timeStamp, MessageOutputForTests messageOutput)
        {
            throw new NotImplementedException();
        }
    }
}
