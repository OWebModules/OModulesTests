﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace OModulesTests
{
    [TestClass]
    public class GitConnectorTests
    {
        [TestMethod]
        public async Task TestSyncProjects()
        {
            var exceptions = new Configuration.ExpectationsGitConnectorModule();
            var connector = exceptions.GetConnector();

            var request = exceptions.GetSyncProjectRequest();

            var result = await connector.SyncGitProjects(request);

            if (result.ResultState.GUID == exceptions.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task TestSyncGetCommitsAll()
        {
            var exceptions = new Configuration.ExpectationsGitConnectorModule();
            var connector = exceptions.GetConnector();

            var request = exceptions.GetCommitsRequestAll();

            var result = await connector.GetGitCommits(request);

            if (result.ResultState.GUID == exceptions.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task TestSyncGetCommitsProject()
        {
            var exceptions = new Configuration.ExpectationsGitConnectorModule();
            var connector = exceptions.GetConnector();

            var request = exceptions.GetCommitsRequestProject();

            var result = await connector.GetGitCommits(request);

            if (result.ResultState.GUID == exceptions.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task TestSyncGetCommitsDateRange()
        {
            var exceptions = new Configuration.ExpectationsGitConnectorModule();
            var connector = exceptions.GetConnector();

            var request = exceptions.GetCommitsRequestDateRange();

            var result = await connector.GetGitCommits(request);

            if (result.ResultState.GUID == exceptions.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task TestSyncGetCommitsText()
        {
            var exceptions = new Configuration.ExpectationsGitConnectorModule();
            var connector = exceptions.GetConnector();

            var request = exceptions.GetCommitsRequestText();

            var result = await connector.GetGitCommits(request);

            if (result.ResultState.GUID == exceptions.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task TestSyncGetCommitsTextRegex()
        {
            var exceptions = new Configuration.ExpectationsGitConnectorModule();
            var connector = exceptions.GetConnector();

            var request = exceptions.GetCommitsRequestRegex();

            var result = await connector.GetGitCommits(request);

            if (result.ResultState.GUID == exceptions.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }
    }
}
