﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OModulesTests.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests
{
    [TestClass]
    public class OntologyConnectorTests
    {
        [TestMethod]
        public async Task TestExportOntology()
        {
            var expectations = new ExpectationsOntologyConnector();

            var exportResult = await expectations.Connector.ExportOntologies(new OntologyItemsModule.Services.ExportOntologyRequest(expectations.Ontology)
            {
                ExportPath = expectations.ExportPath
            });

            if (exportResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(exportResult.ResultState.Additional1 ?? "Unknown error");
            }
        }
    }
}
