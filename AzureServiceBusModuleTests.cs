﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutomationLibrary.Models;
using AzureServiceBusModule.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace OModulesTests
{
    [TestClass]
    public class AzureServiceBusModuleTests
    {
        private static CancellationTokenSource receiveCancellationToken;
        [TestMethod]
        public async Task SendJobMessageTest()
        {
            var expectation = new Configuration.ExpectationsAzureServiceBusModule();
            var controller = expectation.GetConnector();

            var request = expectation.GetSendJobMessageRequest() ;

            var result = await controller.SendJobMessage(request);

            if (result.ResultState.GUID == expectation.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task ReceiveJobMessageTest()
        {
            var expectation = new Configuration.ExpectationsAzureServiceBusModule();
            
            var controller = expectation.GetConnector();

            var request = expectation.GetReceiveJobMessageRequest();
            receiveCancellationToken = expectation.ReceiveCancellationToken;

            controller.JobRequestCompleted += Controller_JobRequestCompleted;
            var result = await controller.ReceiveJobMessage(request);

            if (result.ResultState.GUID == expectation.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task ArchiveFilesTest()
        {
            var expectation = new Configuration.ExpectationsAzureServiceBusModule();

            var controller = expectation.GetConnector();

            var request = expectation.GetArchiveMediaItemsRequest();
            receiveCancellationToken = expectation.ReceiveCancellationToken;

            var result = await controller.ArchiveFiles(request);

            if (result.ResultState.GUID == expectation.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        private void Controller_JobRequestCompleted(AutomationEntity automationEntity)
        {
            receiveCancellationToken.Cancel();
        }

    }
}
