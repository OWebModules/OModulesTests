﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OModulesTests.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests
{
    [TestClass]
    public class LiteraturQuelleModuleTests
    {
        [TestMethod]
        public async Task GetLiteraturquelleString()
        {
            var expectations = new ExpectationsLiteraturquelleModule();

            var controller = expectations.GetController();

            var result = await controller.GetLiteraturQuelle(expectations.GetLiteraturquelleId());
            if (result.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task SetLiteraturquelleString()
        {
            var expectations = new ExpectationsLiteraturquelleModule();

            var controller = expectations.GetController();

            var request = expectations.GetSetQuelleStringOfQuelleRequest();
            var result = await controller.SetQuelleStringOfQuelle(request);
            if (result.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.Additional1);
            }
        }

    }
}
