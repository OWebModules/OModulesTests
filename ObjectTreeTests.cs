﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OntoWebCore.Models;

namespace OModulesTests
{
    [TestClass]
    public class ObjectTreeTests
    {
        [TestMethod]
        public async Task SearchNodes()
        {
            var expectations = new Configuration.ExpectationsObjectTree();
            var connector = expectations.GetController();

            var result = await connector.GetObjectTree(null, expectations.GetIdClass(), expectations.GetIdRelationType(), expectations.GetIdDirection(), -1);

            if (result.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }

            var treeNodes = new List<KendoTreeNodeSearchItem>();
            foreach (var treeNode in result.Result)
            {
                treeNodes.AddRange(await treeNode.SearchNodesByName(expectations.GetSearchString()));
            }
            
            if (!treeNodes.Any())
            {
                Assert.Fail("No nodes are returned!");
            }
        }

        [TestMethod]
        public async Task CopyRelationsTest()
        {
            var exceptions = new Configuration.ExpectationsObjectEdit();
            var connector = exceptions.GetController();

            var request = exceptions.GetCopyRelationsRequest();

            var result = await connector.CopyRelations(request);

            if (result.ResultState.GUID == exceptions.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task GetObjectRelationTreNodes()
        {
            var exceptions = new Configuration.ExpectationsObjectEdit();
            var connector = exceptions.GetObjectTreeController();

            var request = exceptions.GetObjectRelationTreeRequest();

            var result = await connector.GetObjectRelationTree(request);

            if (result.ResultState.GUID == exceptions.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }


    }
}
