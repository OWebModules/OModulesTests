﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests
{
    [TestClass]
    public class WindowsTasksTests
    {
        [TestMethod]
        public async Task TestImportTasks()
        {
            var expectations = new Configuration.ExpectationsWindowsTasksController();
            var request = expectations.GetImportTaskRequest();

            var controller = expectations.GetConnector();

            var importResult = await controller.ImportTasks(request);
            if (importResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(importResult.ResultState.Additional1);
            }

        }
    }
}
