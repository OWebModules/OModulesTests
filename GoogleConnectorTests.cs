﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace OModulesTests
{
    [TestClass]
    public class GoogleConnectorTests
    {
        [TestMethod]
        public async Task TestSync()
        {
            var expectations = new Configuration.ExpectationsGoogleCalendarSync();
            var request = expectations.GetSyncRequest();
            var controller = expectations.GetController();

            var syncResult = await controller.SyncCalendar(request);

            if (syncResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(syncResult.ResultState.Additional1);
            }

        }

        [TestMethod]
        public async Task TestDelete()
        {
            var expectations = new Configuration.ExpectationsGoogleCalendarSync();
            var request = expectations.GetDeleteRequest();
            var controller = expectations.GetController();

            var syncResult = await controller.DeleteEvent(request);

            if (syncResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(syncResult.ResultState.Additional1);
            }
        }
    }
}
