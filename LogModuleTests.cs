﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OModulesTests.Models;

namespace OModulesTests
{
    [TestClass]
    public class LogModuleTests
    {
        [TestMethod]
        public async Task TestSyncEventEntriesToAppInsight()
        {
            var exceptions = new Configuration.ExpectationsLogModule();
            var connector = exceptions.GetController();

            var request = exceptions.GetSyncLogEntriesToAppInsightRequest();
            var result = await connector.SyncLogEntriesToAppInsight(request);

            if (result.ResultState.GUID == exceptions.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }


        [TestMethod]
        public async Task TestSyncTextParserToAppInsight()
        {
            var exceptions = new Configuration.ExpectationsLogModule();
            var connector = exceptions.GetController();

            var request = exceptions.GetSyncTextParserToApplicationInsightsRequest();
            var result = await connector.SyncTextParserToApplicationInsights(request);

            if (result.ResultState.GUID == exceptions.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task TestGetElasticSearchLog()
        {
            var exceptions = new Configuration.ExpectationsLogModule();
            var connector = exceptions.GetController();

            var request = exceptions.GetGetElasticSearchLogRequest();
            var result = await connector.GetElasticSearchLog(request);

            if (result.ResultState.GUID == exceptions.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task TestUrlCheck()
        {
            var exceptions = new Configuration.ExpectationsLogModule();
            var connector = exceptions.GetController();

            var request = exceptions.GetCheckUrlsRequest();
            var messageOutput = new MessageOutputForTests();
            messageOutput.OutputMessageForTests += MessageOutput_OutputMessageForTests;
            request.MessageOutput = messageOutput;
            var result = await connector.CheckUrls(request);

            if (result.ResultState.GUID == exceptions.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        private void MessageOutput_OutputMessageForTests(string message, OntologyAppDBConnector.MessageOutputType messageType, DateTime timeStamp, MessageOutputForTests messageOutput)
        {
            if (messageType == OntologyAppDBConnector.MessageOutputType.Error)
            {
                messageOutput.AreMessagesOk = false;
            }

            if (!messageOutput.AreMessagesOk)
            {
                return;
            }

            messageOutput.MessageCount++;

            Console.WriteLine(message);
        }

        [TestMethod]
        public async Task TestImportCubewareLogs()
        {
            var exceptions = new Configuration.ExpectationsLogModule();
            var connector = exceptions.GetCubewareLogImporter();

            var request = exceptions.GetImportCubewareLogsRequest();
            var messageOutput = new MessageOutputForTests();
            messageOutput.OutputMessageForTests += MessageOutput_OutputMessageForTests1;
            request.MessageOutput = messageOutput;
            var result = await connector.ImportCubewareLogs(request);

            if (result.GUID == exceptions.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.Additional1);
            }
        }

        private void MessageOutput_OutputMessageForTests1(string message, OntologyAppDBConnector.MessageOutputType messageType, DateTime timeStamp, MessageOutputForTests messageOutput)
        {
            if (messageType == OntologyAppDBConnector.MessageOutputType.Error)
            {
                messageOutput.AreMessagesOk = false;
            }

            if (!messageOutput.AreMessagesOk)
            {
                return;
            }

            messageOutput.MessageCount++;
            Console.WriteLine(message);
        }
    }
}
