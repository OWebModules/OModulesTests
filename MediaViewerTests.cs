﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OModulesTests.Configuration;
using OModulesTests.Models;

namespace OModulesTests
{
    [TestClass]
    public class MediaViewerTests
    {
        [TestMethod]
        public async Task TestFileAssociation()
        {
            var expectations = new ExpectationsMediaViewerModule();

            var request = expectations.GetAssociateMediaItemsRequest();

            var controller = expectations.GetConnector();

            var controllerResult = await controller.AssociateFilesToMediaItems(request);

            if (controllerResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(controllerResult.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task TestSearchPDF()
        {
            var expectations = new ExpectationsMediaViewerModule();

            var request = expectations.GetSearchPDFItemsRequest();

            var controller = expectations.GetPDFSearchController();

            var controllerResult = await controller.SearchPDFItems(request);

            if (controllerResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(controllerResult.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task TestImageMap()
        {
            var expectations = new ExpectationsMediaViewerModule();

            var request = expectations.GetImageMapRequest();

            var messageOutput = new MessageOutputForTests();
            request.MessageOutput = messageOutput;
            messageOutput.OutputMessageForTests += MessageOutput_OutputMessageForTests;
                

            var controller = expectations.GetImageMapController();

            var controllerResult = await controller.GetImageMap(request);

            if (controllerResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(controllerResult.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task TestOCR()
        {
            var expectations = new ExpectationsMediaViewerModule();

            var request = expectations.GetOCRImagesRequest();

            var messageOutput = new MessageOutputForTests();
            request.MessageOutput = messageOutput;
            messageOutput.OutputMessageForTests += MessageOutput_OutputMessageForTestsOCR;


            var controller = expectations.GetOCRController();

            var controllerResult = await controller.OCRImages(request);

            if (controllerResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(controllerResult.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task TestCreateBookmark()
        {
            var expectations = new ExpectationsMediaViewerModule();

            var request = expectations.GetSaveBookmarksRequest();

            var messageOutput = new MessageOutputForTests();
            request.MessageOutput = messageOutput;
            messageOutput.OutputMessageForTests += MessageOutput_OutputMessageForTestsOCR;


            var controller = expectations.GetMediaTaggingcontroller();

            var controllerResult = await controller.SaveBookmarks(request);

            if (controllerResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(controllerResult.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task TestGetBookmarksByUser()
        {
            var expectations = new ExpectationsMediaViewerModule();

            var request = expectations.GetByUserRequest();

            var messageOutput = new MessageOutputForTests();
            request.MessageOutput = messageOutput;
            messageOutput.OutputMessageForTests += MessageOutput_OutputMessageForTestsOCR;


            var controller = expectations.GetMediaTaggingcontroller();

            var controllerResult = await controller.GetMediaBookmarks(request);

            if (controllerResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(controllerResult.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task TestGetBookmarksByBookmark()
        {
            var expectations = new ExpectationsMediaViewerModule();

            var request = expectations.GetByBookmarkRequest();

            var messageOutput = new MessageOutputForTests();
            request.MessageOutput = messageOutput;
            messageOutput.OutputMessageForTests += MessageOutput_OutputMessageForTestsOCR;


            var controller = expectations.GetMediaTaggingcontroller();

            var controllerResult = await controller.GetMediaBookmarks(request);

            if (controllerResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(controllerResult.ResultState.Additional1);
            }
        }

        private void MessageOutput_OutputMessageForTestsOCR(string message, OntologyAppDBConnector.MessageOutputType messageType, DateTime timeStamp, MessageOutputForTests messageOutput)
        {
            messageOutput.MessageCount++;

            var correctCount = 10;
            if (messageOutput.MessageCount > correctCount)
            {
                Assert.Fail($"Correct message-count should be {correctCount}!");
                return;
            }

            if (messageType == OntologyAppDBConnector.MessageOutputType.Error)
            {
                Assert.Fail(message);
            }
            new NotImplementedException();
        }

        private void MessageOutput_OutputMessageForTests(string message, OntologyAppDBConnector.MessageOutputType messageType, DateTime timeStamp, MessageOutputForTests messageOutput)
        {
            messageOutput.MessageCount++;

            var correctCount = 10;
            if (messageOutput.MessageCount > correctCount)
            {
                Assert.Fail($"Correct message-count should be {correctCount}!");
                return;
            }

            if (messageType == OntologyAppDBConnector.MessageOutputType.Error)
            {
                Assert.Fail(message);
            }
        }

    }
}
