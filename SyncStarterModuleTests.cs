﻿using AppointmentModule;
using AppointmentModule.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OModulesTests.Configuration;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests
{
    [TestClass]
    public class TransformNamesTests
    {

        [TestMethod]
        public async Task TestGeneralNameTransformProvider()
        {
            var expectations = new ExpectationsSyncStarterModule();

            var controller = expectations.GetMainenanceController();
            var request = expectations.GetExtractPasswordRequest();

            var extractResult = await controller.ExtractPasswords(request);

            if (extractResult.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(extractResult.Additional1);
            }
        }

    }
}
