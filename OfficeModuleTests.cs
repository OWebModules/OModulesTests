﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutomationLibrary.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OModulesTests.Models;

namespace OModulesTests
{
    [TestClass]
    public class OfficeModuleTests
    {
        [TestMethod]
        public async Task ParseWordFilesTest()
        {
            var expectation = new Configuration.ExpectationsOfficeModule();
            var controller = expectation.GetController();

            var request = expectation.GetWordFilesRequest();
            var messageOutput = new MessageOutputForTests();
            messageOutput.OutputMessageForTests += MessageOutput_OutputMessageForTests;

            var result = await controller.ParseWordFiles(request);

            if (result.ResultState.GUID == expectation.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        private void MessageOutput_OutputMessageForTests(string message, OntologyAppDBConnector.MessageOutputType messageType, DateTime timeStamp, MessageOutputForTests messageOutput)
        {
            if (!messageOutput.AreMessagesOk) return;
            var isOk = true;
            
            messageOutput.MessageCount++;
            messageOutput.AreMessagesOk = isOk;
        }
    }
}
