﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MailsToElastic.Models;
using MailModule.Models;
using MailLib.Models;
using OutlookMailConnector;
using OutlookMailConnector.Models;
namespace OModulesTests.Configuration
{
    public class ExpectationsOutlookConnector : ExpectationsAll
    {
        public GetMailItemsRequest GetMailItemsRequest()
        {
            return new GetMailItemsRequest((new System.Threading.CancellationTokenSource()).Token, false) { OnlyUnread = true };
        }

        public OutlookMailConnector.OutlookMailConnector GetController()
        {
            return new OutlookMailConnector.OutlookMailConnector(Globals);
        }
    }
}
