﻿using FileResourceModule;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OModulesTests.Configuration
{
    public class ExpectationsFileRessourceModule : ExpectationsAll
    {
        public clsOntologyItem GetFileResource()
        {
            return new clsOntologyItem
            {
                GUID = "74a87b2955b541be912176a9e0e45406",
                Name = "Cubeware Job-Log",
                GUID_Parent = "7996f98f33d0409aae0abe56b7000397",
                Type = Globals.Type_Object
            };
        }
        
        public FileResourceController GetController()
        {
            return new FileResourceController(Globals);
        }
    }
}
