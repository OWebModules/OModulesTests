﻿using LocalizationModule;
using LocalizationModule.Models;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests.Configuration
{
    public class ExpectationsZeitpunkt : ExpectationsAll
    {
        public ZeitpunktController GetZeitpunktController()
        {
            return new ZeitpunktController(Globals);
        }

        public LocalizationController GetLocalizationController()
        {
            return new LocalizationController(Globals);
        }

        public clsOntologyItem ZeitpunktItem
        {
            get
            {
                return new clsOntologyItem
                {
                    Name = "2005Y10M1D11h12m40s100ms200µs10ps",
                    GUID_Parent = "594428e6fb3345849dc74279bc27696b",
                    Type = Globals.Type_Object
                };
                    
            }
        }

        public CompleteZeitpunktOntologiesByNamesRequest GetCompleteZeitpunktRequest()
        {
            var request = new CompleteZeitpunktOntologiesByNamesRequest { IdConfig = "a411a193ba9f4e45b2301e7d9bfce1a3" };
            return request;
        }

        public GetDateRangeRequest GetDateRangeRequest()
        {
            var request = new GetDateRangeRequest(new List<string> { "456381d5c831409fa133931ab3611085" });
            return request;
        }

    }
}
