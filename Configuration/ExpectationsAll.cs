﻿using ElasticSearchNestConnector;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests.Configuration
{
    public class ExpectationsAll
    {
        public Globals Globals => new Globals(@".\Config\Config_ont.xml");

        public clsUserAppDBSelector GetCustomDbReader(string index)
        {
            return new clsUserAppDBSelector(Globals.Server, Globals.Port, index, Globals.SearchRange, Globals.Session);
        }

        public clsUserAppDBUpdater GetCustomDbUpdater(string index)
        {
            return new clsUserAppDBUpdater(GetCustomDbReader(index));
        }

        public clsUserAppDBDeletor GetCustomDbDeletor(string index)
        {
            return new clsUserAppDBDeletor(GetCustomDbReader(index));
        }

        public clsDBSelector GetDbReader()
        {
            return new clsDBSelector(Globals.Server, Globals.Port, Globals.Index, Globals.Index_Rep, Globals.SearchRange, Globals.Session);
        }

        public clsOntologyItem UserItem => new clsOntologyItem
        {
            GUID = "74f38566eda64c76b862e839b6a8d07d",
            Name = "tassilok.prj",
            GUID_Parent = "c441518dbfe04d55b538df5c5555dd83",
            Type = Globals.Type_Object
        };

        public List<clsObjectAtt> GetDateTimeSearch => new List<clsObjectAtt>
        { 
            new clsObjectAtt
            {
                ID_Class = "8e34e46a480846fa8d13968a850ba63a",
                ID_AttributeType = "52862fca3e6e48febbf4c8b0133e8d3c"
            }
        };

        public DateTimeRangeFilter GetDateTimeRangeFilterFull => new DateTimeRangeFilter
        {
            Start = DateTime.Now.AddDays(-10),
            Ende = DateTime.Now
        };
    }
}
