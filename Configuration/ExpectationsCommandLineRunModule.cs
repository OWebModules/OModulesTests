﻿using CommandLineRunModule;
using CommandLineRunModule.Models;
using DatabaseManagementModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests.Configuration
{
    public class ExpectationsCommandLineRunModule : ExpectationsAll
    {

        public GetCommandRunsToReportRequest GetCommandRunsOfReportsRequest()
        {
            return new GetCommandRunsToReportRequest(new OntologyClasses.BaseClasses.clsOntologyItem
            {
                GUID = "5d9404e45ea34d63a45397bc1b2c3a84",
                Name = "Signatur",
                GUID_Parent = "30cbc6e89c0f47d6920c97fdc40ea1de",
                Type = Globals.Type_Object
            });
        }

        public CommandLineRunController GetController()
        {
            return new CommandLineRunController(Globals);
        }
    }
}
