﻿using FileCopy;
using FileCopy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests.Configuration
{
    public class ExpectationsFileCopyModule : ExpectationsAll
    {
        public GetCopyItemsRequest GetCopyItemsRequest()
        {
            var request = new GetCopyItemsRequest("a39c47a7f1c44a759ad42d22e6b92661");

            return request;
        }

        public FileCopyController GetController()
        {
            var controller = new FileCopyController(Globals);
            return controller;
        }

    }
}
