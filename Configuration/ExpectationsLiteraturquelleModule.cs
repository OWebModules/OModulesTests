﻿using LiteraturQuelleModule;
using LiteraturQuelleModule.Models;
using LogModule;
using LogModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests.Configuration
{
    public class ExpectationsLiteraturquelleModule : ExpectationsAll
    {
        public string GetLiteraturquelleId()
        {
            return "b15d89ba5d204cd987d49ce3349ec015";
        }

        public SetQuelleStringOfQuelleRequest GetSetQuelleStringOfQuelleRequest()
        {
            return new SetQuelleStringOfQuelleRequest("987eb35341214c9e90fcb52f2c500301");
        }
        public LiteraturQuelleController GetController()
        {
            return new LiteraturQuelleController(base.Globals);
        }
    }
}
