﻿using GitConnectorModule;
using GitConnectorModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests.Configuration
{
    public class ExpectationsGitConnectorModule : ExpectationsAll
    {
        public GitConnector GetConnector()
        {
            return new GitConnector(Globals);
        }

        public SyncGitProjectsRequest GetSyncProjectRequest()
        {
            return new SyncGitProjectsRequest("ff01f22a3c68438da01d0b4b0d8995ae");
        }

        public GetGitCommitsRequest GetCommitsRequestAll()
        {
            return new GetGitCommitsRequest("ff01f22a3c68438da01d0b4b0d8995ae");
        }

        public GetGitCommitsRequest GetCommitsRequestProject()
        {
            return new GetGitCommitsRequest("ff01f22a3c68438da01d0b4b0d8995ae") { IdProject = "7cb77ed21efd403b8e7342449cbeda7e" };
        }

        public GetGitCommitsRequest GetCommitsRequestText()
        {
            return new GetGitCommitsRequest("ff01f22a3c68438da01d0b4b0d8995ae") { CommitTextFilter = "#1000" };
        }

        public GetGitCommitsRequest GetCommitsRequestDateRange()
        {
            return new GetGitCommitsRequest("ff01f22a3c68438da01d0b4b0d8995ae") { CommitRangeStart = new DateTime(2019, 1,1) };
        }

        public GetGitCommitsRequest GetCommitsRequestRegex()
        {
            return new GetGitCommitsRequest("ff01f22a3c68438da01d0b4b0d8995ae") { CommitTextFilterRegex = new System.Text.RegularExpressions.Regex(@"#\d+") };
        }
    }
}
