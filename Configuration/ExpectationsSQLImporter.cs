﻿using SQLImporter;
using SQLImporter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OModulesTests.Configuration
{
    public  class ExpectationsSQLImporter : ExpectationsAll
    {
        public ImportSQLRequest GetImportSQLRequest()
        {
            var cancellationTokenSource = new CancellationTokenSource();
            return new ImportSQLRequest(cancellationTokenSource.Token, "8a5dbf91d95145b59724e6783b288a3d") { Password = "Test", IdUser= "6afcab29b5004467a22bb5604dd77106" };
        }

        public SQLImporterConnector GetController()
        {
            return new SQLImporterConnector(Globals);
        }
    }
}
