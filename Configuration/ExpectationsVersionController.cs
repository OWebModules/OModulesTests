﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VersionModule;
using VersionModule.Models;

namespace OModulesTests.Configuration
{
    public class ExpectationsVersionController : ExpectationsAll
    {
        public VersionController GetController()
        {
            return new VersionController(Globals);
        }


        public GetRefVersionsRequest GetRequestGetVersions()
        {
            return new GetRefVersionsRequest("aa5bfc28194c47409c4f2359879d6d1c", Globals, Globals.Direction_LeftRight, "cf27679fcbe74010a3ae472072762b33");
        }

        public AddVersionsRequest GetRequestAddVersionMajorIncrease()
        {
            var result = new AddVersionsRequest("aa5bfc28194c47409c4f2359879d6d1c", Globals, AddVersionType.IncreaseMajor, LogStateType.VersionChanged, UserItem, DateTime.Now, Globals.Direction_LeftRight, "cf27679fcbe74010a3ae472072762b33", "Testmessage");
            result.Increase = 1;

            return result;
        }

        public AddVersionsRequest GetRequestAddVersionMinorIncrease()
        {
            var result = new AddVersionsRequest("aa5bfc28194c47409c4f2359879d6d1c", Globals, AddVersionType.IncreaseMinor, LogStateType.VersionChanged, UserItem, DateTime.Now, Globals.Direction_LeftRight, "cf27679fcbe74010a3ae472072762b33", "Testmessage");
            result.Increase = 1;

            return result;
        }

        public AddVersionsRequest GetRequestAddVersionBuildIncrease()
        {
            var result = new AddVersionsRequest("aa5bfc28194c47409c4f2359879d6d1c", Globals, AddVersionType.IncreaseBuild, LogStateType.VersionChanged, UserItem, DateTime.Now, Globals.Direction_LeftRight, "cf27679fcbe74010a3ae472072762b33", "Testmessage");
            result.Increase = 1;

            return result;
        }

        public AddVersionsRequest GetRequestAddVersionRevisionIncrease()
        {
            var result = new AddVersionsRequest("aa5bfc28194c47409c4f2359879d6d1c", Globals, AddVersionType.IncreaseRevision, LogStateType.VersionChanged, UserItem, DateTime.Now, Globals.Direction_LeftRight, "cf27679fcbe74010a3ae472072762b33", "Testmessage");
            result.Increase = 1;

            return result;
        }

        public AddVersionsRequest GetRequestAddVersionSet()
        {
            var result = new AddVersionsRequest("aa5bfc28194c47409c4f2359879d6d1c", Globals, AddVersionType.Set, LogStateType.VersionChanged, UserItem, DateTime.Now, Globals.Direction_LeftRight, "cf27679fcbe74010a3ae472072762b33", "Testmessage");
            result.Major = 1;
            result.Minor = 2;
            result.Build = 3;
            result.Revision = 4;

            return result;
        }
    }
}
