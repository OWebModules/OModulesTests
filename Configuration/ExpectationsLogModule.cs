﻿using LogModule;
using LogModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OModulesTests.Configuration
{
    public class ExpectationsLogModule : ExpectationsAll
    {
        public SyncTextParserToApplicationInsightsRequest GetSyncTextParserToApplicationInsightsRequest()
        {
            return new SyncTextParserToApplicationInsightsRequest("23544688593d4fe7972b177dd9c208fa");
        }

        public SyncLogEntriesToAppInsightRequest GetSyncLogEntriesToAppInsightRequest()
        {
            return new SyncLogEntriesToAppInsightRequest("94df6728fd974e1f8ea49888940b68cb", "6afcab29b5004467a22bb5604dd77106","Test");
        }

        public GetElasticSearchLogRequest GetGetElasticSearchLogRequest()
        {
            return new GetElasticSearchLogRequest("6bdc2c0bf3b144d79fd1c27cbdbf12f5");
        }

        public CheckUrlsRequest GetCheckUrlsRequest()
        {
            return new CheckUrlsRequest("41799fcbac1e4f33bfc8f276d28139ab", new CancellationTokenSource().Token);
        }

        public ImportCubewareLogsRequest GetImportCubewareLogsRequest()
        {
            return new ImportCubewareLogsRequest("e039c6aea84f49ecbeea35a463afdb8d", new CancellationTokenSource().Token);
        }
        public LogController GetController()
        {
            return new LogController(base.Globals);
        }

        public CubewareLogImporter GetCubewareLogImporter()
        {
            return new CubewareLogImporter(base.Globals);
        }
    }
}
