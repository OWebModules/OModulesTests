﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MailsToElastic.Models;
using MailModule.Models;
using MailLib.Models;
using OfficeModule;
using OfficeModule.Models;

namespace OModulesTests.Configuration
{
    public class ExpectationsOfficeModule : ExpectationsAll
    {
        public ParseWordFilesRequest GetWordFilesRequest()
        {
            return new ParseWordFilesRequest("3c91fcd88fb840d2a4dcbfa6ed5070e9");
        }

        public OfficeController GetController()
        {
            return new OfficeController(Globals);
        }
    }
}
