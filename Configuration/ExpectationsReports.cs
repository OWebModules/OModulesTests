﻿using OntologyClasses.BaseClasses;
using ProcessModule;
using ProcessModule.Models;
using ReportModule;
using ReportModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests.Configuration
{
    public class ExpectationsReports : ExpectationsAll
    {
        public clsOntologyItem GetReportOItemForSync
        {
            get
            {
                return new clsOntologyItem
                {
                    GUID = "6e24303b9139435d84ee061f6c3e2161",
                    Name = "Typed Tags",
                    GUID_Parent = "30cbc6e89c0f47d6920c97fdc40ea1de",
                    Type = Globals.Type_Object
                };
            }
        }

        public clsOntologyItem ClassForFactoryCreate
        {
            get
            {
                return new clsOntologyItem
                {
                    GUID = "e8f6f1ef76b94d78a234d8be72c0fdf2",
                    Name = "Typed Tag",
                    GUID_Parent = "eaf49d6036ed4a04b44f4b195fd306b8",
                    Type = Globals.Type_Class
                };
            }
        }

        public GetFilterItemsRequest GetFilterItemsRequestByReport()
        {
            var request = new GetFilterItemsRequest(FilterRequestType.GetFiltersByReport)
            {
                ReportOItem = new clsOntologyItem
                {
                    GUID = "426350d4db1847fb908cab5eaadb0d75"
                }
            };
            return request;
        }

        public CreatePDFReportRequest GetCreatePDFReportRequest()
        {
            var request = new CreatePDFReportRequest("7597f276613346059c091f80df75de19");
            return request;

        }

        public string IdClassNameTransformConfig()
        {
            return "93f12e1e0ce54dd0b1e37961f09e5c75";
        }

        public List<clsOntologyItem> GetNameTransformObjects1()
        {
            var result = new List<clsOntologyItem>
            {
                new clsOntologyItem
                {
                    GUID = "97f1f1034f0c4999b8b4c361f37a6895",
                    Name = "Tassilos Songs",
                    GUID_Parent = "93f12e1e0ce54dd0b1e37961f09e5c75",
                    Type = Globals.Type_Object
                }
            };

            return result;
        }

        public NameTransformProvider GetNameTransformProvider()
        {
            return new NameTransformProvider(Globals);
        }

        public GetRowDiffRequest GetRowDiffRequest(ReportItem reportItem, List<ReportField> reportFields)
        {
            var result = new GetRowDiffRequest(reportItem, reportFields);
            return result;
        }

        public clsOntologyItem ReportOItem
        {
            get
            {
                return new clsOntologyItem
                {
                    GUID = "426350d4db1847fb908cab5eaadb0d75",
                    Name = "File-Content-Replace",
                    GUID_Parent = "30cbc6e89c0f47d6920c97fdc40ea1de",
                    Type = Globals.Type_Object
                };
            }
        }

        public List<string> TestGetRowDiff_MessageToCheck
        {
            get
            {
                return new List<string>
                {
                    ReportModule.Primitives.LogMessages.GetRowDiff_ValidateRequest,
                    ReportModule.Primitives.LogMessages.GetRowDiff_ValidatedRequest,
                    ReportModule.Primitives.LogMessages.GetRowDiff_GetRowDiffs,
                    ReportModule.Primitives.LogMessages.GetRowDiff_HaveRowDiffs,
                    ReportModule.Primitives.LogMessages.GetRowDiff_GetRowDiffFields,
                    ReportModule.Primitives.LogMessages.GetRowDiff_HaveRowDiffFields,
                    ReportModule.Primitives.LogMessages.GetRowDiff_HaveRowDiffFields
                };
            }
        }

        public List<string> TestSyncColumns_MessageToCheck
        {
            get
            {
                return new List<string>
                {
                    ReportModule.Primitives.LogMessages.SyncColumns_GetReportAndFields,
                    ReportModule.Primitives.LogMessages.SyncColumn_HaveReportAndFields,
                    ReportModule.Primitives.LogMessages.SyncColumns_GetMSSQLColumns,
                    ReportModule.Primitives.LogMessages.SyncColumns_HaveMSSQLColumns
                };
            }
        }

        public ChangeChecklistEntryStateRequest GetChangeChecklistEntryStateRequest()
        {
            var result = new ChangeChecklistEntryStateRequest
            {
                ChecklistEntryStateItems = new List<ChecklistEntryStateItem>
                {
                    new ChecklistEntryStateItem("b9f45ed0344e46479d9d1c05e9a12faa", "08d27942286846ca923328992daab36b", "84251164265e4e0294b2ed7c40a02e56", "74f38566eda64c76b862e839b6a8d07d", "test")
                }
            };

            return result;
        }

        public ChecklistController GetChecklistController()
        {
            return new ChecklistController(Globals);
        }

        public GetChecklistRequest GetChecklistRequest()
        {
            return new GetChecklistRequest("b9f45ed0344e46479d9d1c05e9a12faa");

        }

        public clsOntologyItem GetReportForFilter()
        {
            return new clsOntologyItem
            {
                GUID = "426350d4db1847fb908cab5eaadb0d75",
                Name = "File-Content-Replace (Log)",
                GUID_Parent = "30cbc6e89c0f47d6920c97fdc40ea1de",
                Type = Globals.Type_Object
            };
        }

        public string GetFilterTextToSave()
        {
            return "SessionStamp > '2021-10-12 07:00:00' AND SessionStamp < '2021-10-12 09:00:00'";
        }

        public MoveObjectsByReportRequest GetMoveObjectsByReportRequest()
        {
            var cancellationTokenSource = new System.Threading.CancellationTokenSource();

            return new MoveObjectsByReportRequest("e880205c403c4786b5918c532ad0f041", cancellationTokenSource.Token);
        }

        public clsOntologyItem GetReportForColumnsSync()
        {
            var report = new clsOntologyItem
            {
                GUID = "ed74ed3dc0fc47bf897290c6b217af99",
                Name = "Duration",
                GUID_Parent = "30cbc6e89c0f47d6920c97fdc40ea1de",
                Type = Globals.Type_Object
            };
            return report;
        }

        public GetObjectsFormReportRequest GetObjectsFormReportRequest()
        {
            var request = new GetObjectsFormReportRequest("2f7edb04e6f44fd7888f9309c9146849", new System.Threading.CancellationTokenSource().Token);
            return request;
        }

        public ReportController GetReportController()
        {
            return new ReportController(Globals, false);
        }
    }
}
