﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TFSConnectorModule;
using TFSConnectorModule.Models;

namespace OModulesTests.Configuration
{
    public class ExpectationsTFSConnector : ExpectationsAll
    {
        public SyncTFSRequest GetRequest()
        {
            return new SyncTFSRequest("a72db6caae1949a281847f8f68c90aa0");
        }

        public TFSConnector GetConnectorSyncReleases()
        {
            return new TFSConnector(Globals);
        }
    }
}
