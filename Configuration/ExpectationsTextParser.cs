﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextParserModule;
using TextParserModule.Analyze;
using TextParserModule.Controller;
using TextParserModule.Models;

namespace OModulesTests.Configuration
{
    public class ExpectationsTextParser : ExpectationsAll
    {
        public ConvertPdfToTextRequest GetRequest()
        {
            return new ConvertPdfToTextRequest("7da10e8f1db4498e901535aabf514b23");
        }

        public IndexPDFRequest GetIndexPDFRequest()
        {
            return new IndexPDFRequest("2c483ad1b1c0492bb2e932c772d4eb79");
        }

        public PDFConverter GetPDFImporter()
        {
            return new PDFConverter(Globals);
        }

        public PDFIndexer GetIndexer()
        {
            return new PDFIndexer(Globals);
        }

        public TextParserController GetTextParserController()
        {
            return new TextParserController(Globals);
        }

        public AnalyzeOModulesLogController GetAnalyzeOModulesLogController()
        {
            return new AnalyzeOModulesLogController(Globals);
        }


        public clsOntologyItem GetTextParserItem()
        {
            return new clsOntologyItem
            {
                GUID = "4744643d9df248829250bff3659d5741",
                Name = "Audible Hörbücher",
                GUID_Parent = "f18f0dc8e57b41f0afbf8a0edd4c0fa8",
                Type = Globals.Type_Object
            };
        }

        public MeasureTextParserRequest GetMeasureTextParserRequest()
        {
            return new MeasureTextParserRequest("ce2165895ffb4d5f84b7c88d62179ae3", (new System.Threading.CancellationTokenSource()).Token);
            
        }

        public RelateTextParserDocsRequest GetRelateTextParserDocsRequest(TextParser textParser, List<ParserField> parserFields)
        {
            return new RelateTextParserDocsRequest(textParser, parserFields)
            {
                Query =
                    "Titel:*"
            };

        }

        public clsOntologyItem GetTextParserItemForReIndexPatternSourceFields()
        {
            return new clsOntologyItem
            {
                GUID = "3703cd63bf3e4a228e69e877fe8cda29",
                Name = "SQL Routine Code (Common Datamodel)",
                GUID_Parent = "f18f0dc8e57b41f0afbf8a0edd4c0fa8",
                Type = Globals.Type_Object
            };
        }

        public ReIndexPatternSourceFieldsByTextparserRequest GetReIndexPatternSourceFieldsRequest(TextParser textParser, List<ParserField> parserFields)
        {
            return new ReIndexPatternSourceFieldsByTextparserRequest(textParser, parserFields, (new System.Threading.CancellationTokenSource()).Token);
        }
    }
}
