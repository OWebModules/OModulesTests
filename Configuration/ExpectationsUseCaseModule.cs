﻿using MailsToElastic;
using MailModule;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutomationLibrary.Controller;
using MailsToElastic.Models;
using MailModule.Models;
using MailLib.Models;
using UseCaseModule;
using UseCaseModule.Models;
using System.Threading;

namespace OModulesTests.Configuration
{
    public class ExpectationsUseCaseModule : ExpectationsAll
    {
        public UseCaseNameTransformController GetHtmlTemplateEngine()
        {
            return new UseCaseNameTransformController(Globals);
        }

        public clsOntologyItem GetUseCaseForTemplate()
        {
            return new clsOntologyItem
            {
                GUID = "00f7c3c892ae4e26b59dd92becd3919b",
                Name = "Anzahl der Listenelemente anzeigen",
                GUID_Parent = "bc0125f1a90b4cb3a5578aaad7865021",
                Type = Globals.Type_Object
            };
        }

        public UseCaseConnector GetUseCaseConnector()
        {
            return new UseCaseConnector(Globals);
        }


        public ExportUseCasesToJsonRequest GetExportToJsonRequest()
        {
            var tokenSource = new CancellationTokenSource();
            var result = new ExportUseCasesToJsonRequest("e8495af801b14eeeb42d69040a05a701", tokenSource.Token);
            return result;
        }
    }
}
