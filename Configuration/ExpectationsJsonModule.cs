﻿using JsonModule;
using JsonModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;

namespace OModulesTests.Configuration
{
    public class ExpectationsNameTransform : ExpectationsAll
    {
        public GeneralNameTransformProvider GetProvider()
        {
            return new GeneralNameTransformProvider(Globals);
        }

        public List<clsOntologyItem> GetTransformItems()
        {
            var result = new List<clsOntologyItem>();
            result.Add(new clsOntologyItem
            {
                GUID = "c8d59c39a7df4fe2922cd98eb1d0c9ef",
                Name = "Preperation of Deployment",
                GUID_Parent = "590aedf908474bbf9424005aa8587a8e",
                Type = "Object"
            });
            return result;

        }
    }
}
