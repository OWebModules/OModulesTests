﻿using FileManagementModule;
using FileManagementModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OModulesTests.Configuration
{
    public class ExpectationsFileSystemManagement: ExpectationsAll
    {
        public GetFileVersionsRequest GetFileVersionsRequest()
        {
            return new GetFileVersionsRequest("36e349c6bfe945e08dea299c9b61b183");
        }

        public UpdateCreateStampRequest GetUpdateCreateStampRequest()
        {
            return new UpdateCreateStampRequest("f0c1fb3e74ea4001be15b13a4e01c5a8");
        }

        public ReplaceContentRequest GetReplaceContentRequest()
        {
            var cancellationTokenSource = new CancellationTokenSource();
            return new ReplaceContentRequest("1721933c9e584af1a17de11bd592394c", cancellationTokenSource.Token);
        }

        public RelateExternalFilesRequest GetRelateExternalFilesRequest()
        {
            var cancellationTokenSource = new CancellationTokenSource();
            return new RelateExternalFilesRequest("736200f4e9b0413ba9db0560c4617d14", cancellationTokenSource.Token);
        }

        public FileManagementController GetController()
        {
            return new FileManagementController(Globals);
        }
    }
}
