﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TypedTaggingModule.Connectors;
using TypedTaggingModule.Models;

namespace OModulesTests.Configuration
{
    public class ExpectationsTypedTagging : ExpectationsAll
    {

        public List<string> GetTexts()
        {
            return new List<string>
            {
                "Immanuel Kant"
            };
        }
        public ParseBase GetParseBase()
        {
            return new ParseBase(Globals);
        }

        public ConvertTypedTagsToRelationsRequest GetConvertTypedTagsToRelationsRequest()
        {
            var result = new ConvertTypedTagsToRelationsRequest("56a6f9d137f14d4abe9fc1bc9dbf38c8", new System.Threading.CancellationTokenSource().Token);
            return result;
        }

        public TypedTaggingConnector GetController()
        {
            return new TypedTaggingConnector(Globals);
        }


        public string SearchStringForParseString => "Product";
        public clsOntologyItem GetRefObjectForParseString()
        {
            return new clsOntologyItem
            {
                GUID = "8aa6caa85bcf42d2ba20545e89152bea",
                Name = "sp_ErpProductVariants",
                GUID_Parent = "3dd1a15feef349438ce1828e201a3c9d",
                Type = Globals.Type_Object
            };
        }
    }
}
