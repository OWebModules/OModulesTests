﻿using JiraConnectorModule.Models;
using JiraConnectorModule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OntologyClasses.BaseClasses;

namespace OModulesTests.Configuration
{
    public class ExpectationsJiraConnectorModule : ExpectationsAll
    {
        public SyncIssuesRequest GetSyncIssueRequest()
        {
            return new SyncIssuesRequest("e3a775c21f814166aa211c01386a4fc1", "74f38566eda64c76b862e839b6a8d07d", "Test");
        }

        public AddCommentToIssueRequest GetAddCommentToIssueRequest(List<clsOntologyItem> commentItems, List<clsObjectAtt> commentMessages)
        {
            return new AddCommentToIssueRequest("e3a775c21f814166aa211c01386a4fc1", 
                "74f38566eda64c76b862e839b6a8d07d", 
                "Test", 
                commentItems, 
                commentMessages);
        }

        public JiraController GetConnector()
        {
            return new JiraController(Globals);
        }
    }
}
