﻿using OntologyClasses.BaseClasses;
using OntologyItemsModule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests.Configuration
{
    public class ExpectationsOntologyConnector: ExpectationsAll
    {
        public string ExportPath => "C:\\Temp\\OntologyExport";

        public clsOntologyItem Ontology { get; private set; }
        

        public OntologyConnector Connector { get; private set; }
        
        public ExpectationsOntologyConnector()
        {
            Connector = new OntologyConnector(Globals);
            Ontology = new clsOntologyItem
            {
                GUID = "0c5596776b634fc3a9e63858758c8e57",
                Name = "Partner-Manager",
                GUID_Parent = "eb411e2ff93d4a5ebbbac0b5d7ec0197",
                Type = Globals.Type_Object
            };
        }
    }
}
