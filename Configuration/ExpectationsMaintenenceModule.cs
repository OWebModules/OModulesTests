﻿using MaintainenceModule;
using MaintainenceModule.Models;
using OntologyClasses.BaseClasses;
using OntologyItemsModule;
using OntologyItemsModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OModulesTests.Configuration
{
    public class ExpectationsMaintenenceModule : ExpectationsAll
    {
        public ConvertIntIdToGuidHashRequest GetConvertIntToGuidRequest()
        {
            return new ConvertIntIdToGuidHashRequest
            {
                QueryItem = new OntologyClasses.BaseClasses.clsOntologyItem
                {
                    GUID_Parent = "91f854565e4047cda822e8562bdcd442"
                }
            };
        }

        public MaintenenceController GetMaintenanceController()
        {
            return new MaintenenceController(Globals);
        }

        public CleanupMultipleGUIDsRequest GetCleanupMultipleGUIDsRequest()
        {
            return new CleanupMultipleGUIDsRequest
            {
                IdClass = "91f854565e4047cda822e8562bdcd442",
                Check = false
            };
        }

        public DeleteAbandonedLeafsRequest GetDeleteAbandonedLeafsRequest()
        {
            return new DeleteAbandonedLeafsRequest("319cbb628c5640bd8f68d3791710add0", "e971160347db44d8a476fe88290639a4", Globals.Direction_LeftRight.GUID);
            
        }

        public DeleteUnrelatedObjectsRequest GetDeleteUnrelatedObjectsRequest()
        {
            return new DeleteUnrelatedObjectsRequest("d706d8059efc4947b81b41045c54db21");

        }

        public MergeObjectsRequestByClass GetMergeObjectsRequestByClass()
        {
            return new MergeObjectsRequestByClass("fbf7df7bcc104038babe946c877f9fb3");
        }

        public MergeObjectsRequestByItems GetMergeObjectsRequestByItems()
        {
            return new MergeObjectsRequestByItems("fbf7df7bcc104038babe946c877f9fb3");
        }

        public RelateObjectsWithObjectsByNameToAttributeRequest GetRelateObjectsWithObjectsByNameToAttributeRequest()
        {
            var idClassMigrationScripts = "ed334a0877c4476dab351fe9d4990892";
            var idClassReplaceLog = "18d752ff00ab45e682cab0d4ae70682d";
            var idAttributeTypePath = "bf4b121d4cbc4cb090629fcbdbf4032f";

            var relationType = Globals.RelationType_belongsTo;
            var direction = Globals.Direction_LeftRight;

            return new RelateObjectsWithObjectsByNameToAttributeRequest(idClassMigrationScripts, idClassReplaceLog, idAttributeTypePath, relationType, direction);
        }

        public OItemListController GetOItemListController()
        {
            return new OItemListController(Globals);
        }

        public StatisticsController GetStatisticsController()
        {
            return new StatisticsController(Globals);
        }

        public RepairAttributesAndRelationsRequest GetRepairAttributesAndRelationsRequest()
        {
            var cancellationTokenSource = new CancellationTokenSource();
            return new RepairAttributesAndRelationsRequest("fd873a5b52b245878e562ffc43366872", cancellationTokenSource.Token);
        }
    }
}
