﻿using CloudStorageModule;
using CloudStorageModule.Models;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OModulesTests.Configuration
{
    public class ExpectationsCloudStorageModule : ExpectationsAll
    {
        public CreateContainerRequest GetCreateContainerRequest()
        {
            
            var containerList = new List<clsOntologyItem>
            {
                new clsOntologyItem
                {
                    GUID = "0a23ed280fdc4da783f8146d8eea183f",
                    Name = "tassilosplaylist",
                    GUID_Parent = "d1ff27875bc14be48a3ed4ae70e52759",
                    Type = Globals.Type_Object
                }
            };

            var cancellationTokenSource = new CancellationTokenSource();
            return new CreateContainerRequest(containerList, cancellationTokenSource.Token);
        }

        public SyncMediaListRequest GetSyncMediaListRequest()
        {
            var cancellationTokenSource = new CancellationTokenSource();
            var request = new SyncMediaListRequest("7c1be9ff3bf544d687135e72142d3f66", cancellationTokenSource.Token);
            return request;
        }

        public CloudStorageController GetController()
        {
            var controller = new CloudStorageController(Globals);
            return controller;
        }
    }
}
