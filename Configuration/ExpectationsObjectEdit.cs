﻿using OntologyClasses.BaseClasses;
using OntologyItemsModule;
using OntologyItemsModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OModulesTests.Configuration
{
    public class ExpectationsObjectEdit : ExpectationsAll
    {
        public DuplicateObjectRequest GetDuplicateObjectRequest()
        {
            return new DuplicateObjectRequest("d98bfbf7267743c3b1c68eccd7b9ed3f")
            {
                NewObjectName = "Webposts Scenes"
            };
        }

        public CopyRelationsRequest GetCopyRelationsRequest()
        {
            return new CopyRelationsRequest("2b0a48d67f2445c7b019b6d8dcfb840d");
        }

        public ObjectEditController GetController()
        {
            var controller = new ObjectEditController(Globals);
            return controller;
        }

        public ObjectTreeController GetObjectTreeController()
        {
            var controller = new ObjectTreeController(Globals);
            return controller;
        }

        public OItemListController GetOItemListController()
        {
            var controller = new OItemListController(Globals);
            return controller;
        }

        public GetObjectRelationTreeRequest GetObjectRelationTreeRequest()
        {
            var request = new GetObjectRelationTreeRequest("5a6f4b6d8e4c48f4aca23a17ade4fa64");
            return request;
        }

        public DeleteObjectsRequest GetDeleteObjectsRequest()
        {
            var request = new DeleteObjectsRequest("fced5d2b4a914ec39878e0278240c1ac");
            return request;
        }
    }
}
