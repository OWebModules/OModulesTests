﻿using MailsToElastic;
using MailModule;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MailsToElastic.Models;
using MailModule.Models;
using MailLib.Models;
using LearningModule.Models;
using LearningModule;
using OModulesTests.Models;

namespace OModulesTests.Configuration
{
    public class ExpectationsLearningModule : ExpectationsAll
    {

        public LearningController GetController()
        {
            return new LearningController(Globals);
        }
        public GetRelatedQuestionsRequest GetRelatedQuestionsRequest()
        {
            return new GetRelatedQuestionsRequest(new clsOntologyItem
            {
                GUID = "044a5b7b7cff4738be51af28ac3f30bf"
            });
        }

        public SaveSelfAnswerRequest GetSaveSelfAnswerRequest()
        {
            return new SaveSelfAnswerRequest
            {
                QuestionItem = new clsOntologyItem
                {
                    GUID = "ae3b557af8c54a9b851e600e17145937",
                    Name = "Testfrage",
                    GUID_Parent = "6369213794bb4c3ca8b1d6c6575dc874",
                    Type = Globals.Type_Object
                },
                AnswerCategory = new clsOntologyItem
                {
                    GUID = "291cf52db05249a7bf0ba735d457982f",
                    Name = "Sitzt",
                    GUID_Parent = "0cbec3a8cdb347c8952eb9159c2254f3",
                    Type = Globals.Type_Object
                },
                answerHtml = "<b>Testantwort</b>"
            };
        }

        public GetFilterRequest GetFilterRequest()
        {
            return new GetFilterRequest("d2bc201c1cf948898b3507e6474b51ad");
        }

        #region SaveAnswerWithOfficialAnswer

        public SaveAnswerWithOfficialAnswerRequest GetSaveAnswerWithOfficialAnswerRequest(Question question)
        {
            return new SaveAnswerWithOfficialAnswerRequest(question, new List<string> { "db8df08de3a64fdf8809c305bf2711fa" });
            
        }

        public string GetIdQuestionForSaveAnswer()
        {
            return "12696a0839da49b4b42de203009cde00";
        }

        public Question GetQuestionForSaveAnswer(List<Question> questions)
        {
            var idQuestion = GetIdQuestionForSaveAnswer();
            var question = questions.FirstOrDefault(quest => quest.QuestionItem.GUID == idQuestion);
            return question;
        }

        #endregion
    }
}
