﻿using AppointmentModule;
using AppointmentModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests.Configuration
{
    public class ExpectationsAppointmentModule : ExpectationsAll
    {

        public AppointmentConnector AppointmentConnector => new AppointmentConnector(Globals);
        public AppointmentScheduler SchedulerItem { get; set; }
        public DateTime Start { get; private set; }
        public DateTime Ende { get; private set; }

        public ExpectationsAppointmentModule()
        {
            Start = DateTime.Now;
            Ende = DateTime.Now.AddDays(2);
        }

    }
}
