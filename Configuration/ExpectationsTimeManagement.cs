﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests.Configuration
{
    public class ExpectationsTimeManagement : ExpectationsAll
    {
        public TimeManagementModule.TimeManagementController Controller
        {
            get
            {
                return new TimeManagementModule.TimeManagementController(Globals);
            }
        }
       public TimeManagementModule.Models.StopWatchRequest StopWatchRequestStart
        {
            get
            {
                return new TimeManagementModule.Models.StopWatchRequest(TimeManagementModule.Models.StopWatchMode.Start, "74f38566eda64c76b862e839b6a8d07d", "ac020d4ec1574a22a1c79e874475a4ab")
                {
                    IdStopWatch = "169e5c94f5384db299d4b2a2ce3ee9fc"
                };
            }
        }

        public TimeManagementModule.Models.StopWatchRequest StopWatchRequestStop
        {
            get
            {
                return new TimeManagementModule.Models.StopWatchRequest(TimeManagementModule.Models.StopWatchMode.Stop, "74f38566eda64c76b862e839b6a8d07d", "ac020d4ec1574a22a1c79e874475a4ab")
                {
                    IdStopWatch = "169e5c94f5384db299d4b2a2ce3ee9fc"
                };
            }
        }

        public TimeManagementModule.Models.StopWatchRequest StopWatchRequestReset
        {
            get
            {
                return new TimeManagementModule.Models.StopWatchRequest(TimeManagementModule.Models.StopWatchMode.Reset, "74f38566eda64c76b862e839b6a8d07d", "ac020d4ec1574a22a1c79e874475a4ab")
                {
                    IdStopWatch = "169e5c94f5384db299d4b2a2ce3ee9fc"
                };
            }
        }

        public TimeManagementModule.Models.StopWatchMeasureRequest StopWatchMeasureRequest
        {
            get
            {
                return new TimeManagementModule.Models.StopWatchMeasureRequest("74f38566eda64c76b862e839b6a8d07d", "ac020d4ec1574a22a1c79e874475a4ab")
                {
                    IdStopWatch = "169e5c94f5384db299d4b2a2ce3ee9fc"
                };
            }
        }

    }
}
