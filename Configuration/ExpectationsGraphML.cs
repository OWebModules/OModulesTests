﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OModulesTests.Configuration
{
    public class ExpectationsGraphML : ExpectationsAll
    {
        public GraphMLConnector.GraphMLConnector GetController()
        {
            return new GraphMLConnector.GraphMLConnector(Globals);
        }

        public GraphMLConnector.Models.ExportTypedTagsToGraphMLRequest GetExportTypedTagsToGraphMLRequest()
        {
            return new GraphMLConnector.Models.ExportTypedTagsToGraphMLRequest("59f4b191ecab422fbefa9544c6b3e9c0");
        }

        public GraphMLConnector.Models.ExportERModelRequest GetExportERModelRequest()
        {
            return new GraphMLConnector.Models.ExportERModelRequest("c90ed93606c74b01b7578867d02de298", (new CancellationTokenSource()).Token);
        }

        public GraphMLConnector.Models.ExportOntologyToMLGraphRequest GetExportOntologyToMLGraphRequest()
        {
            return new GraphMLConnector.Models.ExportOntologyToMLGraphRequest("d2ffee9ff5ba42f3b20728760e1269c5", @"C:\Temp\Experte.graphml", GraphMLConnector.Models.GraphType.OntoObject);
        }
           
    }
}
