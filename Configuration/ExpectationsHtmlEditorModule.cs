﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlEditorModule.Models;
using OntologyClasses.BaseClasses;

namespace OModulesTests.Configuration
{
    public class ExpectationsHtmlEditorModule : ExpectationsAll
    {
        public GetHtmlHistoriesByDateRangeRequest GetHtmlHistoriesByDateRangeRequest()
        {
            return new GetHtmlHistoriesByDateRangeRequest
            {
                Start = new DateTime(2020, 1, 1)
            };
        }
        public GetHtmlTagRequest GetHtmlTagRequest(clsOntologyItem documentType, bool finalize, Dictionary<string, string> attributes = null)
        {
            return new GetHtmlTagRequest(documentType, finalize) { Attributes = attributes };
        }

        public HtmlEditorModule.HtmlEditorConnector GetHtmlDocController()
        {
            return new HtmlEditorModule.HtmlEditorConnector(Globals);
        }

        public HtmlEditorModule.ExportHtmlDocController GetHtmlExportController()
        {
            return new HtmlEditorModule.ExportHtmlDocController(Globals);
        }

        public HtmlEditorModule.Models.GetRankedFoundWordsByRefAndHtmlRequest GetRankedFoundWordsByRefAndHtmlRequest()
        {
            return new GetRankedFoundWordsByRefAndHtmlRequest("10839e6405d2493c91e18b1d8a9d2d71");
        }

        public HtmlEditorModule.HtmlSearchProvider GetHtmlSearchProvider()
        {
            return new HtmlEditorModule.HtmlSearchProvider(Globals);
        }
    }
}
