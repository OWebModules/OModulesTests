﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XMLParserModule;
using XMLParserModule.Models;

namespace OModulesTests.Configuration
{
    public class ExpectationsXMLParser : ExpectationsAll
    {
        public ParseXMLFieldRequest GetParseXMLFieldRequest()
        {
            return new ParseXMLFieldRequest("6272c9d918b24bf9b8b6c575fdbc39b0");
        }

        public XMLParserController GetController()
        {
            return new XMLParserController(Globals);
        }
    }
}
