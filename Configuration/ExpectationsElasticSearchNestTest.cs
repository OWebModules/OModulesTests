﻿using ElasticSearchNestConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests.Configuration
{
    public class ExpectationsElasticSearchNestTest : ExpectationsAll
    {
        public string FilePathImportFile { get; } = @"C:\tools\cygwin\var\tmp\ForumGlobalSettings.json";
        public string ImportIndex { get; } = "foconfiguration";
        public string ImportType { get; } = "configuration";

        
    }
}
