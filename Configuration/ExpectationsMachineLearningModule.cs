﻿using MachineLearningModule;
using MachineLearningModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests.Configuration
{
    public class ExpectationsMachineLearningModule : ExpectationsAll
    {
        public GetDecisionTreeTSonRequest GetDecisionTreeTSonRequest()
        {
            var request = new GetDecisionTreeTSonRequest("16bb3d9cb1194b26b59b653479e2f8a6", new System.Threading.CancellationTokenSource().Token);

            return request;
        }

        public MergeSortByPDFDocRequest GetMergeSortByPDFDocRequest()
        {
            var request = new MergeSortByPDFDocRequest("83bb00e691254c8383dba4b85bc71afc", (new System.Threading.CancellationTokenSource()).Token);

            return request;
        }

        public DecisionTreeController GetDecisionTreeController()
        {
            var controller = new DecisionTreeController(Globals);
            return controller;
        }

        public SortController GetSortController()
        {
            var controller = new SortController(Globals);

            return controller;
        }

        public AssociationAnalysisController GetAssociationAnalysisController()
        {
            var controller = new AssociationAnalysisController(Globals);

            return controller;
        }

        public TrainingsdataController GetTrainingsdataController()
        {
            var controller = new TrainingsdataController(Globals);

            return controller;
        }

        public TrainingsdatenRequest GetWrenkorbTrainingsdatenRequest()
        {
            return new TrainingsdatenRequest("29284e577a5d4615b5c0d11d378b13ce");
        }

        public CreateAssociationStatisticRequest GetCreateAssociationStatisticRequest()
        {
            var request = new CreateAssociationStatisticRequest("d17d750c8b144b5c8f470a15d17153ad", new System.Threading.CancellationTokenSource().Token);
            return request;
        }

    }
}
