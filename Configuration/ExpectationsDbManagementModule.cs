﻿using DatabaseManagementModule;
using DatabaseManagementModule.Models;
using DatabaseManagementModule.Services;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OModulesTests.Configuration
{
    public class ExpectationsDbManagementModule : ExpectationsAll
    {

        public GetColumnsRequest GetRequestWithIntegratedSecurity()
        {
            return new GetColumnsRequest("4923b39853564e1589894c893c43f750", "Test");
        }
        
        public GetColumnsRequest GetRequestWithUserNameAndPassword()
        {
            return new GetColumnsRequest("87af1e1398a64ff9b0f112f962d24674", "Test");
        }

        public CheckConnectionStringRequest GetCheckConnectionStringRequest()
        {
            return new CheckConnectionStringRequest("553b64082b694dcf9d651a398d89aa92");
        }

        public ExportCodeToFileRequest GetExportCodeToFileRequest()
        {
            return new ExportCodeToFileRequest("941e5c7035e84ac5932cd76c1a6e5fc8");
        }

        public DatabaseManagementController GetController()
        {
            return new DatabaseManagementController(Globals);
        }

        public CheckExistanceDBItemsRequest GetCheckExistanceDBItemRequest()
        {
            var request = new CheckExistanceDBItemsRequest("ae84a7c6cf1a4afdb3c0c0e09a6957d3", "Wilbo!9", new System.Threading.CancellationTokenSource().Token);
            return request;
        }

        public MsSQLConnector GetMSSqlConnector()
        {
            return new MsSQLConnector(Globals);
        }

        public MsSqlDbItemModel GetItemForCode()
        {
            return new MsSqlDbItemModel
            {
                DbItem = new clsOntologyItem
                {
                    GUID = "45a582320c544bf1a24ed408ccd04f77",
                    Name = "orgview_EntityDatabaseMap",
                    GUID_Parent = "9403650d2a284a0d9a7ff1d893960701",
                    Type = Globals.Type_Object
                },
                DbSchema = new clsOntologyItem
                {
                    GUID = "981602fb78d34d9c96441811e55bed33",
                    Name = "dbo",
                    GUID_Parent = "f91b2b4f7bbf4e3186594e9e0cf666a4",
                    Type = Globals.Type_Object
                }
            };
        }
        public string GetConnectionStringForGetCodeTest()
        {
            var server = "localhost";
            var instance = "\\SQLEXPRESS";
            var catalog = "ont_db_reports_2";
            var authenticationString = "Integrated Security=True";
            return $@"Data Source={ server }{instance};Initial Catalog={catalog};{authenticationString}";
        }

        public GetMSSqlCodeRequest GetMSSqlCodeRequest()
        {
            return new GetMSSqlCodeRequest("3ba48502a3cf444fb433c0192fd88a9c", "Wilbo!9", (new CancellationTokenSource()).Token);
        }
    }
}
