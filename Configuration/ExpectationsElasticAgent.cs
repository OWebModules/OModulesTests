﻿using OModulesTests.Services;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Services;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests.Configuration
{
    public class ExpectationsElasticAgent : ExpectationsAll
    {
        public ElasticBaseAgent GetElasticAgent()
        {
            var result = new ElasticAgentTest(Globals);
            return result;
        }

        public clsOntologyItem GetRelationTypeBelongingSource()
        {
            return new clsOntologyItem
            {
                GUID = "d34d545e9ddf46cebb6f22db1b7bb025",
                Name = "belonging Source",
                Type = Globals.Type_RelationType
            };
        }

        public List<clsOntologyItem> GetFilterList()
        {
            var filter = new List<string>();

            filter.Add("20190720_133130.jpg");
            filter.Add("20190720_133303.jpg");
            filter.Add("20190720_133317.jpg");
            filter.Add("20190720_135334.jpg");
            filter.Add("20190721_163514.jpg");
            filter.Add("20190730_130101.jpg");
            filter.Add("20190730_130104.jpg");
            filter.Add("20190730_130105.jpg");
            filter.Add("20190730_130447.jpg");
            filter.Add("20190730_130449.jpg");
            filter.Add("20190730_130552.jpg");
            filter.Add("20190730_135344.jpg");
            filter.Add("20190730_145131.jpg");
            filter.Add("20190730_164819.jpg");
            filter.Add("20190730_204830.jpg");
            filter.Add("20190730_204832.jpg");
            filter.Add("20190730_204839.jpg");
            filter.Add("20190730_204848.jpg");
            filter.Add("20190803_174950.jpg");
            filter.Add("20190811_103321.jpg");
            filter.Add("20190811_104050.jpg");
            filter.Add("20190811_104054.jpg");
            filter.Add("20190811_140509.jpg");
            filter.Add("20190812_142904.jpg");
            filter.Add("20190812_142916.jpg");
            filter.Add("20190813_093013.jpg");
            filter.Add("20190817_165454.jpg");
            filter.Add("20190817_172103.jpg");
            filter.Add("20190817_172105.jpg");
            filter.Add("20190907_112723.jpg");
            filter.Add("20190907_113245.jpg");
            filter.Add("20190907_113247.jpg");
            filter.Add("20191006_130331.jpg");
            filter.Add("20191007_191133.jpg");
            filter.Add("20191017_223113.jpg");
            filter.Add("20191018_173305.jpg");
            filter.Add("20191018_173554.jpg");
            filter.Add("20191021_155131.jpg");
            filter.Add("20191102_095652.jpg");
            filter.Add("20191111_125146.jpg");
            filter.Add("20191111_125152.jpg");
            filter.Add("20191111_125937.jpg");
            filter.Add("20191111_130459.jpg");
            filter.Add("20191111_131143.jpg");
            filter.Add("20191111_131149.jpg");
            filter.Add("20191111_131424.jpg");
            filter.Add("20191111_132513.jpg");
            filter.Add("20191111_134534.jpg");
            filter.Add("20191111_160506.jpg");
            filter.Add("20191111_160552.jpg");
            filter.Add("20191111_161905.jpg");
            filter.Add("20191111_161939.jpg");
            filter.Add("20191113_170633.jpg");
            filter.Add("20191113_171136.jpg");
            filter.Add("20191113_171145.jpg");
            filter.Add("20191113_171146.jpg");
            filter.Add("20191113_183755.jpg");
            filter.Add("20191113_183756.jpg");
            filter.Add("20191113_183952.jpg");
            filter.Add("20191113_183953.jpg");
            filter.Add("20191113_183954.jpg");
            filter.Add("20191113_183955.jpg");
            filter.Add("20191113_184510.jpg");
            filter.Add("20191113_184511.jpg");
            filter.Add("20191113_185105.jpg");
            filter.Add("20191113_185107.jpg");
            filter.Add("20191113_185108.jpg");
            filter.Add("20191113_185109.jpg");
            filter.Add("20191113_190342.jpg");
            filter.Add("20191113_190343.jpg");
            filter.Add("20191113_190347.jpg");
            filter.Add("20191117_112149.jpg");
            filter.Add("20191117_112332.jpg");
            filter.Add("20191117_112753.jpg");
            filter.Add("20191117_112811.jpg");
            filter.Add("20191117_112911.jpg");
            filter.Add("20191117_113355.jpg");
            filter.Add("20191129_201202.jpg");
            filter.Add("20191129_201205.jpg");
            filter.Add("20191130_202132.jpg");
            filter.Add("20191201_073425.jpg");
            filter.Add("20191201_074046.jpg");
            filter.Add("20191203_075843.jpg");
            filter.Add("20191205_172138.jpg");
            filter.Add("20191208_182004.jpg");
            filter.Add("20191213_155318.jpg");
            filter.Add("20191214_123141.jpg");
            filter.Add("20191214_123153.jpg");
            filter.Add("20191218_182124.jpg");
            filter.Add("20191218_182132.jpg");
            filter.Add("20191218_182138.jpg");
            filter.Add("20191218_182158.jpg");
            filter.Add("20191218_182559.jpg");
            filter.Add("20191219_201606.jpg");
            filter.Add("20191223_203220.jpg");
            filter.Add("20191223_203222.jpg");
            filter.Add("20191224_110537.jpg");
            filter.Add("20191225_153517.jpg");
            filter.Add("20191225_153518.jpg");
            filter.Add("20191226_090316.jpg");
            filter.Add("20191226_090317.jpg");
            filter.Add("20191227_162350.jpg");
            filter.Add("20191227_164803.jpg");
            filter.Add("20191228_110537.jpg");
            filter.Add("20191228_111434.jpg");
            filter.Add("20191228_121407.jpg");
            filter.Add("20191228_121413.jpg");
            filter.Add("20191228_144911.jpg");
            filter.Add("20191228_153050.jpg");
            filter.Add("20200113_201214.jpg");
            filter.Add("20200117_192253.jpg");
            filter.Add("20200117_192255.jpg");
            filter.Add("20200119_073725.jpg");
            filter.Add("20200119_115143.jpg");
            filter.Add("20200119_115151.jpg");
            filter.Add("20200119_115556.jpg");
            filter.Add("20200119_133040.jpg");
            filter.Add("20200126_181829.jpg");
            filter.Add("20200131_173620.jpg");
            filter.Add("20200131_175850.jpg");
            filter.Add("20200201_120032.jpg");
            filter.Add("20200201_121033.jpg");
            filter.Add("20200201_121214.jpg");
            filter.Add("20200201_121215.jpg");
            filter.Add("20200216_161315.jpg");
            filter.Add("20200216_161348.jpg");
            filter.Add("20200221_163748.jpg");
            filter.Add("20200221_165748.jpg");
            filter.Add("20200229_171224.jpg");
            filter.Add("20200229_171233.jpg");
            filter.Add("20200229_171252.jpg");
            filter.Add("20200229_171304.jpg");
            filter.Add("20200229_171313.jpg");
            filter.Add("20200229_171323.jpg");
            filter.Add("20200229_171331.jpg");
            filter.Add("20200304_200628.jpg");
            filter.Add("20200306_182119.jpg");
            filter.Add("20200306_182133.jpg");
            filter.Add("20200308_102442.jpg");
            filter.Add("20200308_103652.jpg");
            filter.Add("20200308_103659.jpg");
            filter.Add("20200309_181811.jpg");
            filter.Add("20200309_181824.jpg");
            filter.Add("20200314_150340.jpg");
            filter.Add("20200323_185826.jpg");
            filter.Add("20200323_185828.jpg");
            filter.Add("20200328_133740.jpg");
            filter.Add("20200404_121249.jpg");
            filter.Add("20200404_162130.jpg");
            filter.Add("20200404_162132.jpg");
            filter.Add("20200404_162851.jpg");
            filter.Add("20200405_150638.jpg");
            filter.Add("20200405_150644.jpg");
            filter.Add("20200412_110932.jpg");
            filter.Add("20200412_131043.jpg");
            filter.Add("20200412_185057.jpg");
            filter.Add("20200415_190013.jpg");
            filter.Add("20200415_190048.jpg");
            filter.Add("20200415_190050.jpg");
            filter.Add("20200415_190054.jpg");
            filter.Add("20200415_190057.jpg");
            filter.Add("20200415_190100.jpg");
            filter.Add("20200415_190101.jpg");
            filter.Add("20200415_190105.jpg");
            filter.Add("20200415_190115.jpg");
            filter.Add("20200416_172142.jpg");
            filter.Add("20200418_194047.jpg");
            filter.Add("20200418_194136.jpg");
            filter.Add("20200419_104959_001.jpg");
            filter.Add("20200419_104959_002.jpg");
            filter.Add("20200419_104959_003.jpg");
            filter.Add("20200419_104959_004.jpg");
            filter.Add("20200419_104959_005.jpg");
            filter.Add("20200419_104959_006.jpg");
            filter.Add("20200419_104959_007.jpg");
            filter.Add("20200419_104959_008.jpg");
            filter.Add("20200419_104959_009.jpg");
            filter.Add("20200419_104959_010.jpg");
            filter.Add("20200419_104959_011.jpg");
            filter.Add("20200419_104959_012.jpg");
            filter.Add("20200419_104959_013.jpg");
            filter.Add("20200419_104959_014.jpg");
            filter.Add("20200419_104959_015.jpg");
            filter.Add("20200419_133853.jpg");
            filter.Add("20200419_133855.jpg");
            filter.Add("20200419_133859.jpg");
            filter.Add("20200419_134212.jpg");
            filter.Add("20200419_134437.jpg");
            filter.Add("20200419_134441.jpg");
            filter.Add("20200419_143816.jpg");
            filter.Add("20200419_143821.jpg");
            filter.Add("20200502_141728.jpg");
            filter.Add("20200504_180025.jpg");
            filter.Add("20200511_223259.jpg");
            filter.Add("20200511_223309.jpg");
            filter.Add("20200515_093156.jpg");
            filter.Add("20200515_093241.jpg");
            filter.Add("20200515_093427.jpg");
            filter.Add("20200515_173343.jpg");
            filter.Add("20200516_113052.jpg");
            filter.Add("20200516_113056.jpg");
            filter.Add("20200516_113101.jpg");
            filter.Add("20200516_113157.jpg");
            filter.Add("20200529_153808.jpg");
            filter.Add("20200531_125746.jpg");
            filter.Add("20200606_103503.jpg");
            filter.Add("20200606_103504.jpg");
            filter.Add("20200606_103505.jpg");
            filter.Add("20200606_103507.jpg");
            filter.Add("20200611_155733.jpg");
            filter.Add("20200611_155753.jpg");
            filter.Add("20200618_213720.jpg");
            filter.Add("20200619_093520.jpg");
            filter.Add("20200619_093546.jpg");
            filter.Add("20200619_111402.jpg");
            filter.Add("20200619_111957.jpg");
            filter.Add("20200619_111958.jpg");
            filter.Add("20200619_114306.jpg");
            filter.Add("20200619_181911.jpg");
            filter.Add("20200619_191932.jpg");
            filter.Add("20200619_192336.jpg");
            filter.Add("20200619_192337.jpg");
            filter.Add("20200619_192339.jpg");
            filter.Add("20200619_192342.jpg");
            filter.Add("20200621_183858.jpg");
            filter.Add("20200622_162943.jpg");
            filter.Add("20200625_150428.jpg");
            filter.Add("20200626_163259.jpg");
            filter.Add("20200627_204311.jpg");
            filter.Add("20200704_125754.jpg");
            filter.Add("20200704_125755.jpg");
            filter.Add("20200709_131638.jpg");
            filter.Add("20200713_130738.jpg");
            filter.Add("20200713_130751.jpg");
            filter.Add("20200713_130759.jpg");
            filter.Add("20200713_135523.jpg");
            filter.Add("20200713_135527(0).jpg");
            filter.Add("20200713_135527.jpg");
            filter.Add("20200713_141937.jpg");
            filter.Add("20200713_142004.jpg");
            filter.Add("20200713_143816.jpg");
            filter.Add("20200716_135041.jpg");
            filter.Add("20200716_135126.jpg");
            filter.Add("20200718_074831.jpg");
            filter.Add("20200718_074837.jpg");
            filter.Add("20200718_074909.jpg");
            filter.Add("20200718_074927.jpg");
            filter.Add("20200718_075029.jpg");
            filter.Add("20200718_081814.jpg");
            filter.Add("20200718_083700.jpg");
            filter.Add("20200718_085650.jpg");
            filter.Add("20200718_090331.jpg");
            filter.Add("20200718_090333.jpg");
            filter.Add("20200718_204339.jpg");
            filter.Add("20200719_125526.jpg");
            filter.Add("20200719_125639.jpg");
            filter.Add("20200719_125702.jpg");
            filter.Add("20200719_125944.jpg");
            filter.Add("20200719_125945.jpg");
            filter.Add("20200719_130857.jpg");
            filter.Add("20200719_131039.jpg");
            filter.Add("20200722_142552.jpg");
            filter.Add("20200722_160612.jpg");
            filter.Add("20200723_113457.jpg");
            filter.Add("20200723_193712.jpg");
            filter.Add("20200723_193713.jpg");
            filter.Add("20200723_204817.jpg");
            filter.Add("20200723_204823.jpg");
            filter.Add("20200723_204827.jpg");
            filter.Add("20200724_180515.jpg");
            filter.Add("20200724_180646.jpg");
            filter.Add("20200725_184309.jpg");
            filter.Add("20200727_210657.jpg");
            filter.Add("20200728_131910.jpg");
            filter.Add("20200730_120225.jpg");
            filter.Add("20200730_120313.jpg");
            filter.Add("20200731_121836.jpg");
            filter.Add("20200731_125513.jpg");
            filter.Add("20200731_125818.jpg");

            return filter.Select(name => new clsOntologyItem
            {
                Name = name,
                GUID_Parent = "6eb4fdd32e254886b288e1bfc2857efb",
                Type = "Object"
            }).ToList();
        }

        public List<clsObjectRel> GetRelationsImagesToFolder(List<clsObjectRel> imagesToFiles)
        {
            var folder = new clsOntologyItem
            {
                GUID = "7a1c09d90d8d4234a35c469dac4070c0",
                Name = "S8",
                GUID_Parent = "f47a3e1c50b44666a41de975597c9adb",
                Type = Globals.Type_Object
            };

            var relationType = new clsOntologyItem
            {
                GUID = "e07469d9766c443e85266d9c684f944f",
                Name = "belongs to",
                Type = Globals.Type_RelationType
            };

            var relationConfig = new clsRelationConfig(Globals);

            var result = imagesToFiles.Select(imgFile => relationConfig.Rel_ObjectRelation(new clsOntologyItem
            {
                GUID = imgFile.ID_Object,
                Name = imgFile.Name_Object,
                GUID_Parent = imgFile.ID_Parent_Object,
                Type = Globals.Type_Object
            }, folder, relationType)).ToList();

            return result;
        }

        public string RelationTypeIdExisting
        {
            get
            {
                return "e07469d9766c443e85266d9c684f944f";
            }
        }

        public string RelationTypeNotIdExisting
        {
            get
            {
                return "xxaaxx";
            }
        }
    }
}
