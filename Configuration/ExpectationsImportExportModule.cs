﻿using ImportExport_Module;
using ImportExport_Module.Models;
using OntologyItemsModule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OModulesTests.Configuration
{
    public class ExpectationsImportExportModule : ExpectationsAll
    {
        public string ExportPath => "F:\\Backup\\EsExport\\OntoExport";
        public int PacketCountBig => 5000;
        public int PacketCountSmall => 500;
        public string ExportKey => "Tests";

        public string AssemblyPath => @"F:\git\OModules\FileManagementModule\FileManagementModule.Config.dll";

        public string ExcelFile => @"C:\Users\kollert\Desktop\DevOps\DevOps Configuration Rollout FO 4.0.xlsx";

        public string JsonFileForNamespace => @"F:\fo\DEPLOYMENT\Configuration\PLAY_ForumGlobalSettings_AT.FV.json";

        public AssemblyImporter GetAssemblyImporter()
        {
            return new AssemblyImporter(Globals);
        }

        public ExcelImporter GetExcelImporter()
        {
            return new ExcelImporter(Globals);
        }

        public JsonExporter GetJsonExporter()
        {
            return new JsonExporter(Globals);
        }

        public CosmosDBConnector GetCosmosDBConnector()
        {
            return new CosmosDBConnector(Globals);
        }

        public OntologyConnector GetOntologyConnector()
        {
            return new OntologyConnector(Globals);
        }

        public MigrateWholeGraphToCosmosDbRequest GetMigrateWholeGraphToCosmosDbRequest()
        {
            return new MigrateWholeGraphToCosmosDbRequest("b28a1c26a763415083478fcb28f53250");
        }

        public ExportEsIndexRequest GetExportEsIndexRequest()
        {
            return new ExportEsIndexRequest(Globals.Server, Globals.Port, "htmleditorcontroller", "code", @"C:\Inbox\HtmlEditor", new CancellationTokenSource().Token) { ExportFilePrefix = "HtmlEditor_"};
        }

        public CompareImpExpRequest GetCompareImpExpRequest()
        {
            return new CompareImpExpRequest(@"F:\Backup\EsExport\OntoExport", false);
        }
    }
}
