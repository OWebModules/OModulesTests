﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsTasksModules;
using WindowsTasksModules.Models;
using WrikeConnectorModule;

namespace OModulesTests.Configuration
{
    public class ExpectationsWindowsTasksController : ExpectationsAll
    {

        public ImportTasksRequest GetImportTaskRequest()
        {
            return new ImportTasksRequest("9c4826a06080410c98cbde03340f09fb");
        }

        public WindowsTasksConnector GetConnector()
        {
            return new WindowsTasksConnector(Globals);
        }
    }
}
