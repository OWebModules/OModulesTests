﻿using MediaViewerModule.Connectors;
using MediaViewerModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests.Configuration
{
    public class ExpectationsMediaViewerModule : ExpectationsAll
    {
        public SearchPDFItemsRequest GetSearchPDFItemsRequest()
        {
            var request = new SearchPDFItemsRequest("Schwimmhalle")
            {
                PageSize = 100,
            };
            return request;
        }

        public PDFSearchController GetPDFSearchController()
        {
            return new PDFSearchController(Globals);
        }
        public AssociateMediaItemsRequest GetAssociateMediaItemsRequest()
        {
            var request = new AssociateMediaItemsRequest(new List<AssociateMediaItemsRequestFilter>
            {
                new AssociateMediaItemsRequestFilter
                {
                    RegexName = true,
                    FileItem = new OntologyClasses.BaseClasses.clsOntologyItem
                    {
                        Name = @"\d-\d{2}\s(Ostwind_|Mary Hat Ausgang_|Lachgas_|Miss Larks Andy_|Die Tanzende Kuh_|Ein Schimmer Dienstag_|Die Vogelfrau_|Mrs Corry_|Die Geschichte Von Barbara Und_|Vollmond_|Weichnachtseinkäufe_|Westwind_).*"
                    }
                }
            }, MediaViewerModule.Services.AssociationType.MediaItem)
            {
                RefItem = new OntologyClasses.BaseClasses.clsOntologyItem
                {
                    GUID = "5d9c55e09f5f41eab71029682f40505c",
                    Name = "Mary Poppins",
                    GUID_Parent = "ac1927e95b764889823a0929e6768f11",
                    Type = Globals.Type_Object
                }
            };

            return request;
        }

        public GetImageMapRequest GetImageMapRequest()
        {
            return new GetImageMapRequest("7e85eaad1e0a4cb3bf7565d7cb43ba67", @"F:\git\OModules\OModules\Resources\UserGroupRessources", "http://localhost:59200/Resources/UserGroupRessources");
        }

        public OCRImagesRequest GetOCRImagesRequest()
        {
            return new OCRImagesRequest("c50064b55b3244af966bab54582ad740");
        }

        public SaveBookmarksRequest GetSaveBookmarksRequest()
        {
            return new SaveBookmarksRequest(new List<MediaBookmark>
            {
                new MediaBookmark
                {
                    IdMediaItem = "873008229a5240fca2224dbce67f690c",
                    Created = DateTime.Now,
                    LogState = new OntologyClasses.BaseClasses.clsOntologyItem
                    {
                        GUID = "11302e9e3774402ea2096206d114130c",
                        Name ="Position",
                        GUID_Parent = "1d9568afb6da49908f4d907dfdd30749",
                        Type = Globals.Type_Object
                    },
                    User = new OntologyClasses.BaseClasses.clsOntologyItem
                    {
                        GUID = "74f38566eda64c76b862e839b6a8d07d",
                        Name = "TassiloK.Prj",
                        GUID_Parent = "c441518dbfe04d55b538df5c5555dd83",
                        Type = Globals.Type_Object

                    },
                    NameBookmark = DateTime.Now.ToString(),
                    Second = 26.943922,
                    References = new List<OntologyClasses.BaseClasses.clsOntologyItem>
                    {
                        new OntologyClasses.BaseClasses.clsOntologyItem
                        {
                            GUID = "360d1362dd494386bf121278fb5c20ec",
                            Name = "Friedrich Kittler",
                            GUID_Parent = "985d0411eabe4c3f9a4a6ebd92b0aa99",
                            Type = Globals.Type_Object
                        }
                    }
                }
            });
        }

        public GetMediaBookmarkModelRequest GetByUserRequest()
        {
            return new GetMediaBookmarkModelRequest(new List<string>
                {
                    "74f38566eda64c76b862e839b6a8d07d"
                }, MediaBookmarkModelRequestType.ByUserIds);
        }

        public GetMediaBookmarkModelRequest GetByBookmarkRequest()
        {
            return new GetMediaBookmarkModelRequest(new List<string>
                {
                    "0938766d70c5428489c6d41e61c25020"
                }, MediaBookmarkModelRequestType.ByBookmarkIds);
        }

        public OCRController GetOCRController()
        {
            return new OCRController(Globals);
        }
        public ImageMapController GetImageMapController()
        {
            return new ImageMapController(Globals);
        }

        public MediaTaggingController GetMediaTaggingcontroller()
        {
            return new MediaTaggingController(Globals);
        }

        public MediaViewerConnector GetConnector()
        {
            var connector = new MediaViewerConnector(Globals);
            return connector;
        }
    }
}
