﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MailsToElastic.Models;
using MailModule.Models;
using MailLib.Models;
using SyncStarterModule.Controller;
using SyncStarterModule.Model;

namespace OModulesTests.Configuration
{
    public class ExpectationsSyncStarterModule : ExpectationsAll
    {
        public ExtractPasswordRequest GetExtractPasswordRequest()
        {
            return new ExtractPasswordRequest("74f38566eda64c76b862e839b6a8d07d", "test");
        }
        public MaintenanceModuleController GetMainenanceController()
        {
            return new MaintenanceModuleController(Globals);
        }

    }
}
