﻿using MailsToElastic;
using MailModule;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MailsToElastic.Models;
using MailModule.Models;
using MailLib.Models;
using System.Threading;

namespace OModulesTests.Configuration
{
    public class ExceptationsMailModule : ExpectationsAll
    {
        public clsOntologyItem OItemImapConfiguration
        {
            get
            {
                return new clsOntologyItem
                {
                    GUID = "2f94eb527abe412994df44ded87f7c96",
                    Name = "Gmx",
                    GUID_Parent = "28db5992c6634d7ab0da05692d601d49",
                    Type = Globals.Type_Object
                };
            }
        }

     
        public MailsToElasticConnector GetMailToElasticConnector()
        {
            return new MailsToElasticConnector();
        }

        public MailConnector GetMailConnector()
        {
            return new MailConnector(Globals);
        }

        public GetMailsRequest GetMailsRequest(MailConnection connection)
        {
            return new GetMailsRequest
            {
                ElasticIndex = connection.ElasticsearchConfig.NameIndex,
                ElasticServer = connection.ElasticsearchConfig.NameServer,
                ElasticPort = connection.ElasticsearchConfig.Port,
                ElasticType = connection.ElasticsearchConfig.NameType
            };
        }
        
        public MailsToElasticConnector GetMailsToElasticConnector()
        {
            return new MailsToElasticConnector();
        }

        public ExportToJsonRequest GetExportToJsonRequest()
        {
            return new ExportToJsonRequest("d604f37a7e2f44b7a996c134b5850996", (new CancellationTokenSource()).Token);
        }
    }
}
