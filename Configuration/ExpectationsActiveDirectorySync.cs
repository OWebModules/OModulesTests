﻿using ActiveDirectorySyncModule;
using ActiveDirectorySyncModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests.Configuration
{
    public class ExpectationsActiveDirectorySync : ExpectationsAll
    {
        public GetGroupMembersRequest GetRequestGroupMembers()
        {
            return new GetGroupMembersRequest("DE-FVHG-FBF-FO-PROD.*WRITE.*", "OU=DE.FVHG,OU=FORUM Order,OU=Forum Business Framework,OU=Applications,OU=DE.Merching,OU=DE,DC=forum,DC=global", "forum.global");
            //return new GetGroupMembersRequest("DE-FVHG-FBF-FO-PROD-READ", "OU=DE.FVHG,OU=FORUM Order,OU=Forum Business Framework,OU=Applications,OU=DE.Merching,OU=DE,DC=forum,DC=global", "forum.global");
        }

        public GetUserGroupsRequest GetUserGroupsRequest()
        {
            return new GetUserGroupsRequest("a06207b50cf04a2c81dab1ce9c12903f");
        }

        public GetGroupMembersRequest GetRequestGroupMembersSrc()
        {
            return new GetGroupMembersRequest("DE-FVHG-FBF-FO-PROD.*WRITE.*", "OU=DE.FVHG,OU=FORUM Order,OU=Forum Business Framework,OU=Applications,OU=DE.Merching,OU=DE,DC=forum,DC=global", "forum.global");
        }

        public GetGroupMembersRequest GetRequestGroupMembersDst()
        {
            return new GetGroupMembersRequest("DE-FVHG-FBF-FO-PROD-READ", "OU=DE.FVHG,OU=FORUM Order,OU=Forum Business Framework,OU=Applications,OU=DE.Merching,OU=DE,DC=forum,DC=global", "forum.global");
        }

        public CompareGroupMembersRequest GetCompareRequest(List<GroupWithMembers> groupsWithMembersSrc, List<GroupWithMembers> groupsWithMembersDst)
        {
            return new CompareGroupMembersRequest(groupsWithMembersSrc, groupsWithMembersDst);
        }

        public ExportGroupMembersCSVRequest GetExportGroupMembersRequest(List<GroupWithMembers> groupsWithMembers)
        {
            return new ExportGroupMembersCSVRequest(groupsWithMembers, @"C:\Temp\group.txt");
            //return new ExportGroupMembersCSVRequest(groupsWithMembers, @"C:\Temp\group1.txt");
        }

        public ComQueryRequest GetRequestPartners(bool allPartners = false)
        {
            if (!allPartners)
            {
                return new ComQueryRequest { IdPartners = new List<string> { "6c80be2729794ebeb2e904502d482c50" } };
            }
            else
            {
                return new ComQueryRequest { AllPartners = true };
            }
            
        }
        public ActiveDirectorySyncController GetController()
        {
            return new ActiveDirectorySyncController(Globals);
        }
    }
}
