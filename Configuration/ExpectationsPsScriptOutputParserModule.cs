﻿using PsScriptOutputParserModule;
using PsScriptOutputParserModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OModulesTests.Configuration
{
    public class ExpectationsPsScriptOutputParserModule : ExpectationsAll
    {

        public ScriptOutputRequest GetRequest()
        {
            var tokenSource = GetTokenSource();
            return new ScriptOutputRequest("c2ece1d9a8fe47c69995590b3bf113b0", "6afcab29b5004467a22bb5604dd77106", "Test", tokenSource.Token);
            
        }

        public CancellationTokenSource GetTokenSource()
        {
            return new CancellationTokenSource();
        }
        public PsScriptOutputParserController GetController()
        {
            return new PsScriptOutputParserController(Globals);
        }
    }
}
