﻿using OntologyClasses.BaseClasses;
using OntologyItemsModule;
using OntologyItemsModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OModulesTests.Configuration
{
    public class ExpectationsObjectTree : ExpectationsAll
    {
        public string GetIdClass()
        {
            return "30cbc6e89c0f47d6920c97fdc40ea1de";  //Report
        }

        public string GetIdRelationType()
        {
            return "e971160347db44d8a476fe88290639a4"; //contains
        }

        public string GetSearchString()
        {
            return "ToDo";
        }

        public string GetIdDirection()
        {
            return Globals.Direction_LeftRight.GUID;
        }

        public ObjectTreeController GetController()
        {
            var controller = new ObjectTreeController(Globals);
            return controller;
        }
    }
}
