﻿using JsonModule;
using JsonModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests.Configuration
{
    public class ExpectationsJsonModule : ExpectationsAll
    {
        public JsonController GetConverter()
        {
            return new JsonController(Globals);
        }

        public GetEsIndexItemRequest GetEsIndexItemRequest()
        {
            var request = new GetEsIndexItemRequest("_id", "069eb56045b84d4e807fc4e2e2e011de")
            {
                IndexTypeIdItem = new IndexTypeIdItem
                {
                    IdEsIndex = "20a689df658c42a3be30a9e921115ad9",
                    IdEsType = "51ca984f8e8047dd92f491979672deec"
                }
            };
            return request;

        }

        public ImportToJsonAttributeRequest GetImportToJsonAttributeRequest()
        {
            var request = new ImportToJsonAttributeRequest("b743c9bc7f9f48f186290975d75fddbf");
            return request;

        }

        public ConvertJsonToSchemaRequest GetJsonToSchemaRequest()
        {
            var request = new ConvertJsonToSchemaRequest("5242c285b5fb4b869744825006bbd088");
            return request;
        }
    }
}
