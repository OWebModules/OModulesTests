﻿using BarcodeModule;
using BarcodeModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests.Configuration
{
    public class ExpecationsBarCodeModule : ExpectationsAll
    {
        public GetQRCodeImageRequest GetQRCodeRequest()
        {
            return new GetQRCodeImageRequest("C:\\Temp", "Das ist ein Test");
        }

        public BarCodeController GetController()
        {
            return new BarCodeController(Globals);
        }
    }
}
