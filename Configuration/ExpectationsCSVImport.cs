﻿using BCMCSVImporter;
using BCMCSVImporter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests.Configuration
{
    public class ExpectationsCSVImport : ExpectationsAll
    {
        public CSVManipulation GetController()
        {
            return new CSVManipulation(Globals);
        }
        public CompareCSVRequest GetCSVCompareRequestAddresses()
        {
            var request = new CompareCSVRequest("0dda1973d02b48cfaa99c72466413ae8");
            //var request = new CompareCSVRequest(@"C:\Temp\Invoice_and_delivery_addresses_FERTIG.csv.txt",
            //        @"C:\Temp\NewAddresses Playground.csv", @"C:\Temp\CompareAddresses.csv", new List<CSVFieldNameMap>
            //        {
            //            new CSVFieldNameMap
            //            {
            //                FieldNameSrc = "address_id",
            //                FieldNameDst = "address_id_old"
            //            },
            //            new CSVFieldNameMap
            //            {
            //                FieldNameSrc = "customer_no",
            //                FieldNameDst = "customer_no_old"
            //            }
            //        }, new List<FieldType>
            //        {
            //            new FieldType
            //            {
            //                FieldName = "lock",
            //                FieldTypeItem = typeof(DateTime)
            //            },
            //            new FieldType
            //            {
            //                FieldName = "date_created",
            //                FieldTypeItem = typeof(DateTime)
            //            },
            //            new FieldType
            //            {
            //                FieldName = "date_undeliverable",
            //                FieldTypeItem = typeof(DateTime)
            //            },
            //            new FieldType
            //            {
            //                FieldName = "vat_valid_from",
            //                FieldTypeItem = typeof(DateTime)
            //            },
            //            new FieldType
            //            {
            //                FieldName = "date_modified",
            //                FieldTypeItem = typeof(DateTime)
            //            },
            //            new FieldType
            //            {
            //                FieldName= "valid_from",
            //                FieldTypeItem = typeof(DateTime)
            //            },
            //            new FieldType
            //            {
            //                FieldName="date_last_vat_validation",
            //                FieldTypeItem = typeof(DateTime)
            //            }

            //        });

            return request;

        }

        public CompareCSVRequest GetCSVCompareRequestCustomer()
        {
            var request = new CompareCSVRequest("3c24c48a2dde405ea7367a5fd7846333");
            //var request = new CompareCSVRequest(@"C:\Temp\Customers_invoice_and_delivery_FERTIG.csv.txt",
            //        @"C:\Temp\NewCustomer Playground.csv", @"C:\Temp\CompareCustomer.csv", new List<CSVFieldNameMap>
            //        {
            //            new CSVFieldNameMap
            //            {
            //                FieldNameSrc = "customer_id",
            //                FieldNameDst = "customer_id_old"
            //            },
            //            new CSVFieldNameMap
            //            {
            //                FieldNameSrc = "customer_no",
            //                FieldNameDst = "customer_no_old"
            //            },
            //            new CSVFieldNameMap
            //            {
            //                FieldNameSrc = "address_id",
            //                FieldNameDst = "address_id_old"
            //            }
            //        }, new List<FieldType>());

            return request;

        }

        public CompareCSVRequest GetCSVCompareRequestCustomerAttributes()
        {
            var request = new CompareCSVRequest("30b307c72f54489eb29a4416dcbb4a3a");
            //var request = new CompareCSVRequest(@"C:\Temp\Customer_document_attribute_FERTIG.csv",
            //        @"C:\Temp\NewCustomerAttributes Playground.csv", @"C:\Temp\CompareCustomerAttributes.csv", new List<CSVFieldNameMap>
            //        {
            //            new CSVFieldNameMap
            //            {
            //                FieldNameSrc = "customer_id",
            //                FieldNameDst = "customer_id_old"
            //            },
            //            new CSVFieldNameMap
            //            {
            //                FieldNameSrc = "customer_no",
            //                FieldNameDst = "customer_no_old"
            //            }
            //        }, new List<FieldType>());

            return request;

        }

        public CompareCSVRequest GetCSVCompareRequestContacts()
        {
            var request = new CompareCSVRequest("5ee69f35bbdc4fb39672937515ac7296");
            //var request = new CompareCSVRequest(@"C:\Temp\Contacts FERTIG.csv",
            //        @"C:\Temp\NewContacts Playground.csv", @"C:\Temp\CompareContacts.csv", new List<CSVFieldNameMap>
            //        {
            //            new CSVFieldNameMap
            //            {
            //                FieldNameSrc = "address_id",
            //                FieldNameDst = "address_id_old"
            //            },
            //            new CSVFieldNameMap
            //            {
            //                FieldNameSrc = "customer_no",
            //                FieldNameDst = "customer_no_old"
            //            }
            //        }, new List<FieldType>
            //        {
            //            new FieldType
            //            {
            //                FieldName = "date_created",
            //                FieldTypeItem = typeof(DateTime)
            //            },
            //            new FieldType
            //            {
            //                FieldName = "date_modified",
            //                FieldTypeItem = typeof(DateTime)
            //            }
            //        })
            //{
            //    ExcludeNames = new List<string> { "contact_id" }
            //};

            return request;

        }

        public CompareCSVRequest GetCSVCompareRequestSubscriptions()
        {
            var request = new CompareCSVRequest("a0b458686c6542c7a5606c2cd58bf7b1");

            return request;

        }
    }
}
