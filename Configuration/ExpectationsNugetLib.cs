﻿using NugetPackageLib;
using NugetPackageLib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests.Configuration
{
    public class ExpectationsNugetLib : ExpectationsAll
    {

        public CreatePackageRequest GetCreatePackageRequest()
        {
            return new CreatePackageRequest("762421cd5d47460cafbf94c6eccf031f");
        }
        public PackageCreator GetPackageCreator()
        {
            return new PackageCreator(Globals);
        }
    }
}
