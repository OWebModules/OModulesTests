﻿using GeminiConnectorModule;
using GeminiConnectorModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests.Configuration
{
    public class ExpectationsGeminiConnectorModule: ExpectationsAll
    {
        public SyncIssuesRequest GetSyncIssuesRequest()
        {
            return new SyncIssuesRequest("c7b8acfe2d1d48149867830d38d74f17", "49231941dfa546999ee9c114365d9ce4", "Test");
        }

        public GeminiConnector GetConnector()
        {
            return new GeminiConnector(Globals);
        }
    }
}
