﻿using MediaStore_Module;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests.Configuration
{
    public class ExpectationsMeidaStore : ExpectationsAll
    {
        public clsOntologyItem FileSystemObjectUNCFile
        {
            get
            {
                return new clsOntologyItem
                {
                    GUID = "b146b15986c64ecebbd55b5d2d7de635",
                    Name = "MOV_0083.mp4",
                    GUID_Parent = "6eb4fdd32e254886b288e1bfc2857efb",
                    Type = Globals.Type_Object
                };
            }
        }

        public clsOntologyItem FileSystemObjectDriveFile
        {
            get
            {
                return new clsOntologyItem
                {
                    GUID = "672cf4d5cddb440585204b296a8f8147",
                    Name = "omodules.de.pfx",
                    GUID_Parent = "6eb4fdd32e254886b288e1bfc2857efb",
                    Type = Globals.Type_Object
                };
            }
        }

        public FileWorkManager GetFileWorkConnector()
        {
            return new FileWorkManager(Globals);
        }
    }
}
