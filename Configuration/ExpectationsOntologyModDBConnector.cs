﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlEditorModule.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;

namespace OModulesTests.Configuration
{
    public class ExpectationsOntologyModDBConnector : ExpectationsAll
    {
        public List<string> ObjectIds
        {
            get
            {
                var result = new List<string>
                {
                    "e857474c9d0e41dd9b1bc597c0d6d98b",
                    "d0e4de0864f54b739d77d8d37579da28",
                    "705bb40a66a04b9b9d315908514ca0ba"
                };

                return result;
            }
            
        }

        public List<string> ClassIds
        {
            get
            {
                var result = new List<string>
                {
                    "dc4de8c5670241a9aa3f8155f433a471",
                    "ba687f94b39b4407bbfe870ba3605a0c",
                    "619c2a339e484ed0b25a3c1b65f9a27d"
                };

                return result;
            }
        }

        public List<string> AttributeTypeIds
        {
            get
            {
                var result = new List<string>
                {
                    "431643c94f1e47d294fb0e71ef817ad8",
                    "551d9dbf628a4e3187bfee458539f8c0",
                    "f01ac0d8e2f845a88cd4ca4711676bdc"
                };

                return result;
            }
        }

        public List<string> RelationTypesIds
        {
            get
            {
                var result = new List<string>
                {
                    "273b80735db645dea908ce4bf346b071",
                    "872b26378d7041f68c6708e3fdac0ff8",
                    "9a588eab7e904a36a49d969a46cd535a"
                };

                return result;
            }
        }

        public List<string> DifferentIds
        {
            get
            {
                var result = new List<string>
                {
                    "e857474c9d0e41dd9b1bc597c0d6d98b",
                    "dc4de8c5670241a9aa3f8155f433a471",
                    "431643c94f1e47d294fb0e71ef817ad8",
                    "273b80735db645dea908ce4bf346b071"
                };

                return result;
            }
            
        }

        public List<string> InvalidId
        {
            get
            {
                var result = new List<string>
                {
                    "5dc6fb84258642ef98cdeb153ee384b2"
                };

                return result;
            }
        }

        public OntologyModDBConnector GetController()
        {
            return new OntologyModDBConnector(Globals);
        }
    }
}
