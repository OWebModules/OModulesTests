﻿using BillModule;
using BillModule.Models;
using BillModule.VoucherParser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests.Configuration
{
    public class ExpectationsBillModule : ExpectationsAll
    {

        public GetTransactionsRequest GetRequest()
        {
            return new GetTransactionsRequest(new OntologyClasses.BaseClasses.clsOntologyItem
            {
                GUID = "dbc3024144a6455ab48cc0cf59ee3c73",
                Name = "Tassilo Volker Koller",
                GUID_Parent = "a2d7d0fe04954afe85e11dcb88a27546",
                Type = Globals.Type_Object
            }, null);
            
        }
        public TransactionConnector GetController()
        {
            return new TransactionConnector(Globals);
        }

        public List<ParserInput> GetReweBelege()
        {
            return new List<ParserInput>
            {
                new ParserInput
                {
                    IdReference = "05785f97fdae4bf0890fbac480531ad7",
                    Template = @"



Wallbergstr. 16
86415 Mering
UID Nr.: DE812706034
EUR
BUTTERCROISSANT 1,38 B
2 Stk x 0,69
JOGHURTDRINK ERD 0,95 B
DUPLO BIG PACK 3,79 B
MIN.WA.CLASS.PET 0,55 A
PFAND 0,15 EUR 0,15 A *
--------------------------------------
SUMME EUR 6,82
======================================
Geg. EC-Cash EUR 6,82

* * Kundenbeleg * *
Datum: 13.09.2022
Uhrzeit: 12:13:51 Uhr
Beleg-Nr. 1892
Trace-Nr. 442311
Kartenzahlung
Kontaktlos
girocard
Nr. ###############2721 0005
Terminal-ID 56007040
Pos-Info 00 073 00
AS-Zeit 13.09. 12:13 Uhr
Betrag EUR 6,82
Zahlung erfolgt

Steuer % Netto Steuer Brutto
A= 19,0% 0,59 0,11 0,70
B= 7,0% 5,72 0,40 6,12
Gesamtbetrag 6,31 0,51 6,82

TSE-Signatur: eyyJZj0FMpEucyoMxdZXXZR5Ewwh4KXmy
FkJh/233e3Tv5FBqu46DnkHA5/i9GrXAM
2P73VvE+ZCkYyx9EPpYM+uYnCgWaphDV1
smzoGxR4yoly6whRSohoO7O5okHZ3
TSE-Signaturzähler: 1304841
TSE-Transaktion: 625177
TSE-Start: 2022-09-13T12:16:20.000
TSE-Stop: 2022-09-13T12:16:39.000
Seriennnummer Kasse: REWE:00:01:2e:62:b8:4b:00
13.09.2022 12:16 Bon-Nr.:5175
Markt:0568 Kasse:4 Bed.:171717
****************************************

Ihre REWE PAYBACK Vorteile heute
PAYBACK Karten-Nr.: #########9437
Punktestand vor Einkauf: 2.130
Punktestand entspricht: 21,30 EUR

Sie erhalten 3 PAYBACK Punkte auf
einen PAYBACK Umsatz von 6,67 EUR!

Jetzt mit PAYBACK Punkten bezahlen!
Einfach REWE Guthaben am Service-Punkt

aufladen.

Für die mit * gekennzeichneten Produkte
erhalten Sie leider keine Rabatte
oder PAYBACK Punkte.

****************************************

REWE-Markt GmbH

Für Sie da:
MO - SA 7:00 bis 20:00 Uhr

Sie haben Fragen?
Antworten gibt es unter www.rewe.de"
                },
                new ParserInput
                {
                    IdReference = "bb7ef5bef405489d9273363d7ea14911",
                    Template = @"


R E W E
Ohmstr. 2-6
86415 Mering
UID Nr.: DE812706034
EUR
BIO SCHINK.WURST 1,49 B
AMERICAN SANDW. 1,19 B
JOGHURTDRINK ERD 0,95 B
LIMETTE PET 0,89 A
PFAND 0,15 EUR 0,15 A *
--------------------------------------
SUMME EUR 4,67
======================================
Geg. EC-Cash EUR 4,67

* * Kundenbeleg * *
Datum: 08.09.2022
Uhrzeit: 12:29:22 Uhr
Beleg-Nr. 6845
Trace-Nr. 269163
Kartenzahlung
Contactless
girocard
Nr. ###############2721 0005
gültig bis 12/24
Terminal-ID 56006849
Pos-Info 00 075 00
AS-Zeit 08.09. 12:29 Uhr
Betrag EUR 4,67
Zahlung erfolgt

Steuer % Netto Steuer Brutto
A= 19,0% 0,87 0,17 1,04
B= 7,0% 3,39 0,24 3,63
Gesamtbetrag 4,26 0,41 4,67

TSE-Signatur: S1hy52R0TcSItvUfoWmKVnRVxW4PNon7R
Jir0lomEnbjDY/qLpqgWGy8ikiY4BWNbQ
B1sE4ZzJPReWjYfxPUxcdI7wLDz1AnbRp
NSrMypJR+EvW/1qc8FT1cdsJ7Tm9l
TSE-Signaturzähler: 1019098
TSE-Transaktion: 480511
TSE-Start: 2022-09-08T12:29:29.000
TSE-Stop: 2022-09-08T12:29:43.000
Seriennnummer Kasse: REWE:b4:2e:99:2b:74:c9:00
08.09.2022 12:29 Bon-Nr.:9847
Markt:0488 Kasse:3 Bed.:282828
****************************************

Ihre REWE PAYBACK Vorteile heute
PAYBACK Karten-Nr.: #########9437
Punktestand vor Einkauf: 1.988
Punktestand entspricht: 19,88 EUR

Sie erhalten 2 PAYBACK Punkte auf
einen PAYBACK Umsatz von 4,52 EUR!

Jetzt mit PAYBACK Punkten bezahlen!
Einfach REWE Guthaben am Service-Punkt

aufladen.

Für die mit * gekennzeichneten Produkte
erhalten Sie leider keine Rabatte
oder PAYBACK Punkte.

****************************************

REWE Markt GmbH

Für Sie da:
MO - SA 7:00 bis 20:00 Uhr

Sie haben Fragen?
Antworten gibt es unter www.rewe.de"
                },
                new ParserInput
                {
                    IdReference = "5cb760334ee0480b8d9d5f736b825ce0",
                    Template = @"



Wallbergstr. 16
86415 Mering
UID Nr.: DE812706034
EUR
BUTTERCROISSANT 1,38 B
2 Stk x 0,69
JOGHURTDRINK ERD 0,95 B
MIN.WA.CLASS.PET 0,55 A
PFAND 0,15 EUR 0,15 A *
--------------------------------------
SUMME EUR 3,03
======================================
Geg. EC-Cash EUR 3,03

* * Kundenbeleg * *
Datum: 05.09.2022
Uhrzeit: 12:13:12 Uhr
Beleg-Nr. 0196
Trace-Nr. 370082
Kartenzahlung
Kontaktlos
girocard
Nr. ###############2721 0005
Terminal-ID 56007039
Pos-Info 00 073 00
AS-Zeit 05.09. 12:13 Uhr
Betrag EUR 3,03
Zahlung erfolgt

Steuer % Netto Steuer Brutto
A= 19,0% 0,59 0,11 0,70
B= 7,0% 2,18 0,15 2,33
Gesamtbetrag 2,77 0,26 3,03

TSE-Signatur: SncJQkg1RsRxKPgdSfHnkHi2JFa61JKkZ
CTOdT5kKVwjMOu2JoqE+CffBvk5r8+1LN
bQwRywiBEHnCBRyz8op5E8lggrha5I2AH
ClmMCEDso9FLiFD1Eze9s/c50Xc9Y
TSE-Signaturzähler: 949310
TSE-Transaktion: 445711
TSE-Start: 2022-09-05T12:16:55.000
TSE-Stop: 2022-09-05T12:17:24.000
Seriennnummer Kasse: REWE:00:01:2e:62:b8:5c:00
05.09.2022 12:17 Bon-Nr.:9550
Markt:0568 Kasse:3 Bed.:171717
****************************************

Ihre REWE PAYBACK Vorteile heute
PAYBACK Karten-Nr.: #########9437
Punktestand vor Einkauf: 4.978
Punktestand entspricht: 49,78 EUR

Sie erhalten 1 PAYBACK Punkt auf
einen PAYBACK Umsatz von 2,88 EUR!

Jetzt mit PAYBACK Punkten bezahlen!
Einfach REWE Guthaben am Service-Punkt
aufladen.


Für die mit * gekennzeichneten Produkte
erhalten Sie leider keine Rabatte
oder PAYBACK Punkte.

****************************************

REWE-Markt GmbH

Für Sie da:
MO - SA 7:00 bis 20:00 Uhr

Sie haben Fragen?
Antworten gibt es unter www.rewe.de"
                },
                new ParserInput
                {
                    IdReference = "3b212b693220419d9794daefbc15ee37",
                    Template = @"



Victoriastr. 3
86150 Augsburg
UID Nr.: DE812706034
EUR
BUTTERTOAST 1,59 B
BANANE BIO 1,45 B
0,730 kg x 1,99 EUR/kg
BIO HEUMILCH 1,85 B
ALPRO HAFERDRINK 1,99 A
DRINK MANDEL 2,89 A
SCHOKOREIS EDEL- 1,69 B
MILCHAUFSCHAEUME 29,99 A
--------------------------------------
SUMME EUR 41,45
======================================
Geg. EC-Cash EUR 41,45

* * Kundenbeleg * *
Datum: 03.09.2022
Uhrzeit: 18:22:50 Uhr
Beleg-Nr. 3131
Trace-Nr. 144619
Kartenzahlung
Contactless
girocard
Nr. ###############2721 0005
gültig bis 12/24
Terminal-ID 56038642
Pos-Info 00 075 00
AS-Zeit 03.09. 18:22 Uhr
Betrag EUR 41,45
Zahlung erfolgt

Steuer % Netto Steuer Brutto
A= 19,0% 29,30 5,57 34,87
B= 7,0% 6,15 0,43 6,58
Gesamtbetrag 35,45 6,00 41,45

TSE-Signatur: PzMBMsEQOrtq2KT6FfULgL4leRlUrfoFg
40LjYGncs9mZnSsourab6leX3NQVoG3Md
b7yez+X5UhJXO0rg+XTLjhoOnJ6bNp43r
UVB/iYEd7sSWEwL9uDeyAC1vBpNbc
TSE-Signaturzähler: 448713
TSE-Transaktion: 210885
TSE-Start: 2022-09-03T18:22:33.000
TSE-Stop: 2022-09-03T18:23:03.000
Seriennnummer Kasse: REWE:e0:d5:5e:c6:d0:63:00
03.09.2022 18:22 Bon-Nr.:3280
Markt:0778 Kasse:4 Bed.:151515
****************************************

Ihre REWE PAYBACK Vorteile heute
PAYBACK Karten-Nr.: #########9437
Punktestand vor Einkauf: 4.958
Punktestand entspricht: 49,58 EUR

Sie erhalten 20 PAYBACK Punkte auf
einen PAYBACK Umsatz von 41,45 EUR!


Jetzt mit PAYBACK Punkten bezahlen!
Einfach REWE Guthaben am Service-Punkt
aufladen.

****************************************

REWE Markt GmbH

Für Sie da:
MO - SA 07:00 bis 20:00 Uhr

Sie haben Fragen?
Antworten gibt es unter www.rewe.de"
                },
                new ParserInput
                {
                    IdReference = "fbd8720dc31c46dc8f876074dea672f2",
                    Template = @"



Wallbergstr. 16
86415 Mering
UID Nr.: DE812706034
EUR
JOGHURTDRINK ERD 0,95 B
DUPLO BIG PACK 3,79 B
MIN.WA.CLASS.PET 0,55 A
PFAND 0,15 EUR 0,15 A *
--------------------------------------
SUMME EUR 5,44
======================================
Geg. EC-Cash EUR 5,44

* * Kundenbeleg * *
Datum: 02.09.2022
Uhrzeit: 12:24:35 Uhr
Beleg-Nr. 9121
Trace-Nr. 298271
Kartenzahlung
Kontaktlos
girocard
Nr. ###############2721 0005
Terminal-ID 56007038
Pos-Info 00 073 00
AS-Zeit 02.09. 12:24 Uhr
Betrag EUR 5,44
Zahlung erfolgt

Steuer % Netto Steuer Brutto
A= 19,0% 0,59 0,11 0,70
B= 7,0% 4,43 0,31 4,74
Gesamtbetrag 5,02 0,42 5,44

TSE-Signatur: ElmALoazsdWGTNq5iHH7YkhFwiFOzb3sC
U4+CosGogGXpv9yFjkOaSMFyW78gw0xUl
Xd8wCk/Er0rfCgB6r009t/rUrv95ymQvs
trXC4ZJCCBcQ69tbvE+iSohPx85eR
TSE-Signaturzähler: 946559
TSE-Transaktion: 444450
TSE-Start: 2022-09-02T12:28:15.000
TSE-Stop: 2022-09-02T12:28:34.000
Seriennnummer Kasse: REWE:00:01:2e:62:b8:50:00
02.09.2022 12:28 Bon-Nr.:1107
Markt:0568 Kasse:2 Bed.: 4343
****************************************

Ihre REWE PAYBACK Vorteile heute
PAYBACK Karten-Nr.: #########9437
Punktestand vor Einkauf: 4.956
Punktestand entspricht: 49,56 EUR

Sie erhalten 2 PAYBACK Punkte auf
einen PAYBACK Umsatz von 5,29 EUR!

Jetzt mit PAYBACK Punkten bezahlen!
Einfach REWE Guthaben am Service-Punkt
aufladen.

Für die mit * gekennzeichneten Produkte
erhalten Sie leider keine Rabatte
oder PAYBACK Punkte.

****************************************

REWE-Markt GmbH

Für Sie da:
MO - SA 7:00 bis 20:00 Uhr

Sie haben Fragen?
Antworten gibt es unter www.rewe.de"
                },
                new ParserInput
                {
                    IdReference = "b8fc8bbffdbf4202981c8e93ecdbccd0",
                    Template = @"


R E W E
Ohmstr. 2-6
86415 Mering
UID Nr.: DE812706034
EUR
BAUERNBROT DKL 3,73 B
--------------------------------------
SUMME EUR 3,73
======================================
Geg. EC-Cash EUR 3,73

* * Kundenbeleg * *
Datum: 01.09.2022
Uhrzeit: 17:59:56 Uhr
Beleg-Nr. 6534
Trace-Nr. 244168
Kartenzahlung
Contactless
girocard
Nr. ###############2721 0005
gültig bis 12/24
Terminal-ID 56006848
Pos-Info 00 075 00
AS-Zeit 01.09. 17:59 Uhr
Betrag EUR 3,73
Zahlung erfolgt

Steuer % Netto Steuer Brutto
B= 7,0% 3,49 0,24 3,73
Gesamtbetrag 3,49 0,24 3,73

TSE-Signatur: LiuNyO3MfryXAV+37fRmh68RC31s69ZRQ
fAwlrpF8sLC3l+tUBZRsbwoWgSvDpMAGa
nUPFeXWEo2lbunW5aWkwXZr7XBgkzrW6S
jj3kf1DYiPAcGNfGhmLxKeBCrhMCm
TSE-Signaturzähler: 787154
TSE-Transaktion: 364813
TSE-Start: 2022-09-01T18:00:04.000
TSE-Stop: 2022-09-01T18:00:16.000
Seriennnummer Kasse: REWE:b4:2e:99:2b:77:68:00
01.09.2022 17:59 Bon-Nr.:5936
Markt:0488 Kasse:2 Bed.:161616
****************************************

Ihre REWE PAYBACK Vorteile heute
PAYBACK Karten-Nr.: #########9437
Punktestand vor Einkauf: 4.955
Punktestand entspricht: 49,55 EUR

Sie erhalten 1 PAYBACK Punkt auf
einen PAYBACK Umsatz von 3,73 EUR!

Jetzt mit PAYBACK Punkten bezahlen!
Einfach REWE Guthaben am Service-Punkt
aufladen.

****************************************

REWE Markt GmbH


Für Sie da:
MO - SA 7:00 bis 20:00 Uhr

Sie haben Fragen?
Antworten gibt es unter www.rewe.de"
                }
            };
        }

        public ParserInput GetCSVInput()
        {
            var result = new ParserInput
            {
                IdReference = null,
                Template = @"03.10.2022 16:30:00;2;Stück;Eintritt;7,0"
            };
            return result;
        }

        public VoucherController GetVoucherController()
        {
            return new VoucherController(Globals);
        }

        public IVoucherParser GetReweParser()
        {
            return new ReweParser();
        }

        public IVoucherParser GetCSVParser()
        {
            return new CSVParser();
        }

        public SyncFinancialTransactionsParentToChildrenRequest GetSyncFinancialTransactionsParentToChildrenRequest()
        {
            return new SyncFinancialTransactionsParentToChildrenRequest("09a0da236bd446b89d7a3d7059ee48d0");
        }
    }

    public class ParserInput
    {
        public string IdReference { get; set; }
        public string Template { get; set; }
    }
}
