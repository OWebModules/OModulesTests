﻿using GenerateOntologyAssembly;
using GenerateOntologyAssembly.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests.Configuration
{
    public class ExpectationsGenerateOntologyAssembly : ExpectationsAll
    {
        public GenerateOntologyAssemblyController GetConnector()
        {
            return new GenerateOntologyAssemblyController(new OntologyAppDBConnector.Globals());
        }

        public GenerateAssemblyRequest GetOntologyAssemblyRequest()
        {
            return new GenerateAssemblyRequest("74e7b20bc00d4df2a00392fe13a1865f");

        }

        public GenerateClassAssemblyRequest GetClassOntologyAssemblyRequest()
        {
            return new GenerateClassAssemblyRequest("e1ff4f68de1d4c5c9ece1f3f3238341b", "1.0.0.0", "VersionModule.Models", "VersionModule.Models.dll", @"D:\Gitlab\OModules\VersionModule", "f30436d62ffc4071af5e3ce708b8c2d9");
        }
    }
}
