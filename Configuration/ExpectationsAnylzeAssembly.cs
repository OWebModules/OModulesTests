﻿using AssemblyAnaylzeModule;
using AssemblyAnaylzeModule.Models;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OModulesTests.Configuration
{
    public class ExpectationsAnylzeAssembly :ExpectationsAll
    {
        public AnalyzeAssemblyRequest GetRequestAssemblyAnalyze()
        {
            var cancellationTokenSource = new CancellationTokenSource();
            return new AnalyzeAssemblyRequest("d21659c27ddc42f0a2e0a0fd6f0098ef", cancellationTokenSource.Token, UserItem);
        }

        public SyncNamespacesRequest GetRequestSyncNamespaces()
        {
            var cancellationTokenSource = new CancellationTokenSource();
            return new SyncNamespacesRequest("1ffec9ad31e74dd79db198a040d7b065", cancellationTokenSource.Token,UserItem);
        }

        public SyncMembersRequest GetRequestSyncClasses()
        {
            var cancellationTokenSource = new CancellationTokenSource();
            return new SyncMembersRequest("e0f533169f69418a8bfe0b57faa052a9", cancellationTokenSource.Token, UserItem);
        }

        public GetServiceReferencesHierarchyRequest GetGetServiceReferenceHierarchyRequest()
        {
            var cancellationTokenSource = new CancellationTokenSource();
            var request = new GetServiceReferencesHierarchyRequest(@"z:\", cancellationTokenSource.Token);

            return request;
        }
        public AssemblyAnalyzeController GetController()
        {
            var controller = new AssemblyAnalyzeController(Globals);
            return controller;
        }
    }
}
