﻿using GoogleCalendarConnector;
using GoogleCalendarConnector.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests.Configuration
{
    public class ExpectationsGoogleCalendarSync : ExpectationsAll
    {
        public DeleteEventRequest GetDeleteRequest()
        {
            return new DeleteEventRequest("f8a33783d6f44f5f9914196d8c2f6fc7");
        }
        public CalendarSyncRequest GetSyncRequest()
        {
            return new CalendarSyncRequest("046ff960a7174a9c88c97eb67f6734d1");
        }

        public CalendarConnector GetController()
        {
            return new CalendarConnector(Globals);
        }
    }
}
