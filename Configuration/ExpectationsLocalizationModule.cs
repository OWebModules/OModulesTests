﻿using LocalizationModule;
using LocalizationModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests.Configuration
{
    public class ExpectationsLocalizationModule : ExpectationsAll
    {
        public GetLocalizedGuiEntriesRequest GetGuiEntriesRequest()
        {
            var request = new GetLocalizedGuiEntriesRequest("fa13314314c34c0ebc85150daf512081", System.Globalization.CultureInfo.CurrentCulture.Name);

            return request;
        }

        public LocalizationController GetController()
        {
            return new LocalizationController(Globals);
        }

        public SyncCategoriesRequest GetSyncCategoriesRequest()
        {
            return new SyncCategoriesRequest(new List<OntologyClasses.BaseClasses.clsOntologyItem>
            {
                new OntologyClasses.BaseClasses.clsOntologyItem
                {
                    GUID = "4a6430a889f3409b883c3641a8b88898",
                    Name = "Financial-Transaction",
                    GUID_Parent = "fc3872da25164d8aa21b297c6b2b40dd",
                    Type = Globals.Type_Class
                }
            }, (new System.Threading.CancellationTokenSource()).Token);
        }

        public CategoryController GetCategoryController()
        {
            return new CategoryController(Globals);
        }
    }
}
