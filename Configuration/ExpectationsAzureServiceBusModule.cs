﻿using MailsToElastic;
using MailModule;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MailsToElastic.Models;
using MailModule.Models;
using MailLib.Models;
using AzureServiceBusModule;
using AzureServiceBusModule.Models;
using System.Threading;

namespace OModulesTests.Configuration
{
    public class ExpectationsAzureServiceBusModule : ExpectationsAll
    {
        public CancellationTokenSource ReceiveCancellationToken
        {
            get; private set;
        }
        public AzureServiceBusConnector GetConnector()
        {
            return new AzureServiceBusConnector(Globals);
        }

        public SendJobMessageRequest GetSendJobMessageRequest()
        {
            var result = new SendJobMessageRequest("e724e0c556cc4feb80b79feb7fff69dd");
            return result;
        }

        public ReceiveJobMessageRequest GetReceiveJobMessageRequest()
        {
            ReceiveCancellationToken = new CancellationTokenSource();
            var result = new ReceiveJobMessageRequest("457dfa5f768f46ad812c9f2f2782bc9a", ReceiveCancellationToken.Token);
            return result;
        }

        public ArchiveFilesItemsRequest GetArchiveMediaItemsRequest()
        {
            ReceiveCancellationToken = new CancellationTokenSource();
            var result = new ArchiveFilesItemsRequest("782ace4112eb446b87d7db1674347920", ReceiveCancellationToken.Token);
            return result;
        }
    }
}
