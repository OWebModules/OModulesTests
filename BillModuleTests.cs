﻿using BillModule.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OModulesTests.Configuration;
using OModulesTests.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests
{
    [TestClass]
    public class BillModuleTests
    {
        [TestMethod]
        public async Task ParseReweVoucherTest()
        {
            var expectations = new ExpectationsBillModule();

            var parser = expectations.GetReweParser();
            var reweBelege = expectations.GetReweBelege();
            var parseResults = new List<Voucher>();

            foreach (var reweBeleg in reweBelege)
            {
                var isResponsible = parser.IsResponsible(reweBeleg.Template);
                if (!isResponsible)
                {
                    Assert.Fail("The Rewe-Parser is not responsible for the provided Text!");
                    break;
                }
                var parserResult = await parser.ParseVoucher(reweBeleg.IdReference, reweBeleg.Template, expectations.Globals);
                if (parserResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
                {
                    Assert.Fail(parserResult.ResultState.Additional1);
                    break;
                }

                parseResults.Add(parserResult.Result);
            }    
        }

        [TestMethod]
        public async Task ParseCSVVoucherTest()
        {
            var expectations = new ExpectationsBillModule();

            var parser = expectations.GetCSVParser();
            var csvInput = expectations.GetCSVInput();
            var parseResults = new List<Voucher>();
            var isResponsible = parser.IsResponsible(csvInput.Template);
            if (!isResponsible)
            {
                Assert.Fail("The Rewe-Parser is not responsible for the provided Text!");
            }

            var parserResult = await parser.ParseVoucher(csvInput.Template, expectations.Globals);
            if (parserResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(parserResult.ResultState.Additional1);
            }

            parseResults.Add(parserResult.Result);
        }

        [TestMethod]
        public async Task SyncTransactions()
        {
            var expectations = new ExpectationsBillModule();
            var controller = expectations.GetController();

            var request = expectations.GetSyncFinancialTransactionsParentToChildrenRequest();
            var messageOutput = new MessageOutputForTests();
            messageOutput.OutputMessageForTests += MessageOutput_OutputMessageForTests;
            request.MessageOutput = messageOutput;

            var syncResult = await controller.SyncFinancialTransactionsParentToChildren(request);

            if (syncResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(syncResult.ResultState.Additional1);
            }

            if (!messageOutput.AreMessagesOk)
            {
                Assert.Fail("Errors occured!");
            }
        }

        private void MessageOutput_OutputMessageForTests(string message, OntologyAppDBConnector.MessageOutputType messageType, DateTime timeStamp, MessageOutputForTests messageOutput)
        {
            messageOutput.AreMessagesOk = true;
        }
    }
}
