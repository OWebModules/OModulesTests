﻿using AppointmentModule;
using AppointmentModule.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OModulesTests.Configuration;
using OModulesTests.Models;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests
{
    [TestClass]
    public class TypedTaggingTests
    {

        [TestMethod]
        public async Task TestGetWords()
        {
            var expectations = new ExpectationsTypedTagging();

            var controller = expectations.GetParseBase();
            var parseTexts = expectations.GetTexts();
            foreach (var parseText in parseTexts)
            {
                var parseResult = await controller.FindWords(parseText);

            }

        }

        [TestMethod]
        public async Task TestConvertTypedTagsToRelations()
        {
            var expectations = new ExpectationsTypedTagging();

            var controller = expectations.GetController();
            var request = expectations.GetConvertTypedTagsToRelationsRequest();
            request.MessageOutput = new MessageOutputForTests();
            request.MessageOutput.OutputUIMessage += MessageOutput_OutputUIMessage;

            var result = await controller.ConvertTypedTagsToRelations(request);

            if (result.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }

        }

        [TestMethod]
        public async Task TestParseString()
        {
            var expectations = new ExpectationsTypedTagging();

            var controller = expectations.GetController();

            var parseResult = await controller.ParseString(expectations.GetRefObjectForParseString(), false, expectations.SearchStringForParseString, null);

            if (parseResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(parseResult.ResultState.Additional1);
            }
        }

        private bool MessageOutput_OutputUIMessage(string message, OntologyAppDBConnector.MessageOutputType outputType, DateTime messageStamp)
        {
            return true;
        }
    }

}
