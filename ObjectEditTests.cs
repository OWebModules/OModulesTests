﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OModulesTests.Models;

namespace OModulesTests
{
    [TestClass]
    public class ObjectEditTests
    {
        [TestMethod]
        public async Task DuplicateObjectTest()
        {
            var exceptions = new Configuration.ExpectationsObjectEdit();
            var connector = exceptions.GetController();

            var request = exceptions.GetDuplicateObjectRequest();

            var result = await connector.DuplicateObject(request);

            if (result.ResultState.GUID == exceptions.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task CopyRelationsTest()
        {
            var exceptions = new Configuration.ExpectationsObjectEdit();
            var connector = exceptions.GetController();

            var request = exceptions.GetCopyRelationsRequest();

            var result = await connector.CopyRelations(request);

            if (result.ResultState.GUID == exceptions.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task DeleteObjectsTest()
        {
            var expectations = new Configuration.ExpectationsObjectEdit();
            var controller = expectations.GetOItemListController();

            var request = expectations.GetDeleteObjectsRequest();
            var messageOutput = new MessageOutputForTests();
            messageOutput.AreMessagesOk = true;
            messageOutput.OutputMessageForTests += MessageOutput_OutputMessageForDeleteObjectsTest;
            request.MessageOutput = messageOutput;
            var result = await controller.DeleteObjectsAndRelations(request);
            if (result.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }

        }

        private void MessageOutput_OutputMessageForDeleteObjectsTest(string message, OntologyAppDBConnector.MessageOutputType messageType, DateTime timeStamp, MessageOutputForTests messageOutput)
        {

        }
    }
}
