﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace OModulesTests
{
    [TestClass]
    public class ActiveDirectorySyncTests
    {
        [TestMethod]
        public async Task ComQueryTest()
        {
            var expectation = new Configuration.ExpectationsActiveDirectorySync();
            var controller = expectation.GetController();

            var queryResult = await controller.QueryActiveDirectory(expectation.GetRequestPartners());

            if (queryResult.ResultState.GUID == expectation.Globals.LState_Error.GUID)
            {
                Assert.Fail(queryResult.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task GetGroupMembersTest()
        {
            var expectation = new Configuration.ExpectationsActiveDirectorySync();
            var controller = expectation.GetController();

            var request = expectation.GetRequestGroupMembers();

            var queryResult = await controller.GetGroupMembers(request);

            if (queryResult.ResultState.GUID == expectation.Globals.LState_Error.GUID)
            {
                Assert.Fail(queryResult.ResultState.Additional1);
            }

            if (queryResult.Result.GroupMembers.Any(res => res.ResultState.GUID == expectation.Globals.LState_Error.GUID))
            {
                var error = queryResult.Result.GroupMembers.Where(res => res.ResultState.GUID == expectation.Globals.LState_Error.GUID).Select(res => res.ResultState.Additional1).ToList();
                Assert.Fail(string.Join("\r\n", error));
            }

        }

        [TestMethod]
        public async Task GetUserGroupsTest()
        {
            var expectation = new Configuration.ExpectationsActiveDirectorySync();
            var controller = expectation.GetController();

            var request = expectation.GetUserGroupsRequest();

            var queryResult = await controller.GetUserGroups(request);

            if (queryResult.ResultState.GUID == expectation.Globals.LState_Error.GUID)
            {
                Assert.Fail(queryResult.ResultState.Additional1);
            }

        }

        [TestMethod]
        public async Task ExportGroupMembersCSVTest()
        {
            var expectation = new Configuration.ExpectationsActiveDirectorySync();
            var controller = expectation.GetController();

            var request1 = expectation.GetRequestGroupMembers();

            var queryResult = await controller.GetGroupMembers(request1);

            if (queryResult.ResultState.GUID == expectation.Globals.LState_Error.GUID)
            {
                Assert.Fail(queryResult.ResultState.Additional1);
            }

            if (queryResult.Result.GroupMembers.Any(res => res.ResultState.GUID == expectation.Globals.LState_Error.GUID))
            {
                var error = queryResult.Result.GroupMembers.Where(res => res.ResultState.GUID == expectation.Globals.LState_Error.GUID).Select(res => res.ResultState.Additional1).ToList();
                Assert.Fail(string.Join("\r\n", error));
            }

            var request2 = expectation.GetExportGroupMembersRequest(queryResult.Result.GroupMembers.Where(result => result.ResultState.GUID == expectation.Globals.LState_Success.GUID).Select(result => result.Result).ToList());

            var exportResult = await controller.ExportGroupMembersToCSV(request2);

            if (exportResult.GUID == expectation.Globals.LState_Error.GUID)
            {
                Assert.Fail(exportResult.Additional1);
            }

        }

        [TestMethod]
        public async Task CompareGroupMembersTest()
        {
            var expectation = new Configuration.ExpectationsActiveDirectorySync();
            var controller = expectation.GetController();

            var request1 = expectation.GetRequestGroupMembersSrc();

            var queryResult1 = await controller.GetGroupMembers(request1);

            if (queryResult1.ResultState.GUID == expectation.Globals.LState_Error.GUID)
            {
                Assert.Fail(queryResult1.ResultState.Additional1);
            }

            if (queryResult1.Result.GroupMembers.Any(res => res.ResultState.GUID == expectation.Globals.LState_Error.GUID))
            {
                var error = queryResult1.Result.GroupMembers.Where(res => res.ResultState.GUID == expectation.Globals.LState_Error.GUID).Select(res => res.ResultState.Additional1).ToList();
                Assert.Fail(string.Join("\r\n", error));
            }


            var request2 = expectation.GetRequestGroupMembersDst();

            var queryResult2 = await controller.GetGroupMembers(request2);

            if (queryResult2.ResultState.GUID == expectation.Globals.LState_Error.GUID)
            {
                Assert.Fail(queryResult2.ResultState.Additional1);
            }

            if (queryResult2.Result.GroupMembers.Any(res => res.ResultState.GUID == expectation.Globals.LState_Error.GUID))
            {
                var error = queryResult2.Result.GroupMembers.Where(res => res.ResultState.GUID == expectation.Globals.LState_Error.GUID).Select(res => res.ResultState.Additional1).ToList();
                Assert.Fail(string.Join("\r\n", error));
            }


            var compareRequest = expectation.GetCompareRequest(queryResult1.Result.GroupMembers.Select(grp => grp.Result).ToList(), queryResult2.Result.GroupMembers.Select(grp => grp.Result).ToList());

            var compareResult = await controller.CompareGroupMembers(compareRequest);

            if (compareResult.ResultState.GUID == expectation.Globals.LState_Error.GUID)
            {
                Assert.Fail(compareResult.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task SyncComTest()
        {
            var expectation = new Configuration.ExpectationsActiveDirectorySync();
            var controller = expectation.GetController();

            var queryResult = await controller.QueryActiveDirectory(expectation.GetRequestPartners());

            if (queryResult.ResultState.GUID == expectation.Globals.LState_Error.GUID)
            {
                Assert.Fail(queryResult.ResultState.Additional1);
            }

            var saveResult = await controller.UpdatePartners(queryResult.Result);
            if (saveResult.GUID == expectation.Globals.LState_Error.GUID)
            {
                Assert.Fail(saveResult.Additional1);
            }
        }

        [TestMethod]
        public async Task SyncComTestAllPartners()
        {
            var expectation = new Configuration.ExpectationsActiveDirectorySync();
            var controller = expectation.GetController();

            var queryResult = await controller.QueryActiveDirectory(expectation.GetRequestPartners(true));

            if (queryResult.ResultState.GUID == expectation.Globals.LState_Error.GUID)
            {
                Assert.Fail(queryResult.ResultState.Additional1);
            }

            var saveResult = await controller.UpdatePartners(queryResult.Result);
            if (saveResult.GUID == expectation.Globals.LState_Error.GUID)
            {
                Assert.Fail(saveResult.Additional1);
            }
        }
    }
}
