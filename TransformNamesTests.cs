﻿using AppointmentModule;
using AppointmentModule.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OModulesTests.Configuration;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests
{
    [TestClass]
    public class SyncStarterModuleTests
    {

        [TestMethod]
        public async Task TestGeneralNameTransformProvider()
        {
            var expectations = new ExpectationsNameTransform();

            var controller = expectations.GetProvider();
            var transformItems = expectations.GetTransformItems();
            var idClass = transformItems.Select(trans => trans.GUID_Parent).FirstOrDefault();

            var extractResult = await controller.TransformNames(transformItems, false, idClass);

            if (extractResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(extractResult.ResultState.Additional1);
            }
        }

    }
}
