﻿using MailLib.Models;
using MailModule.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OModulesTests.Configuration;
using OModulesTests.Models;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests
{
    [TestClass]
    public class MailModuleTests
    {
        [TestMethod]
        public async Task GetMailItemsTest()
        {
            var expectations = new ExceptationsMailModule();
            var connector = expectations.GetMailConnector();

            var resultGetConnector = await connector.GetConnection(expectations.OItemImapConfiguration);

            
            
            if (resultGetConnector.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(resultGetConnector.ResultState.Additional1);
            }

            var mailItemsResult = await connector.GetMailItems(resultGetConnector.Result, new DateRange { Start = DateTime.Now, End = DateTime.Now.Subtract(new TimeSpan(90, 0, 0, 0)) }, null);

            if (mailItemsResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(mailItemsResult.ResultState.Additional1);
            }
            


        }

        [TestMethod]
        public async Task TestImportMails()
        {
            var expectations = new ExceptationsMailModule();
            var connector = expectations.GetMailsToElasticConnector();

            var connectorMail = expectations.GetMailConnector();

            var request = new MailModule.Models.ImportMailsRequest
            {
                DeleteMails = false,
                IdConfiguration = "2f94eb527abe412994df44ded87f7c96",
                MasterPassword = "Test"
            };
            var resultImportMails = await connectorMail.ImportImapMails(request);
            
            if (resultImportMails.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(resultImportMails.ResultState.Additional1);
            }

            
        }

        [TestMethod]
        public async Task TestExportMailsToJson()
        {
            var expectations = new ExceptationsMailModule();
            var connector = expectations.GetMailConnector();

            var exportToJsonRequest = expectations.GetExportToJsonRequest();

            var messageOutput = new MessageOutputForTests();
            messageOutput.OutputMessageForTests += MessageOutput_OutputMessageForTests; ;
            exportToJsonRequest.MessageOutput = messageOutput;

            var resultExportToJson = await connector.ExportMailItemsToJson(exportToJsonRequest);

            if (resultExportToJson.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(resultExportToJson.ResultState.Additional1);
            }
        }

        private void MessageOutput_OutputMessageForTests(string message, OntologyAppDBConnector.MessageOutputType messageType, DateTime timeStamp, MessageOutputForTests messageOutput)
        {
            
        }
    }
}
