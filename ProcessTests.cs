﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OModulesTests.Configuration;
using OModulesTests.Models;
using ReportModule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests
{
    [TestClass]
    public class ProcessTests
    {
        [TestMethod]
        public async Task TestGetChecklist()
        {
            var expectations = new ExpectationsReports();

            var getChecklistRequest = expectations.GetChecklistRequest();

            var controller = expectations.GetChecklistController();

            var messageOutput = new MessageOutputForTests();
            messageOutput.OutputMessageForTests += MessageOutput_OutputMessageForTestsChecklist;
            getChecklistRequest.MessageOutput = messageOutput;

            var getChecklistResult = await controller.GetChecklist(getChecklistRequest);

            if (getChecklistResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail("Error while getting the Checklist");
            }

        }

        private void MessageOutput_OutputMessageForTestsChecklist(string message, OntologyAppDBConnector.MessageOutputType messageType, DateTime timeStamp, MessageOutputForTests messageOutput)
        {
            
        }

        [TestMethod]
        public async Task TestCreatePDFReport()
        {
            var expectations = new ExpectationsReports();

            var request = expectations.GetCreatePDFReportRequest();

            var controller = new ReportController(expectations.Globals, false);
            var createPDFReportResult = await controller.CreatePDFReport(request);

            if (createPDFReportResult.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(createPDFReportResult.Additional1);
            }
        }

        [TestMethod]
        public async Task TestNameTransform()
        {
            var expectations = new ExpectationsReports();

            var controller = expectations.GetNameTransformProvider();
            var items = expectations.GetNameTransformObjects1();
            var idClass = expectations.IdClassNameTransformConfig();

            var transformResult = await controller.TransformNames(items, true, idClass);

            if (transformResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(transformResult.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task TestGetRowDiff()
        {
            var expectations = new ExpectationsReports();

            var controller = expectations.GetReportController();
            var reportOItem = expectations.ReportOItem;
            var report = await controller.GetReport(reportOItem);
            if (report.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(report.ResultState.Additional1);
            }

            var getRowDiffRequest = expectations.GetRowDiffRequest(report.Result.Report, report.Result.ReportFields);
            var messageOutput = new MessageOutputForTests();
            messageOutput.MessagesToCheck = expectations.TestGetRowDiff_MessageToCheck;
            messageOutput.AreMessagesOk = true;
            messageOutput.OutputMessageForTests += MessageOutput_OutputMessageForTests;
            getRowDiffRequest.MessageOutput = messageOutput;

            var rowDiff = await controller.GetRowDiff(getRowDiffRequest);

            if (rowDiff.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(rowDiff.ResultState.Additional1);
            }

            if (!messageOutput.AreMessagesOk)
            {
                Assert.Fail("The process was not executed like expected!");
            }
        }

        [TestMethod]
        public async Task TestChangeChecklistEntryState()
        {
            var expectations = new ExpectationsReports();

            var controller = expectations.GetChecklistController();

            var request = expectations.GetChangeChecklistEntryStateRequest();
            var messageOutput = new MessageOutputForTests();
            messageOutput.AreMessagesOk = true;
            messageOutput.OutputMessageForTests += MessageOutput_OutputMessageForTests1; ;

            var changeResult = await controller.ChangeChecklistEntryState(request);
            
            if (changeResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(changeResult.ResultState.Additional1);
            }
        }

        private void MessageOutput_OutputMessageForTests1(string message, OntologyAppDBConnector.MessageOutputType messageType, DateTime timeStamp, MessageOutputForTests messageOutput)
        {
            
        }

        private void MessageOutput_OutputMessageForTests(string message, OntologyAppDBConnector.MessageOutputType messageType, DateTime timeStamp, MessageOutputForTests messageOutput)
        {
            if (!messageOutput.AreMessagesOk) return;
            var isOk = true;
            var messageToTest = messageOutput.MessagesToCheck[messageOutput.MessageCount];
            if (message != messageToTest) isOk = false;
            messageOutput.MessageCount++;
            messageOutput.AreMessagesOk = isOk;
        }
    }
}
