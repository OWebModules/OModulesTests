﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OModulesTests.Configuration;
using OntologyClasses.BaseClasses;

namespace OModulesTests
{
    [TestClass]
    public class JiraConnectorTests
    {
        [TestMethod]
        public async Task TestSyncIssues()
        {
            var expectations = new ExpectationsJiraConnectorModule();

            var request = expectations.GetSyncIssueRequest();
            var connector = expectations.GetConnector();

            var syncResult = await connector.SyncJiraIssues(request);

            if (syncResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(syncResult.ResultState.Additional1);
            }
            
        }


        [TestMethod]
        public async Task TestAddComments()
        {
            var expectations = new ExpectationsJiraConnectorModule();
            var expectationsGit = new ExpectationsGitConnectorModule();

            var connectorGit = expectationsGit.GetConnector();

            var requestGitCommits = expectationsGit.GetCommitsRequestRegex();

            var resultCommits = await connectorGit.GetGitCommits(requestGitCommits);

            if (resultCommits.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(resultCommits.ResultState.Additional1);
            }

            var request = expectations.GetAddCommentToIssueRequest(resultCommits.Result.GitCommitsToProjects.Select(rel => new clsOntologyItem
            {
                GUID = rel.ID_Object,
                Name = rel.Name_Object,
                GUID_Parent = rel.ID_Parent_Object,
                Type = expectations.Globals.Type_Object
            }).ToList(), resultCommits.Result.CommitMessages);

            var connector = expectations.GetConnector();

            var syncResult = await connector.AddCommentToIssue(request);

            if (syncResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(syncResult.ResultState.Additional1);
            }

        }
    }
}
