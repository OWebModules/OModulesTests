﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OModulesTests.Configuration;
using OModulesTests.Models;

namespace OModulesTests
{
    [TestClass]
    public class FileRessourceModuleTests
    {
        [TestMethod]
        public async Task TestExcludeFiles()
        {
            var expectations = new ExpectationsFileRessourceModule();
            var controller = expectations.GetController();
            var oFileRessource = expectations.GetFileResource();

            var serviceResult = await controller.GetFileResource(oFileRessource);

            if (serviceResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(serviceResult.ResultState.Additional1);
            }

            var filesResult = await controller.GetFilesByFileResource(serviceResult.Result);

            if (filesResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(filesResult.ResultState.Additional1);
            }

        }
    }
}
