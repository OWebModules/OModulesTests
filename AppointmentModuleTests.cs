﻿using AppointmentModule;
using AppointmentModule.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OModulesTests.Configuration;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests
{
    [TestClass]
    public class AppointmentModuleTests
    {
        private ExpectationsAll expectationsAll;
        private ExpectationsAppointmentModule expectationsAppointmentModule;

        public AppointmentModuleTests()
        {
            expectationsAll = new ExpectationsAll();
            expectationsAppointmentModule = new ExpectationsAppointmentModule();
        }

        [TestMethod]
        public async Task GetCalendarConfigTest()
        {
            var appointmentConnector = expectationsAppointmentModule.AppointmentConnector;

            var result = await appointmentConnector.GetDataSource("Read", "Create", "Update", "Destroy");
            if (result.Result.GUID != appointmentConnector.LocalConfig.Globals.LState_Success.GUID)
            {
                Assert.Fail("Can't get CalendarConfig");
            }

            if (result.DataSource.Count != 2)
            {
                Assert.Fail("Wrong number of DataSources");
            }

            if (!result.DataSource.ContainsKey("transport"))
            {
                Assert.Fail("Transport is not contained in datasources");
            }

            var transportRaw = result.DataSource["transport"];
            if (transportRaw.GetType() != typeof(Dictionary<string, object>))
            {
                Assert.Fail("Transport is not correct typed");
            }

            var transport = (Dictionary<string, object>)(result.DataSource["transport"]);

            if (!transport.ContainsKey("read") || !transport.ContainsKey("create") || !transport.ContainsKey("update") || !transport.ContainsKey("destroy"))
            {
                Assert.Fail("Wrong Crud-Config");
            }

            var read = (Dictionary<string, object>) transport["read"];
            var create = (Dictionary<string, object>)transport["create"];
            var update = (Dictionary<string, object>)transport["update"];
            var destroy = (Dictionary<string, object>)transport["destroy"];

            if (!read.ContainsKey("url") || !create.ContainsKey("url") || !update.ContainsKey("url") || !destroy.ContainsKey("url"))
            {
                Assert.Fail("No Url-Config");
            }

            var url = read["url"].ToString();

            if (url != "Read")
            {
                Assert.Fail("No read Url-Config");
            }

            url = create["url"].ToString();

            if (url != "Create")
            {
                Assert.Fail("No create Url-Config");
            }

            url = update["url"].ToString();

            if (url != "Update")
            {
                Assert.Fail("No update Url-Config");
            }

            url = destroy["url"].ToString();

            if (url != "Destroy")
            {
                Assert.Fail("No destroy Url-Config");
            }
        }

        [TestMethod]
        public async Task SaveAndGetAppointmentTest()
        {
            var appointmentConnector = expectationsAppointmentModule.AppointmentConnector;
            var userItem = expectationsAll.UserItem;

            expectationsAppointmentModule.SchedulerItem = new AppointmentScheduler
            {
                NameAppointment = "Test-Appointment",
                IdUser = userItem.GUID,
                NameUser = userItem.Name,
                uid = Guid.NewGuid().ToString(),
                Start = expectationsAppointmentModule.Start,
                End = expectationsAppointmentModule.Ende
            };

            var resultSave = await appointmentConnector.SaveAppointment(expectationsAppointmentModule.SchedulerItem, userItem);

            if (resultSave.GUID != expectationsAll.Globals.LState_Success.GUID)
            {
                Assert.Fail("Appointment cannot be saved");
            }

            if (resultSave.OList_Rel == null || !resultSave.OList_Rel.Any())
            {
                Assert.Fail("Appointment is not saved correctly");
            }

            var oItemAppointment = resultSave.OList_Rel.First();

            expectationsAppointmentModule.SchedulerItem.IdAppointment = oItemAppointment.GUID;

            var resultGet = await appointmentConnector.GetAppointments(userItem);

            if (resultGet.Appointments == null)
            {
                Assert.Fail("Appointment is not saved correctly");
            }


            var loadedEvent = resultGet.Appointments.FirstOrDefault(appointment => appointment.IdAppointment == expectationsAppointmentModule.SchedulerItem.IdAppointment);

            if (loadedEvent == null || loadedEvent.Start != expectationsAppointmentModule.Start || loadedEvent.End != expectationsAppointmentModule.Ende)
            {
                Assert.Fail("Appointment is not saved correctly");
            }

        }

    }
}
