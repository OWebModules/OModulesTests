﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OModulesTests.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests
{
    [TestClass]
    public class JsonModuleTests
    {
        [TestMethod]
        public async Task TestJsonConvertToSchema()
        {
            var expectation = new Configuration.ExpectationsJsonModule();
            var controller = expectation.GetConverter();
            var request = expectation.GetJsonToSchemaRequest();

            var result = await controller.ConvertJsonToSchema(request);
            if (result.ResultState.GUID == expectation.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task TestJsonGetEsIndexItem()
        {
            var expectation = new Configuration.ExpectationsJsonModule();
            var controller = expectation.GetConverter();
            var request = expectation.GetEsIndexItemRequest();
            var messageOutput = new MessageOutputForTests();
            messageOutput.OutputMessageForTests += MessageOutput_OutputMessageForTests;
            request.MessageOutput = messageOutput;
            var result = await controller.GetEsIndexItem(request);
            if (result.ResultState.GUID == expectation.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        private void MessageOutput_OutputMessageForTests(string message, OntologyAppDBConnector.MessageOutputType messageType, DateTime timeStamp, MessageOutputForTests messageOutput)
        {
            
        }

        [TestMethod]
        public async Task TestImportJsonAttribute()
        {
            var expectation = new Configuration.ExpectationsJsonModule();
            var controller = expectation.GetConverter();
            var request = expectation.GetImportToJsonAttributeRequest();

            var result = await controller.ImportToJsonAttribute(request);
            if (result.ResultState.GUID == expectation.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }
    }
}
