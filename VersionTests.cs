﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OModulesTests.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests
{
    [TestClass]
    public class VersionTests
    {
        [TestMethod]
        public async Task TestGetVersions()
        {
            var expectations = new ExpectationsVersionController();

            var controller = expectations.GetController();
            var request = expectations.GetRequestGetVersions();

            var taskResult = await controller.GetRefVersions(request);

            if (taskResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(taskResult.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task TestAddVersionIncrease()
        {
            var expectations = new ExpectationsVersionController();

            var controller = expectations.GetController();

            var request = expectations.GetRequestAddVersionMajorIncrease();

            var taskResult = await controller.AddVersion(request);

            if (taskResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(taskResult.ResultState.Additional1);
            }

            request = expectations.GetRequestAddVersionMinorIncrease();

            taskResult = await controller.AddVersion(request);

            if (taskResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(taskResult.ResultState.Additional1);
            }

            request = expectations.GetRequestAddVersionBuildIncrease();

            taskResult = await controller.AddVersion(request);

            if (taskResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(taskResult.ResultState.Additional1);
            }

            request = expectations.GetRequestAddVersionRevisionIncrease();

            taskResult = await controller.AddVersion(request);

            if (taskResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(taskResult.ResultState.Additional1);
            }

            request = expectations.GetRequestAddVersionSet();

            taskResult = await controller.AddVersion(request);

            if (taskResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(taskResult.ResultState.Additional1);
            }
        }
    }
}
