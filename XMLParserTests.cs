﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace OModulesTests
{
    [TestClass]
    public class XMLParserTests
    {
        [TestMethod]
        public async Task ParseXMLFieldTest()
        {
            var expectations = new Configuration.ExpectationsXMLParser();

            var request = expectations.GetParseXMLFieldRequest();
            var controller = expectations.GetController();

            var result = await controller.ParseXMLField(request);

            if (result.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task ParseXMLFile()
        {
            var expectations = new Configuration.ExpectationsXMLParser();

            var controller = expectations.GetController();

            var result = await controller.ImportXMLFile(@"C:\Temp\Casebook\DE.FVHGA.2020_05_06_105\DE.FVHGA.2020_05_06_105.xml", "casebookxml", "product", "localhost", 9200, new System.Collections.Generic.List<string> { "Case" });

            if (result.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.Additional1);
            }
        }
    }
}
