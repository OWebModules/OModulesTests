﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OModulesTests.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests
{
    [TestClass]
    public class CloudStorageTests
    {
        [TestMethod]
        public async Task TestCreateContainer()
        {
            var expectation = new Configuration.ExpectationsCloudStorageModule();
            var controller = expectation.GetController();
            var request = expectation.GetCreateContainerRequest();
            var messageOutput = new MessageOutputForTests();
            messageOutput.OutputMessageForTests += MessageOutput_OutputUIMessageSaveAnswerForCreateContainer;
            request.MessageOutput = messageOutput;

            var result = await controller.CreateContainer(request);
            if (result.ResultState.GUID == expectation.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
            if (!messageOutput.AreMessagesOk)
            {
                Assert.Fail("Errors occurred!");
            }
        }

        [TestMethod]
        public async Task TestSyncMediaList()
        {
            var expectation = new Configuration.ExpectationsCloudStorageModule();
            var controller = expectation.GetController();
            var request = expectation.GetSyncMediaListRequest();
            var messageOutput = new MessageOutputForTests();
            messageOutput.OutputMessageForTests += MessageOutput_OutputMessageForTestsSyncMediaList;
            request.MessageOutput = messageOutput;

            var result = await controller.SyncMediaList(request);
            if (result.ResultState.GUID == expectation.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
            if (!messageOutput.AreMessagesOk)
            {
                Assert.Fail("Errors occurred!");
            }
        }

        private void MessageOutput_OutputMessageForTestsSyncMediaList(string message, OntologyAppDBConnector.MessageOutputType messageType, DateTime timeStamp, MessageOutputForTests messageOutput)
        {
            Console.WriteLine(message);
            if (messageType == OntologyAppDBConnector.MessageOutputType.Error)
            {
                messageOutput.AreMessagesOk = false;
            }

            if (!messageOutput.AreMessagesOk)
            {
                return;
            }

            messageOutput.MessageCount++;

            
        }

        private void MessageOutput_OutputUIMessageSaveAnswerForCreateContainer(string message, OntologyAppDBConnector.MessageOutputType outputType, DateTime messageStamp, MessageOutputForTests messageOutput)
        {
            Console.WriteLine(message);
            if (outputType == OntologyAppDBConnector.MessageOutputType.Error)
            {
                messageOutput.AreMessagesOk = false;
            }

            if (!messageOutput.AreMessagesOk)
            {
                return;
            }

            messageOutput.MessageCount++;

            
        }
    }
}
