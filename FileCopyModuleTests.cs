﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OModulesTests.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests
{
    [TestClass]
    public class FileCopyModuleTests
    {
        [TestMethod]
        public async Task GetFileItemsTest()
        {
            var expectations = new ExpectationsFileCopyModule();

            var controller = expectations.GetController();
            var request = expectations.GetCopyItemsRequest();

            var result = await controller.GetCopyItems(request);
            if (result.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task CopyFileItemsTest()
        {
            var expectations = new ExpectationsFileCopyModule();

            var controller = expectations.GetController();
            var request = expectations.GetCopyItemsRequest();

            var result = await controller.GetCopyItems(request);
            if (result.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }

            var resultCopy = await controller.CopyItems(result);
            if (resultCopy.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(resultCopy.ResultState.Additional1);
            }
        }
    }
}
