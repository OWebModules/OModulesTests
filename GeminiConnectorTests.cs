﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace OModulesTests
{
    [TestClass]
    public class GeminiConnectorTests
    {
        [TestMethod]
        public async Task TestSyncIssues()
        {
            var expectations = new Configuration.ExpectationsGeminiConnectorModule();

            var connector = expectations.GetConnector();
            var request = expectations.GetSyncIssuesRequest();

            var connectorResult = await connector.SyncIssues(request);

            if (connectorResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(connectorResult.ResultState.Additional1);
            }
        }
    }
}
