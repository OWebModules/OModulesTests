﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OModulesTests.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests
{
    [TestClass]
    public class LearningModuleTests
    {
        [TestMethod]
        public async Task TestGetRelatedQuestsions()
        {
            var expectation = new Configuration.ExpectationsLearningModule();
            var controller = expectation.GetController();
            var request = expectation.GetRelatedQuestionsRequest();

            var result = await controller.GetRelatedQuestions(request);
            if (result.ResultState.GUID == expectation.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task TestSaveSelfAnswer()
        {
            var expectation = new Configuration.ExpectationsLearningModule();
            var controller = expectation.GetController();
            var request = expectation.GetSaveSelfAnswerRequest();

            var result = await controller.SaveSelfAnswer(request);
            if (result.ResultState.GUID == expectation.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }

            if (result.Result.AnswerItem == null || 
                result.Result.AnswerToCategory == null || 
                result.Result.HtmlDocument == null ||
                result.Result.HtmlAttribute == null ||
                result.Result.QuestionToAnswer == null)
            {
                Assert.Fail("Not all result-items are set!");
            }
        }

        [TestMethod]
        public async Task TestSaveAnswerWithOfficialAnswer()
        {
            var expectation = new Configuration.ExpectationsLearningModule();
            var controller = expectation.GetController();
            var getRelatedQuestionsRequest = expectation.GetRelatedQuestionsRequest();

            var getQuestionResult = await controller.GetRelatedQuestions(getRelatedQuestionsRequest);
            if (getQuestionResult.ResultState.GUID == expectation.Globals.LState_Error.GUID)
            {
                Assert.Fail(getQuestionResult.ResultState.Additional1);
            }

            if (!getQuestionResult.Result.Questions.Any())
            {
                Assert.Fail($"No Questions of {getRelatedQuestionsRequest.ReferenceItem.GUID} found!");
            }

            var question = expectation.GetQuestionForSaveAnswer(getQuestionResult.Result.Questions);

            if (question == null)
            {
                Assert.Fail($"Question {expectation.GetIdQuestionForSaveAnswer()} not found!");
            }
            
            var request = expectation.GetSaveAnswerWithOfficialAnswerRequest(question);
            var messageOutput = new MessageOutputForTests();
            messageOutput.OutputMessageForTests += MessageOutput_OutputUIMessageSaveAnswerForOfficeAnswer;
            request.MessageOutput = messageOutput;
            

            var saveQuestionResult = controller.SaveAnswerWithOfficialAnswer(request);
            while(!saveQuestionResult.IsCompleted)
            {

            }

            if (saveQuestionResult.Result.GUID == expectation.Globals.LState_Error.GUID)
            {
                Assert.Fail(saveQuestionResult.Result.Additional1);
            }


        }

        private void MessageOutput_OutputUIMessageSaveAnswerForOfficeAnswer(string message, OntologyAppDBConnector.MessageOutputType outputType, DateTime messageStamp, MessageOutputForTests messageOutput)
        {
            if (outputType == OntologyAppDBConnector.MessageOutputType.Error)
            {
                messageOutput.AreMessagesOk = false;
            }

            if (!messageOutput.AreMessagesOk)
            {
                return;
            }

            messageOutput.MessageCount++;

            Console.WriteLine(message);
        }

        [TestMethod]
        public async Task TestGetFilter()
        {
            var expectation = new Configuration.ExpectationsLearningModule();
            var controller = expectation.GetController();
            var request = expectation.GetFilterRequest();

            var result = await controller.GetFilter(request);
            if (result.ResultState.GUID == expectation.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

    }
}
