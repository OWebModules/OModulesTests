﻿using ImportExport_Module;
using ImportExport_Module.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OModulesTests.Configuration;
using OModulesTests.Models;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Models;
using OntologyItemsModule.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests
{
    [TestClass]
    public class ImportExportModuleTests
    {
        [TestMethod]
        public async Task TestExportAttributeTypes()
        {
            var expectionasAll = new ExpectationsImportExportModule();

            var jsonExporter = new JsonExporter(expectionasAll.Globals);
            jsonExporter.PacketCount = expectionasAll.PacketCountBig;

            var dbReader = new OntologyModDBConnector(expectionasAll.Globals);

            var result = dbReader.GetDataAttributeType();

            if (result.GUID == expectionasAll.Globals.LState_Error.GUID)
            {
                Assert.Fail("Error while loading AttributeTypes");
            }

            jsonExporter.AttributeTypes = dbReader.AttributeTypes;

            var resultExport = await jsonExporter.ExportItems(expectionasAll.ExportPath, expectionasAll.ExportKey);

            if (resultExport.ResultState.GUID == expectionasAll.Globals.LState_Error.GUID)
            {
                Assert.Fail(resultExport.ResultState.Additional1);
            }

        }

        [TestMethod]
        public async Task TestExportObjects()
        {
            var expectionasAll = new ExpectationsImportExportModule();

            var jsonExporter = new JsonExporter(expectionasAll.Globals);
            jsonExporter.PacketCount = expectionasAll.PacketCountBig;

            var dbReader = new OntologyModDBConnector(expectionasAll.Globals);

            var result = dbReader.GetDataObjects();

            if (result.GUID == expectionasAll.Globals.LState_Error.GUID)
            {
                Assert.Fail("Error while loading Objects");
            }

            jsonExporter.ObjectItems = dbReader.Objects1;

            var resultExport = await jsonExporter.ExportItems(expectionasAll.ExportPath, expectionasAll.ExportKey);

            if (resultExport.ResultState.GUID == expectionasAll.Globals.LState_Error.GUID)
            {
                Assert.Fail(resultExport.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task TestExportClasses()
        {
            var expectionasAll = new ExpectationsImportExportModule();

            var jsonExporter = new JsonExporter(expectionasAll.Globals);
            jsonExporter.PacketCount = expectionasAll.PacketCountBig;

            var dbReader = new OntologyModDBConnector(expectionasAll.Globals);

            var result = dbReader.GetDataClasses();

            if (result.GUID == expectionasAll.Globals.LState_Error.GUID)
            {
                Assert.Fail("Error while loading Classes");
            }

            jsonExporter.ClassItems = dbReader.Classes1;

            var resultExport = await jsonExporter.ExportItems(expectionasAll.ExportPath, expectionasAll.ExportKey);

            if (resultExport.ResultState.GUID == expectionasAll.Globals.LState_Error.GUID)
            {
                Assert.Fail(resultExport.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task TestExportRelationTypes()
        {
            var expectionasAll = new ExpectationsImportExportModule();

            var jsonExporter = new JsonExporter(expectionasAll.Globals);
            jsonExporter.PacketCount = expectionasAll.PacketCountBig;

            var dbReader = new OntologyModDBConnector(expectionasAll.Globals);

            var result = dbReader.GetDataRelationTypes(null);

            if (result.GUID == expectionasAll.Globals.LState_Error.GUID)
            {
                Assert.Fail("Error while loading RelationTypes");
            }

            jsonExporter.RelationTypes = dbReader.RelationTypes;

            var resultExport = await jsonExporter.ExportItems(expectionasAll.ExportPath, expectionasAll.ExportKey);

            if (resultExport.ResultState.GUID == expectionasAll.Globals.LState_Error.GUID)
            {
                Assert.Fail(resultExport.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task TestExportClassAtts()
        {
            var expectionasAll = new ExpectationsImportExportModule();

            var jsonExporter = new JsonExporter(expectionasAll.Globals);
            jsonExporter.PacketCount = expectionasAll.PacketCountBig;

            var dbReader = new OntologyModDBConnector(expectionasAll.Globals);

            var result = dbReader.GetDataClassAtts();

            if (result.GUID == expectionasAll.Globals.LState_Error.GUID)
            {
                Assert.Fail("Error while loading RelationTypes");
            }

            jsonExporter.ClassAttributes = dbReader.ClassAtts;

            var resultExport = await jsonExporter.ExportItems(expectionasAll.ExportPath, expectionasAll.ExportKey);

            if (resultExport.ResultState.GUID == expectionasAll.Globals.LState_Error.GUID)
            {
                Assert.Fail(resultExport.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task TestExportClassRels()
        {
            var expectionasAll = new ExpectationsImportExportModule();

            var jsonExporter = new JsonExporter(expectionasAll.Globals);
            jsonExporter.PacketCount = expectionasAll.PacketCountBig;

            var dbReader = new OntologyModDBConnector(expectionasAll.Globals);

            var result = dbReader.GetDataClassRel();

            if (result.GUID == expectionasAll.Globals.LState_Error.GUID)
            {
                Assert.Fail("Error while loading Classrelations");
            }

            jsonExporter.ClassRelations = dbReader.ClassRels;

            var resultExport = await jsonExporter.ExportItems(expectionasAll.ExportPath, expectionasAll.ExportKey);

            if (resultExport.ResultState.GUID == expectionasAll.Globals.LState_Error.GUID)
            {
                Assert.Fail(resultExport.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task TestExportObjectAtts()
        {
            var expectionasAll = new ExpectationsImportExportModule();

            var jsonExporter = new JsonExporter(expectionasAll.Globals);
            jsonExporter.PacketCount = expectionasAll.PacketCountBig;

            var dbReader = new OntologyModDBConnector(expectionasAll.Globals);

            var result = dbReader.GetDataObjectAtt();

            if (result.GUID == expectionasAll.Globals.LState_Error.GUID)
            {
                Assert.Fail("Error while loading ObjectAttributes");
            }

            jsonExporter.ObjectAttributes = dbReader.ObjAtts;

            var resultExport = await jsonExporter.ExportItems(expectionasAll.ExportPath, expectionasAll.ExportKey);

            if (resultExport.ResultState.GUID == expectionasAll.Globals.LState_Error.GUID)
            {
                Assert.Fail(resultExport.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task TestExportObjectRels()
        {
            var expectionasAll = new ExpectationsImportExportModule();

            var jsonExporter = new JsonExporter(expectionasAll.Globals);
            jsonExporter.PacketCount = expectionasAll.PacketCountBig;

            var dbReader = new OntologyModDBConnector(expectionasAll.Globals);

            var result = dbReader.GetDataObjectRel(null);

            if (result.GUID == expectionasAll.Globals.LState_Error.GUID)
            {
                Assert.Fail("Error while loading ObjectAttributes");
            }

            jsonExporter.ObjectRelations = dbReader.ObjectRels;

            var resultExport = await jsonExporter.ExportItems(expectionasAll.ExportPath, expectionasAll.ExportKey);

            if (resultExport.ResultState.GUID == expectionasAll.Globals.LState_Error.GUID)
            {
                Assert.Fail(resultExport.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task TestExportGraph()
        {
            var expectionasAll = new ExpectationsImportExportModule();

            var jsonExporter = new JsonExporter(expectionasAll.Globals);
            jsonExporter.PacketCount = expectionasAll.PacketCountBig;

            var exportRequest = new ExportWholeGraphRequest(expectionasAll.ExportPath);
            var resultExport = await jsonExporter.ExportWholeGraph(exportRequest);

            if (resultExport.ResultState.GUID == expectionasAll.Globals.LState_Error.GUID)
            {
                Assert.Fail(resultExport.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task TestImport()
        {
            var expections = new ExpectationsImportExportModule();

            var jsonImporter = new JsonImporter(expections.Globals);
            jsonImporter.PacketCount = expections.PacketCountBig;

            var resultImport = await jsonImporter.ImportFiles(expections.ExportPath);

            if (resultImport.ResultState.GUID == expections.Globals.LState_Error.GUID)
            {
                Assert.Fail(resultImport.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task TestAssemblyImportWithType()
        {
            var expectations = new ExpectationsImportExportModule();

            var controller = expectations.GetAssemblyImporter();

            var resultImport = await controller.ImportAssembly(typeof(BCMCSVImporter.Config.LocalData));

            if (resultImport.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(resultImport.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task TestAssemblyImportWithPath()
        {
            var expectations = new ExpectationsImportExportModule();

            var controller = expectations.GetAssemblyImporter();

            var resultCheck = await controller.CheckAssembly(expectations.AssemblyPath);

            if (resultCheck.ResultState.GUID == controller.Globals.LState_Nothing.GUID // No OntologyAssembly
                || resultCheck.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                Assert.Fail(resultCheck.ResultState.Additional1);
            }

            var resultImport = await controller.ImportAssembly(resultCheck.AssemblyType);

            if (resultImport.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(resultImport.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task TestAssemblyCheck()
        {
            var expectations = new ExpectationsImportExportModule();

            var controller = expectations.GetAssemblyImporter();

            var result = await controller.CheckAssembly(expectations.AssemblyPath);

            if (result.ResultState.GUID == controller.Globals.LState_Nothing.GUID // No OntologyAssembly
                || result.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task TestExcelImport()
        {
            var expectations = new ExpectationsImportExportModule();

            var controller = expectations.GetExcelImporter();

            var request = new CreateExcelDataTableRequest
            {
                FilePathExcel = expectations.ExcelFile,
                Sheets = new List<string>()
            };

            var result = await controller.CreateExcelDataTable(request);

            if (result.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task TestCreateNameSpaceOfJson()
        {
            var expectionasAll = new ExpectationsImportExportModule();

            var jsonExporter = expectionasAll.GetJsonExporter();

            var request = new JsonToNamespaceNotationRequest(System.IO.File.ReadAllText(expectionasAll.JsonFileForNamespace), ".", true);

            var result = await jsonExporter.JsonToNameSpaceNotation(request);

            if (result.ResultState.GUID == expectionasAll.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task TestMigrateWholeGraphToCosmosDbRequest()
        {
            var expectations = new ExpectationsImportExportModule();
            var controller = expectations.GetCosmosDBConnector();
            var ontologyController = expectations.GetOntologyConnector();
            var request = expectations.GetMigrateWholeGraphToCosmosDbRequest();

            controller.RequestOntolgies +=  (ontologiesRaw) =>
             {

                 var ontologyResult = Task.Run<List<Ontology>>(async () =>
                 {
                     var ontologies = new List<Ontology>();
                     foreach (var ontologyRaw in ontologiesRaw)
                     {
                         var getontologyRequest = new GetOntologyRequest(ontologyRaw);

                         var ontologiesResult = await ontologyController.GetOntologies(getontologyRequest);
                         if (ontologiesResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
                         {
                             Assert.Fail(ontologiesResult.ResultState.Additional1);
                         }
                         ontologies.AddRange(ontologiesResult.Result);
                     }
                     var enrichResult = await ontologyController.EnrichOntologies(ontologies, true);

                     if (enrichResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
                     {
                         Assert.Fail(enrichResult.ResultState.Additional1);
                     }

                     return enrichResult.Result;
                 });
                 ontologyResult.Wait();
                 return ontologyResult.Result;
             };
            var taskResult = await controller.MigrateWholeGraphToCosmosDb(request);

            if (taskResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(taskResult.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task TestExportEsIndex()
        {
            var expectionasAll = new ExpectationsImportExportModule();

            var jsonExporter = new JsonExporter(expectionasAll.Globals);
            jsonExporter.PacketCount = expectionasAll.PacketCountSmall;

            var request = expectionasAll.GetExportEsIndexRequest();
            var messageOutput = new MessageOutputForTests();
            messageOutput.OutputMessageForTests += MessageOutput_OutputMessageForTestsEsIndex;

            var resultExport = await jsonExporter.ExportESIndex(request);

            if (resultExport.GUID == expectionasAll.Globals.LState_Error.GUID)
            {
                Assert.Fail(resultExport.Additional1);
            }

        }

        private void MessageOutput_OutputMessageForTestsEsIndex(string message, MessageOutputType messageType, DateTime timeStamp, MessageOutputForTests messageOutput)
        {
            messageOutput.AreMessagesOk = true;
        }

        [TestMethod]
        public async Task TestCompareImpExp()
        {
            var expections = new ExpectationsImportExportModule();

            var compareRequest = expections.GetCompareImpExpRequest();
            var jsonImporter = new JsonImporter(expections.Globals);

            var messageOutput = new MessageOutputForTests();
            messageOutput.OutputMessageForTests += MessageOutput_OutputMessageForTests;

            compareRequest.MessageOutput = messageOutput;

            var compareResult = await jsonImporter.CompareImpExp(compareRequest);

            if (compareResult.ResultState.GUID == expections.Globals.LState_Error.GUID)
            {
                Assert.Fail(compareResult.ResultState.Additional1);
            }
        }

        private void MessageOutput_OutputMessageForTests(string message, MessageOutputType messageType, DateTime timeStamp, MessageOutputForTests messageOutput)
        {
            messageOutput.AreMessagesOk = true;
        }
    }
}
