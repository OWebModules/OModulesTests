﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OModulesTests.Configuration;
using OModulesTests.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests
{
    [TestClass]
    public class MachineLearningModuleTests
    {
        [TestMethod]
        public async Task GetDecisionTreeTSonTest()
        {
            var expectations = new ExpectationsMachineLearningModule();

            var controller = expectations.GetDecisionTreeController();
            var request = expectations.GetDecisionTreeTSonRequest();

            var result = await controller.GetDecisionTreeTSon(request);
            if (result.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task MergeSortByPDFDocs()
        {
            var expectations = new ExpectationsMachineLearningModule();

            var controller = expectations.GetSortController();
            var request = expectations.GetMergeSortByPDFDocRequest();

            var messageOutput = new MessageOutputForTests();
            messageOutput.OutputMessageForTests += MessageOutput_OutputMessageForTests;

            request.MessageOutput = messageOutput;

            var taskResult = await controller.MergeSortByPDFDoc(request);

            if (taskResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(taskResult.ResultState.Additional1);
            }
        }

        private void MessageOutput_OutputMessageForTests(string message, OntologyAppDBConnector.MessageOutputType messageType, DateTime timeStamp, MessageOutputForTests messageOutput)
        {
            messageOutput.AreMessagesOk = true;
        }

        [TestMethod]
        public async Task CreateAssociationStatisticTest()
        {
            var expectations = new ExpectationsMachineLearningModule();

            var controller = expectations.GetAssociationAnalysisController();

            var request = expectations.GetCreateAssociationStatisticRequest();
            var messageOutput = new MessageOutputForTests();
            messageOutput.OutputMessageForTests += MessageOutput_CreateAssociationStatistic;

            var createResult = await controller.CreateAssociationStatistic(request);

            if (createResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(createResult.ResultState.Additional1);
            }

            if (!messageOutput.AreMessagesOk)
            {
                Assert.Fail("Orror Occurred");
            }
        }

        private void MessageOutput_CreateAssociationStatistic(string message, OntologyAppDBConnector.MessageOutputType messageType, DateTime timeStamp, MessageOutputForTests messageOutput)
        {
            messageOutput.AreMessagesOk = true;
        }

        [TestMethod]
        public async Task CreateTrainingsdataTrainingskleidungTest()
        {
            var expectations = new ExpectationsMachineLearningModule();

            var controller = expectations.GetTrainingsdataController();

            var warenkorbTrainingsdatenRequest = expectations.GetWrenkorbTrainingsdatenRequest();
            var messageOutput = new MessageOutputForTests();
            messageOutput.OutputMessageForTests += MessageOutput_CreateTrainingsdataTrainingskleidung;
            warenkorbTrainingsdatenRequest.MessageOutput = messageOutput;

            var createTrainingsdataResult = await controller.CreateTrainingsdata(warenkorbTrainingsdatenRequest);

            if (!messageOutput.AreMessagesOk)
            {
                Assert.Fail("Orror Occurred");
            }
        }

        private void MessageOutput_CreateTrainingsdataTrainingskleidung(string message, OntologyAppDBConnector.MessageOutputType messageType, DateTime timeStamp, MessageOutputForTests messageOutput)
        {
            messageOutput.AreMessagesOk = true;
        }
    }
}
