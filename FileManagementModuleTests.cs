﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OModulesTests.Configuration;
using OModulesTests.Models;

namespace OModulesTests
{
    [TestClass]
    public class FileManagementModuleTests
    {
        [TestMethod]
        public async Task TestFileVersions()
        {
            var expectations = new ExpectationsFileSystemManagement();
            var controller = expectations.GetController();
            var getFileVersionsRequest = expectations.GetFileVersionsRequest();

            var serviceResult = await controller.GetFileVersions(getFileVersionsRequest);

            if (serviceResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(serviceResult.ResultState.Additional1);
            }

        }

        [TestMethod]
        public async Task TestUpdateCreateStamp()
        {
            var expectations = new ExpectationsFileSystemManagement();
            var controller = expectations.GetController();
            var updateCreateStampRequest = expectations.GetUpdateCreateStampRequest();

            var serviceResult = await controller.UpdateCreateStamp(updateCreateStampRequest);

            if (serviceResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(serviceResult.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task TestReplaceContent()
        {
            var expectations = new ExpectationsFileSystemManagement();
            var controller = expectations.GetController();
            var replaceContentRequest = expectations.GetReplaceContentRequest();
            var messageOutput = new MessageOutputForTests();
            messageOutput.OutputMessageForTests += MessageOutput_OutputMessageForTests;

            var serviceResult = await controller.ReplaceContent(replaceContentRequest, false);

            if (serviceResult.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(serviceResult.Additional1);
            }
        }

        private void MessageOutput_OutputMessageForTests(string message, OntologyAppDBConnector.MessageOutputType messageType, DateTime timeStamp, MessageOutputForTests messageOutput)
        {
            
        }

        [TestMethod]
        public async Task TestRelateExternalFiles()
        {
            var expectations = new ExpectationsFileSystemManagement();
            var controller = expectations.GetController();
            var relateExternalFilesRequest = expectations.GetRelateExternalFilesRequest();
            var messageOutput = new MessageOutputForTests();
            messageOutput.OutputMessageForTests += MessageOutput_OutputMessageForTests1; ;

            var serviceResult = await controller.RelateExternalFile(relateExternalFilesRequest);

            if (serviceResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(serviceResult.ResultState.Additional1);
            }
        }

        private void MessageOutput_OutputMessageForTests1(string message, OntologyAppDBConnector.MessageOutputType messageType, DateTime timeStamp, MessageOutputForTests messageOutput)
        {
            
        }
    }
}
