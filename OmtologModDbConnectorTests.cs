﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OModulesTests.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests
{
    [TestClass]
    public class OmtologModDbConnectorTests
    {
        [TestMethod]
        public void TestGetOItems()
        {
            var expectations = new ExpectationsOntologyModDBConnector();

            var connector = expectations.GetController();

            var result = connector.GetOItems(expectations.AttributeTypeIds);
            
            if (result.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1 ?? "Unknown error");
            }

            if (result.Result.Count != 3)
            {
                Assert.Fail("The AttributeTypes cannot be found!");
            }

            result = connector.GetOItems(expectations.RelationTypesIds);

            if (result.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1 ?? "Unknown error");
            }

            if (result.Result.Count != 3)
            {
                Assert.Fail("The RelationTypes cannot be found!");
            }

            result = connector.GetOItems(expectations.ClassIds);

            if (result.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1 ?? "Unknown error");
            }

            if (result.Result.Count != 3)
            {
                Assert.Fail("The Classes cannot be found!");
            }

            result = connector.GetOItems(expectations.ObjectIds);

            if (result.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1 ?? "Unknown error");
            }

            if (result.Result.Count != 3)
            {
                Assert.Fail("The Objects cannot be found!");
            }

            result = connector.GetOItems(expectations.DifferentIds);

            if (result.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1 ?? "Unknown error");
            }

            if (result.Result.Count != 4)
            {
                Assert.Fail("The Different Items cannot be found!");
            }

            result = connector.GetOItems(expectations.InvalidId);

            if (result.ResultState.GUID != expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail("No error returned!");
            }
        }
    }
}
