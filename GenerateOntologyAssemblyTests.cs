﻿using GenerateOntologyAssembly;
using GenerateOntologyAssembly.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OModulesTests.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests
{
    [TestClass]
    public class GenerateOntologyAssemblyTests
    {
        

        [TestMethod]
        public async Task GenerateOntologyAssemblyTest()
        {
            var expectations = new ExpectationsGenerateOntologyAssembly();
            var connector = expectations.GetConnector();
            var request = expectations.GetOntologyAssemblyRequest();
            var result = await connector.GenerateAssembly(request);
            if (result.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task GenerateClassOntologyAssemblyTest()
        {
            var expectations = new ExpectationsGenerateOntologyAssembly();
            var connector = expectations.GetConnector();
            var request = expectations.GetClassOntologyAssemblyRequest();
            var result = await connector.GenerateClassAssembly(request);

            if (result.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }



        }
    }
}
