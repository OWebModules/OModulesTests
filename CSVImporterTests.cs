﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace OModulesTests
{
    [TestClass]
    public class CSVImporterTests
    {
        [TestMethod]
        public async Task TestCSVCompare()
        {
            
            var expectations = new Configuration.ExpectationsCSVImport();
            var controller = expectations.GetController();

            var request = expectations.GetCSVCompareRequestSubscriptions();

            var result = await controller.CompareCSV(request);

            if (result.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.Additional1);
            }
        }
    }
}
