﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OModulesTests.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests
{
    [TestClass]
    public class DatabaseManagementModuleTests
    {
        [TestMethod]
        public async Task TestGetColumnsIntegrated()
        {
            var expectation = new Configuration.ExpectationsDbManagementModule();
            var controller = expectation.GetController();
            var request = expectation.GetRequestWithIntegratedSecurity();

            var result = await controller.GetColumns(request);
            if (result.ResultState.GUID == expectation.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task TestGetColumnsAuthenticated()
        {
            var expectation = new Configuration.ExpectationsDbManagementModule();
            var controller = expectation.GetController();
            var request = expectation.GetRequestWithUserNameAndPassword();

            var result = await controller.GetColumns(request);
            if (result.ResultState.GUID == expectation.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task TestCheckMsConnectionString()
        {
            var expectation = new Configuration.ExpectationsDbManagementModule();
            var controller = expectation.GetController();
            var request = expectation.GetCheckConnectionStringRequest();
            var messageOutput = new MessageOutputForTests();
            messageOutput.AreMessagesOk = true;
            messageOutput.OutputMessageForTests += MessageOutput_TestCheckMsConnectionString;
            request.MessageOutput = messageOutput;

            var result = await controller.CheckMSConnectionString(request);
            if (result.GUID == expectation.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.Additional1);
            }
        }

        private void MessageOutput_TestCheckMsConnectionString(string message, OntologyAppDBConnector.MessageOutputType messageType, DateTime timeStamp, MessageOutputForTests messageOutput)
        {

        }

        [TestMethod]
        public async Task TestExportCodeToFile()
        {
            var expectation = new Configuration.ExpectationsDbManagementModule();
            var controller = expectation.GetController();
            var request = expectation.GetExportCodeToFileRequest();
            var messageOutput = new MessageOutputForTests();
            messageOutput.AreMessagesOk = true;
            messageOutput.OutputMessageForTests += MessageOutput_TestExportCodeToFile; ;
            request.MessageOutput = messageOutput;

            var result = await controller.ExportCodeToFile(request);
            if (result.ResultState.GUID == expectation.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        private void MessageOutput_TestExportCodeToFile(string message, OntologyAppDBConnector.MessageOutputType messageType, DateTime timeStamp, MessageOutputForTests messageOutput)
        {
            
        }

        [TestMethod]
        public async Task TestCheckExistanceDBItem()
        {
            var expectation = new Configuration.ExpectationsDbManagementModule();
            var controller = expectation.GetController();
            var messageOutput = new MessageOutputForTests();
            messageOutput.AreMessagesOk = true;
            messageOutput.OutputMessageForTests += MessageOutput_OutputMessageForTests;
            var request = expectation.GetCheckExistanceDBItemRequest();
            request.MessageOutput = messageOutput;
            var result = await controller.CheckExistanceDBItems(request);
            if (result.GUID == expectation.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.Additional1);
            }
        }

        private void MessageOutput_OutputMessageForTests(string message, OntologyAppDBConnector.MessageOutputType messageType, DateTime timeStamp, MessageOutputForTests messageOutput)
        {
            
        }

        [TestMethod]
        public async Task TestGetCode()
        {
            var expectations = new Configuration.ExpectationsDbManagementModule();
            var controller = expectations.GetMSSqlConnector();

            var dbItem = expectations.GetItemForCode();
            var connectionString = expectations.GetConnectionStringForGetCodeTest();

            var codeResult = await controller.GetMsCode(dbItem, connectionString);

            if (codeResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(codeResult.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task TestGetMsSQLCode()
        {
            var expectations = new Configuration.ExpectationsDbManagementModule();
            var controller = expectations.GetController();

            var request = expectations.GetMSSqlCodeRequest();
            var messageOutput = new MessageOutputForTests();
            messageOutput.OutputMessageForTests += MessageOutput_OutputMessageForTests_MsSQLCode;

            var codeResult = await controller.GetMSSqlCode(request);

            if (codeResult.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(codeResult.Additional1);
            }
        }

        private void MessageOutput_OutputMessageForTests_MsSQLCode(string message, OntologyAppDBConnector.MessageOutputType messageType, DateTime timeStamp, MessageOutputForTests messageOutput)
        {
            messageOutput.AreMessagesOk = true;
        }
    }
}
