﻿using System;
using System.Threading.Tasks;
using MaintainenceModule.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OModulesTests.Models;

namespace OModulesTests
{
    [TestClass]
    public class MaintenenceModuleTests
    {
        [TestMethod]
        public async Task TestConvertIntIdToGuidHash()
        {
            var expectations = new Configuration.ExpectationsMaintenenceModule();

            var request = expectations.GetConvertIntToGuidRequest();

            var controller = expectations.GetMaintenanceController();

            var result = await controller.ConvertIntIdToGuid(request);

            if (result.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task TestCleanupGUIDs()
        {
            var expectations = new Configuration.ExpectationsMaintenenceModule();

            var request = expectations.GetCleanupMultipleGUIDsRequest();

            var controller = expectations.GetMaintenanceController();

            var result = await controller.CleanupMultipleGUIDs(request);

            if (result.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task DeleteAbandonedLeafsTest()
        {
            var expectations = new Configuration.ExpectationsMaintenenceModule();

            var request = expectations.GetDeleteAbandonedLeafsRequest();

            var controller = expectations.GetMaintenanceController();

            var result = await controller.DeleteAbandonedLeafs(request);

            if (result.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task GetRelationsWithoutRelationTypes()
        {
            var expectations = new Configuration.ExpectationsMaintenenceModule();

            var controller = expectations.GetMaintenanceController();

            var result = await controller.GetRelationsWithoutRelationTypes();

            if (result.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task DeleteUnrelatedObjects()
        {
            var expectations = new Configuration.ExpectationsMaintenenceModule();

            var controller = expectations.GetMaintenanceController();

            var deleteRequest = expectations.GetDeleteUnrelatedObjectsRequest();
            var result = await controller.DeleteUnrelatedObjects(deleteRequest);

            if (result.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task MergeObjectsByClassTest()
        {
            var expectations = new Configuration.ExpectationsMaintenenceModule();

            var controller = expectations.GetOItemListController();

            var mergeRequest = expectations.GetMergeObjectsRequestByClass();
            var messageOutput = new MessageOutputForTests();
            messageOutput.OutputMessageForTests += MessageOutput_OutputMessageForTests;
            mergeRequest.MessageOutput = messageOutput;
            var result = await controller.MergeObjects(mergeRequest);

            if (result.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
            if (!messageOutput.AreMessagesOk)
            {
                Assert.Fail("Errors occurred!");
            }
        }

        [TestMethod]
        public async Task GenerateStatisticsTest()
        {
            var expectations = new Configuration.ExpectationsMaintenenceModule();

            var controller = expectations.GetStatisticsController();

            var messageOutput = new MessageOutputForTests();
            messageOutput.OutputMessageForTests += MessageOutput_OutputMessageForTests;
            var result = await controller.GenerateStatistics(messageOutput);

            if (result.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
            if (!messageOutput.AreMessagesOk)
            {
                Assert.Fail("Errors occurred!");
            }
        }


        [TestMethod]
        public async Task MergeObjectsByItemsTest()
        {
            var expectations = new Configuration.ExpectationsMaintenenceModule();

            var controller = expectations.GetOItemListController();

            var mergeRequest = expectations.GetMergeObjectsRequestByItems();
            var messageOutput = new MessageOutputForTests();
            messageOutput.OutputMessageForTests += MessageOutput_OutputMessageForTests;
            mergeRequest.MessageOutput = messageOutput;
            var result = await controller.MergeObjects(mergeRequest);

            if (result.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
            if (!messageOutput.AreMessagesOk)
            {
                Assert.Fail("Errors occurred!");
            }
        }

        private void MessageOutput_OutputMessageForTests(string message, OntologyAppDBConnector.MessageOutputType messageType, DateTime timeStamp, MessageOutputForTests messageOutput)
        {
            if (messageType == OntologyAppDBConnector.MessageOutputType.Error)
            {
                messageOutput.AreMessagesOk = false;
            }

            if (!messageOutput.AreMessagesOk)
            {
                return;
            }

            messageOutput.MessageCount++;

            Console.WriteLine(message);
        }

        [TestMethod]
        public async Task TestRelateMigrationScriptsToReplaceLogs()
        {
            var expectations = new Configuration.ExpectationsMaintenenceModule();

            var controller = expectations.GetMaintenanceController();
            var request = expectations.GetRelateObjectsWithObjectsByNameToAttributeRequest();

            var messageOutput = new MessageOutputForTests();
            messageOutput.OutputMessageForTests += MessageOutput_OutputMessageForTests1; ;
            request.MessageOutput = messageOutput;

            var result = await controller.RelateObjectsWithObjectsByNameToAttribute(request);

            if (result.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task TestRelateSynonymsToElementsUsing()
        {
            var expectations = new Configuration.ExpectationsMaintenenceModule();

            var controller = expectations.GetMaintenanceController();

            var messageOutput = new MessageOutputForTests();
            messageOutput.OutputMessageForTests += MessageOutput_OutputMessageForTests2;

            var result = await controller.RelateSynonymsToElementsUsing(messageOutput);

            if (result.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        private void MessageOutput_OutputMessageForTests2(string message, OntologyAppDBConnector.MessageOutputType messageType, DateTime timeStamp, MessageOutputForTests messageOutput)
        {
            
        }

        private void MessageOutput_OutputMessageForTests1(string message, OntologyAppDBConnector.MessageOutputType messageType, DateTime timeStamp, MessageOutputForTests messageOutput)
        {
            
        }

        [TestMethod]
        public async Task TestMapPathToApplicationByReplaceLog()
        {
            var expectations = new Configuration.ExpectationsMaintenenceModule();

            var controller = expectations.GetMaintenanceController();

            var messageOutput = new MessageOutputForTests();
            messageOutput.OutputMessageForTests += MessageOutput_MapPathToApplicationByReplaceLog;

            var result = await controller.MapPathToApplicationByReplaceLog(messageOutput);

            if (result.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        private void MessageOutput_MapPathToApplicationByReplaceLog(string message, OntologyAppDBConnector.MessageOutputType messageType, DateTime timeStamp, MessageOutputForTests messageOutput)
        {
            
        }

        [TestMethod]
        public async Task TestRepairAttributesAndRelations()
        {
            var expectations = new Configuration.ExpectationsMaintenenceModule();

            var controller = expectations.GetMaintenanceController();
            var request = expectations.GetRepairAttributesAndRelationsRequest();

            var messageOutput = new MessageOutputForTests();
            messageOutput.OutputMessageForTests += MessageOutput_OutputMessageForTests3;
            request.MessageOutput = messageOutput;

            var result = await controller.RepairAttributesAndRelations(request);

            if (result.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        private void MessageOutput_OutputMessageForTests3(string message, OntologyAppDBConnector.MessageOutputType messageType, DateTime timeStamp, MessageOutputForTests messageOutput)
        {
            
        }

    }
}
