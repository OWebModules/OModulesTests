﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OModulesTests.Configuration;
using OModulesTests.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests
{
    [TestClass]
    public class GraphMLTests
    {
        [TestMethod]
        public async Task ExportTypedTagsToGraphMLTest()
        {
            var expectations = new ExpectationsGraphML();

            var controller = expectations.GetController();
            var request = expectations.GetExportTypedTagsToGraphMLRequest();

            var createResult = await controller.ExportTypedTagsToGraphML(request);

            if (createResult.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                Assert.Fail(createResult.ResultState.Additional1);
            }
                
        }

        [TestMethod]
        public async Task ExportOntologyToGraphML()
        {
            var expectations = new ExpectationsGraphML();

            var controller = expectations.GetController();
            var request = expectations.GetExportOntologyToMLGraphRequest();

            var messageOutput = new MessageOutputForTests();
            messageOutput.OutputMessageForTests += MessageOutput_OutputMessageForTests1;

            var exportResult = await controller.ExportOntologyToMLGraph(request);

            if (exportResult.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(exportResult.Additional1);
            }

            if (!messageOutput.AreMessagesOk)
            {
                Assert.Fail("The Messages are not Ok!");
            }
        }

        private void MessageOutput_OutputMessageForTests1(string message, OntologyAppDBConnector.MessageOutputType messageType, DateTime timeStamp, MessageOutputForTests messageOutput)
        {
            messageOutput.AreMessagesOk = true;
        }

        [TestMethod]
        public async Task ExportERModelToGraphMLTest()
        {
            var expectations = new ExpectationsGraphML();

            var controller = expectations.GetController();
            var request = expectations.GetExportERModelRequest();

            var messageOutput = new MessageOutputForTests();
            messageOutput.OutputMessageForTests += MessageOutput_OutputMessageForTests;
            request.MessageOutput = messageOutput;

            var exportResult = await controller.ExportErModelToGraphML(request);

            if (exportResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(exportResult.ResultState.Additional1);
            }

            if (!messageOutput.AreMessagesOk)
            {
                Assert.Fail("Messages are not Ok!");
            }
        }

        private void MessageOutput_OutputMessageForTests(string message, OntologyAppDBConnector.MessageOutputType messageType, DateTime timeStamp, MessageOutputForTests messageOutput)
        {
            messageOutput.AreMessagesOk = true;
        }
    }
}
