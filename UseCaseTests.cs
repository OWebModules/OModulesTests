﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OModulesTests.Models;

namespace OModulesTests
{
    [TestClass]
    public class UseCaseTests
    {
        [TestMethod]
        public async Task TestHtmlTemplate()
        {
            var expectations = new Configuration.ExpectationsUseCaseModule();
            var request = expectations.GetUseCaseForTemplate();

            var controller = expectations.GetHtmlTemplateEngine();

            var importResult = await controller.TransformNames(new List<OntologyClasses.BaseClasses.clsOntologyItem> { request });
            if (importResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(importResult.ResultState.Additional1);
            }

        }

        [TestMethod]
        public async Task TestExportToJson()
        {
            var expectations = new Configuration.ExpectationsUseCaseModule();
            var request = expectations.GetExportToJsonRequest();
            var messageOutput = new MessageOutputForTests();
            request.MessageOutput = messageOutput;
            messageOutput.OutputMessageForTests += MessageOutput_OutputMessageForTests;
            var controller = expectations.GetUseCaseConnector();

            var importResult = await controller.ExportUseCasesToJson(request);
            if (importResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(importResult.ResultState.Additional1);
            }

        }

        private void MessageOutput_OutputMessageForTests(string message, OntologyAppDBConnector.MessageOutputType messageType, DateTime timeStamp, MessageOutputForTests messageOutput)
        {
            if (messageType == OntologyAppDBConnector.MessageOutputType.Error)
            {
                messageOutput.AreMessagesOk = false;
            }

            if (!messageOutput.AreMessagesOk)
            {
                return;
            }

            messageOutput.MessageCount++;
        }
    }
}
