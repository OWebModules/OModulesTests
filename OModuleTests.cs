﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests
{
    [TestClass]
    public class OModuleTests
    {
        [TestMethod]
        public async Task TestOModuleSync()
        {
            var expectations = new Configuration.ExpectationsOModuleTests();

            var omoduleSync = new OModules.Services.OModuleSync(expectations.Globals);

            var result = await omoduleSync.SyncOModules(null);

            if (result.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }
    }
}
