﻿using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OModulesTests.Models;

namespace OModulesTests
{
    [TestClass]
    public class HtmlEditorModuleTests
    {
        [TestMethod]
        public async Task CreateHtmlTest()
        {
            var expectation = new Configuration.ExpectationsHtmlEditorModule();
            var controller = expectation.GetHtmlExportController();

            var sbHtml = new StringBuilder();
            var intro = controller.GetHtmlIntro();
            sbHtml.AppendLine(intro);
            var request = expectation.GetHtmlTagRequest(HtmlEditorModule.ExportHtmlDocController.DocTypeDocumentInit, false);
            var result = await controller.GetHtmlTag(request);
            sbHtml.AppendLine(result.Result);
            request = expectation.GetHtmlTagRequest(HtmlEditorModule.ExportHtmlDocController.DocTypeHead, false);
            result = await controller.GetHtmlTag(request);
            sbHtml.AppendLine(result.Result);
        }

        [TestMethod]
        public async Task GetHtmlHistoryEntriesByDate()
        {
            var expectation = new Configuration.ExpectationsHtmlEditorModule();
            var controller = expectation.GetHtmlDocController();

            var request = expectation.GetHtmlHistoriesByDateRangeRequest();

            var result = await controller.GetHtmlHistoryByDateEntries(request);

            if (result.ResultState.GUID == expectation.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task GetHtmlHistoryDayStat()
        {
            var expectation = new Configuration.ExpectationsHtmlEditorModule();
            var controller = expectation.GetHtmlDocController();

            var request = expectation.GetHtmlHistoriesByDateRangeRequest();

            var result = await controller.GetHtmlHistoryByDateEntries(request);

            if (result.ResultState.GUID == expectation.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }

            var resultStat = await controller.GetHtmlHistoryDayStat(result.Result.HtmlHistoryEntries);

            if (resultStat.ResultState.GUID == expectation.Globals.LState_Error.GUID)
            {
                Assert.Fail(resultStat.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task SyncStatTest()
        {
            var expectation = new Configuration.ExpectationsHtmlEditorModule();
            var controller = expectation.GetHtmlDocController();


            var result = await controller.SyncHtmlState(new HtmlEditorModule.Models.SyncHtmlStateRequest());

            if (result.ResultState.GUID == expectation.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }

         
        }
        [TestMethod]
        public async Task GetRankedFoundWordsByRefAndHtmlTest()
        {
            var expectations = new Configuration.ExpectationsHtmlEditorModule();
            var controller = expectations.GetHtmlSearchProvider();
            var request = expectations.GetRankedFoundWordsByRefAndHtmlRequest();

            var messageOutput = new MessageOutputForTests();
            messageOutput.OutputMessageForTests += MessageOutput_OutputMessageForTests;
            request.MessageOutput = messageOutput;

            var getRankedFoundWordsResult = await controller.GetRankedFoundWordsByRefAndHtml(request);
            if (getRankedFoundWordsResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(getRankedFoundWordsResult.ResultState.Additional1);
            }     
        }

        private void MessageOutput_OutputMessageForTests(string message, OntologyAppDBConnector.MessageOutputType messageType, DateTime timeStamp, MessageOutputForTests messageOutput)
        {
            messageOutput.AreMessagesOk = true;
        }
    }
}
