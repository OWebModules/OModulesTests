﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OModulesTests.Configuration;
using OModulesTests.Models;
using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests
{
    [TestClass]
    public class TextParserModuleTests
    {

        private Globals globals = new Globals();

        [TestMethod]
        public async Task TestPdfConvert()
        {
            var expectations = new ExpectationsTextParser();

            var pdfImporter = expectations.GetPDFImporter();

            var request = expectations.GetRequest();

            var result = await pdfImporter.ConvertPDFFileToText(request);

            if (result.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }

            var errors = new List<string>();
            foreach (var resultItem in result.Result)
            {
                if (resultItem.Result.GUID == expectations.Globals.LState_Error.GUID)
                {
                    errors.Add(resultItem.Result.Additional1);
                }
            }

            if (errors.Any())
            {
                Assert.Fail($"Errors:{string.Join("\r\n", errors)}");
            }
        }

        [TestMethod]
        public async Task TestIndexPDF()
        {
            var expectations = new ExpectationsTextParser();

            var textParserController = expectations.GetIndexer();

            var request = expectations.GetIndexPDFRequest();

            var result = await textParserController.IndexPDFfiles(request);

            if (result.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task TestAnalyzeOModulesLog()
        {
            var expectations = new ExpectationsTextParser();

            var analyzeController = expectations.GetAnalyzeOModulesLogController();

            var messageOutput = new MessageOutputForTests();
            messageOutput.OutputMessageForTests += MessageOutput_OutputMessageForTests;  ;

            var result = await analyzeController.Analyze((new System.Threading.CancellationTokenSource()).Token, messageOutput);

            if (result.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.Additional1);
            }
        }

        private void MessageOutput_OutputMessageForTests(string message, OntologyAppDBConnector.MessageOutputType messageType, DateTime timeStamp, MessageOutputForTests messageOutput)
        {
            messageOutput.AreMessagesOk = true;
        }

        [TestMethod]
        public async Task TestMeasureTextParser ()
        {
            var expectations = new ExpectationsTextParser();

            var analyzeController = expectations.GetTextParserController();
            var request = expectations.GetMeasureTextParserRequest();

            var messageOutput = new MessageOutputForTests();
            messageOutput.OutputMessageForTests += MessageOutput_OutputMessageForTests1;
            request.MessageOutput = messageOutput;

            analyzeController.SavedMeasureDocs += AnalyzeController_SavedMeasureDocs;
            var result = await analyzeController.MeasureTextParser(request);

            if (result.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.Additional1);
            }
        }

        private OntologyClasses.BaseClasses.clsOntologyItem AnalyzeController_SavedMeasureDocs(List<OntologyClasses.BaseClasses.clsAppDocuments> documents, object sender)
        {
            return globals.LState_Success.Clone();
        }

        private void MessageOutput_OutputMessageForTests1(string message, OntologyAppDBConnector.MessageOutputType messageType, DateTime timeStamp, MessageOutputForTests messageOutput)
        {
            messageOutput.AreMessagesOk = true;
        }

        [TestMethod]
        public async Task RelateTextParserDocs()
        {
            var expectations = new ExpectationsTextParser();

            var textParserController = expectations.GetTextParserController();

            var textParser = await textParserController.GetTextParser(expectations.GetTextParserItem());
            var textParserFields = await textParserController.GetParserFields(new OntologyClasses.BaseClasses.clsOntologyItem
            {
                GUID = textParser.Result.First().IdFieldExtractor,
                Name = textParser.Result.First().NameFieldExtractor,
                GUID_Parent = textParser.Result.First().IdClassFieldExtractor,
                Type = expectations.Globals.Type_Object
            });

            var request = expectations.GetRelateTextParserDocsRequest(textParser.Result.First(), textParserFields.Result);

            var result = await textParserController.RelateTextParserDocs(request);

            if (result.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task TestReIndexParserSourceFields()
        {
            var expectations = new ExpectationsTextParser();

            var textParserController = expectations.GetTextParserController();

            var textParser = await textParserController.GetTextParser(expectations.GetTextParserItemForReIndexPatternSourceFields());
            var textParserFields = await textParserController.GetParserFields(new OntologyClasses.BaseClasses.clsOntologyItem
            {
                GUID = textParser.Result.First().IdFieldExtractor,
                Name = textParser.Result.First().NameFieldExtractor,
                GUID_Parent = textParser.Result.First().IdClassFieldExtractor,
                Type = expectations.Globals.Type_Object
            });

            var request = expectations.GetReIndexPatternSourceFieldsRequest(textParser.Result.First(), textParserFields.Result);

            var result = await textParserController.ReIndexPatternSourceFields(request);

            if (result.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }

        }
    }
}
