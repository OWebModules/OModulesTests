﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OModulesTests.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests
{
    [TestClass]
    public class ElasticAgentTests
    {
        [TestMethod]
        public async Task GetFilterItemsTest()
        {
            var expectations = new ExpectationsElasticAgent();

            var controller = expectations.GetElasticAgent();
            var filterItems = expectations.GetFilterList();

            var result = await controller.GetOItems(filterItems);
            if (result.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }

            if (result.Result.Count != filterItems.Count)
            {
                Assert.Fail($"The number of filteritems {filterItems.Count} is not equal to number of resultitems {result.Result.Count}");
            }
        }

        [TestMethod]
        public async Task RelateItems()
        {
            var expectations = new ExpectationsElasticAgent();

            var controller = expectations.GetElasticAgent();
            var filterItems = expectations.GetFilterList();

            var result = await controller.GetOItems(filterItems);
            if (result.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }

            var relateResult = await controller.RelateItems(result.Result,
                OntologyAppDBConnector.Services.TypeOfRelation.Create,
                "f62cf1ccee51475f9c007094f1809b56",
                expectations.Globals.Direction_RightLeft,
                expectations.GetRelationTypeBelongingSource());

            if (relateResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(relateResult.ResultState.Additional1);
            }

            var toRelate = expectations.GetRelationsImagesToFolder(relateResult.Result);

            var relateResult2 = await controller.RelateItems(toRelate);

            if (relateResult2.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(relateResult.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task GetRelationtypeTypeTest()
        {
            var expectations = new ExpectationsElasticAgent();

            var controller = expectations.GetElasticAgent();
            var relationTypeId = expectations.RelationTypeIdExisting;

            var result = await controller.GetOItem(relationTypeId);
            if (result.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }

            if (result.ResultState.GUID == expectations.Globals.LState_Nothing.GUID)
            {
                Assert.Fail($"The RelationType cannot be found!");
            }

            result = await controller.GetOItem(relationTypeId, expectations.Globals.Type_RelationType);
            if (result.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }

            relationTypeId = expectations.RelationTypeNotIdExisting;
            result = await controller.GetOItem(relationTypeId);
            if (result.ResultState.GUID != expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }


            
        }

    }
}
