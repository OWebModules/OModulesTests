﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests.Models
{
    public delegate void OutputMessageToTestDelegate(string message, MessageOutputType messageType, DateTime timeStamp, MessageOutputForTests messageOutput);
    public class MessageOutputForTests : IMessageOutput
    {
        private object locker = new object();

        public List<string> MessagesToCheck { get; set; } = new List<string>();

        private bool areMessagesOk;
        public bool AreMessagesOk
        {
            get
            {
                lock (locker)
                {
                    return areMessagesOk;
                }
            }

            set
            {
                lock (locker)
                {
                    areMessagesOk = value;
                }
            }
        }

        private int messageCount;
        public int MessageCount
        {
            get
            {
                lock (locker)
                {
                    return messageCount;
                }
            }

            set
            {
                lock (locker)
                {
                    messageCount = value;
                }
            }
        }

        public event OutputMessageDelegate OutputUIMessage;
        public event OutputMessageAddedDelegate OutputUIMessageAdded;
        public event OutputMessageToTestDelegate OutputMessageForTests;

        public void OutputCritical(string message)
        {
            OutputMessageForTests?.Invoke(message, MessageOutputType.Critical, DateTime.Now, this);
        }

        public void OutputError(string message)
        {
            OutputMessageForTests?.Invoke(message, MessageOutputType.Error, DateTime.Now, this);
        }

        public void OutputInfo(string message)
        {
            OutputMessageForTests?.Invoke(message, MessageOutputType.Info, DateTime.Now, this);
        }

        public void OutputMessage(string message, MessageOutputType messsageType)
        {
            OutputMessageForTests?.Invoke(message, messsageType, DateTime.Now, this);
        }

        public void OutputWarning(string message)
        {
            OutputMessageForTests?.Invoke(message, MessageOutputType.Warning, DateTime.Now, this);
        }
    }
}
