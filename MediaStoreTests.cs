﻿using MailLib.Models;
using MailModule.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OModulesTests.Configuration;
using OModulesTests.Models;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests
{
    [TestClass]
    public class MediaStoreTests
    {
        [TestMethod]
        public void GetPathFileSystemObjectTest()
        {
            var expectations = new ExpectationsMeidaStore();
            var connector = expectations.GetFileWorkConnector();

            var path = connector.GetPathFileSystemObject(expectations.FileSystemObjectUNCFile);

            if (Path.GetFullPath(path) != path)
            {
                Assert.Fail($"The path {path} is not valid!");
            }

            path = connector.GetPathFileSystemObject(expectations.FileSystemObjectDriveFile);

            if (Path.GetFullPath(path) != path)
            {
                Assert.Fail($"The path {path} is not valid!");
            }
        }

        
    }
}
