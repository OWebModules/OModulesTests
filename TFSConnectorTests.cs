﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OModulesTests.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests
{
    [TestClass]
    public class TFSConnectorTests
    {
        [TestMethod]
        public async Task TestTFSSync()
        {
            var expectations = new ExpectationsTFSConnector();

            var connector = expectations.GetConnectorSyncReleases();
            var request = expectations.GetRequest();

            var result = await connector.SyncTFS(request);

            if (result.ResultState.Additional1 == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }
    }
}
