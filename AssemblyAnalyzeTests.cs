﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OModulesTests.Configuration;

namespace OModulesTests
{
    [TestClass]
    public class AssemblyAnalyzeTests
    {
        [TestMethod]
        public async Task AnalyzeAssemblies()
        {
            var expections = new ExpectationsAnylzeAssembly();
            var request = expections.GetRequestAssemblyAnalyze();
            var connector = expections.GetController();
            var result = await connector.AnalyzeAssemblyIntoESIndex(request);
            if (result.ResultState.GUID == expections.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task SyncNamespaces()
        {
            var expections = new ExpectationsAnylzeAssembly();
            var request = expections.GetRequestSyncNamespaces();
            var connector = expections.GetController();
            var result = await connector.SyncNameSpaces(request);
            if (result.ResultState.GUID == expections.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task SyncClasses()
        {
            var expections = new ExpectationsAnylzeAssembly();
            var request = expections.GetRequestSyncClasses();
            var connector = expections.GetController();
            var result = await connector.SyncMembers(request);
            if (result.ResultState.GUID == expections.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task GetServiceReferences()
        {
            var expections = new ExpectationsAnylzeAssembly();
            var request = expections.GetGetServiceReferenceHierarchyRequest();
            var connector = expections.GetController();
            var result = await connector.GetServiceReferences(request);
            if (result.ResultState.GUID == expections.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }
    }
}
