﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OModulesTests.Models;

namespace OModulesTests
{
    [TestClass]
    public class LocalizationModuleTests
    {
        [TestMethod]
        public async Task GuiEntriesTest()
        {
            var expectations = new Configuration.ExpectationsLocalizationModule();

            var request = expectations.GetGuiEntriesRequest();
            var controller = expectations.GetController();

            var result = await controller.GetLicalizedGuiEntries(request);
            if (result.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task GetZeitpunkte()
        {
            var expectations = new Configuration.ExpectationsZeitpunkt();
            var controller = expectations.GetZeitpunktController();

            var result = await controller.GetZeitpunktModel();
            if (result.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task SyncNameTest()
        {
            var expectations = new Configuration.ExpectationsZeitpunkt();
            var controller = expectations.GetZeitpunktController();


            var result = await controller.SyncName(expectations.ZeitpunktItem);
            if (result.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.Additional1);
            }
        }

        [TestMethod]
        public async Task CompleteZeitpunktTest()
        {
            var expectations = new Configuration.ExpectationsZeitpunkt();
            var controller = expectations.GetZeitpunktController();

            var request = expectations.GetCompleteZeitpunktRequest();

            var result = await controller.CompleteZeitpunktOntologiesByNames(request);
            if (result.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task GetDateRangeTest()
        {
            var expectations = new Configuration.ExpectationsZeitpunkt();
            var controller = expectations.GetLocalizationController();

            var request = expectations.GetDateRangeRequest();
            var messageOutput = new MessageOutputForTests();
            messageOutput.OutputMessageForTests += MessageOutput_OutputMessageForTests;

            var result = await controller.GetDateRange(request);
            if (result.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task SyncCategories()
        {
            var expectations = new Configuration.ExpectationsLocalizationModule();
            var controller = expectations.GetCategoryController();
            var request = expectations.GetSyncCategoriesRequest();
            var messageOutput = new MessageOutputForTests();
            request.MessageOutput = messageOutput;
            messageOutput.OutputMessageForTests += MessageOutput_OutputMessageForTests_Categories;
            
            var result = await controller.SyncCategories(request);
            if (result.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }


        }

        private void MessageOutput_OutputMessageForTests_Categories(string message, OntologyAppDBConnector.MessageOutputType messageType, DateTime timeStamp, MessageOutputForTests messageOutput)
        {
            messageOutput.AreMessagesOk = true;
        }

        private void MessageOutput_OutputMessageForTests(string message, OntologyAppDBConnector.MessageOutputType messageType, DateTime timeStamp, MessageOutputForTests messageOutput)
        {
            messageOutput.AreMessagesOk = true;
        }
    }
}
