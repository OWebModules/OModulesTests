﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests
{
    [TestClass]
    public class TimeManagementModuleTests
    {
        [TestMethod]
        public async Task StartStopWatch()
        {
            var expectations = new Configuration.ExpectationsTimeManagement();
            var controller = expectations.Controller;
            var request = expectations.StopWatchRequestStart;
            var result = await controller.TriggerStopWatch(request);

            if (result.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task StopStopWatch()
        {
            var expectations = new Configuration.ExpectationsTimeManagement();
            var controller = expectations.Controller;
            var request = expectations.StopWatchRequestStop;
            var result = await controller.TriggerStopWatch(request);

            if (result.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        [TestMethod]
        public async Task MeasureStopWatch()
        {
            var expectations = new Configuration.ExpectationsTimeManagement();
            var controller = expectations.Controller;
            var request = expectations.StopWatchMeasureRequest;
            var result = await controller.GetStopWatchMeasure(request);

            if (result.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }

            var timeResult = result.Result.GetTime(expectations.Globals);
            if (timeResult.ResultState.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }
    }
}
