﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OModulesTests.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesTests
{
    [TestClass]
    public class CommandLineRunModuleTests
    {
        [TestMethod]
        public async Task TestGetCommandLineRunsToReport()
        {
            var expectation = new Configuration.ExpectationsCommandLineRunModule();
            var controller = expectation.GetController();
            var request = expectation.GetCommandRunsOfReportsRequest();
            var messageOutput = new MessageOutputForTests();
            messageOutput.AreMessagesOk = true;
            messageOutput.OutputMessageForTests += MessageOutput_TestGetCommandLineRunsToReport; ;
            request.MessageOutput = messageOutput;

            var result = await controller.GetCommandLineRunsOfReports(request, false);
            if (result.ResultState.GUID == expectation.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }

        private void MessageOutput_TestGetCommandLineRunsToReport(string message, OntologyAppDBConnector.MessageOutputType messageType, DateTime timeStamp, MessageOutputForTests messageOutput)
        {
            
        }

        
    }
}
