﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace OModulesTests
{
    [TestClass]
    public class PsScriptOutputParserTests
    {
        [TestMethod]
        public async Task TestScriptParser()
        {
            var expectations = new Configuration.ExpectationsPsScriptOutputParserModule();
            var controller = expectations.GetController();
            var request = expectations.GetRequest();

            

            var result = await controller.PsScriptOutput(request);

            if (result.ResultState.GUID == controller.Globals.LState_Error.GUID)
            {
                Assert.Fail(result.ResultState.Additional1);
            }
        }
    }
}
