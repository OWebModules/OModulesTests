﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace OModulesTests
{
    [TestClass]
    public class ElasticSearchNestConnectorTests
    {
        [TestMethod]
        public void SaveJsonFileToNewIndex()
        {
            var expectations = new Configuration.ExpectationsElasticSearchNestTest();

            var dbUpdater = expectations.GetCustomDbUpdater(expectations.ImportIndex);

            var sourceJson = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, object>>(System.IO.File.ReadAllText(expectations.FilePathImportFile));

            var result = dbUpdater.SaveDoc(sourceJson, expectations.ImportType);
            if (result.GUID == expectations.Globals.LState_Error.GUID)
            {
                Assert.Fail("Json cannot be saved!");
            }
        }

        [TestMethod]
        public void GetObjectAttributeByDateRangeFilter()
        {
            var expectations = new Configuration.ExpectationsElasticSearchNestTest();

            var dbReader = expectations.GetDbReader();

            var search = expectations.GetDateTimeSearch;
            var filter = expectations.GetDateTimeRangeFilterFull;

            var items = dbReader.get_Data_ObjectAtt(search, false, filter: filter);

            if (!items.Any())
            {
                Assert.Fail("Json cannot be saved!");
            }
        }
    }
}
